<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php 
	global $wow_settings, $post;
	$direction = wow_get_direction();
	$class_direction = '';
	if($direction && $direction == 'rtl'){
		$class_direction .= 'direction="rtl"';
	} 
	$sitelogo = wow_get_config('sitelogo');
?>
<!--<![endif]-->
<head <?php echo esc_attr($class_direction); ?>>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) : ?>
		<?php if (isset($wow_settings['favicon']) && $wow_settings['favicon']['url']){ ?>
			<link rel="shortcut icon" href="<?php echo esc_url( $wow_settings['favicon']['url'] ); ?>" />
		<?php } ?>
	<?php endif;  ?>	
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php if(isset($wow_settings['show-loading-overlay']) && $wow_settings['show-loading-overlay']) : ?>
	<div class="loader-content">
		<div id="loader">
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
		</div>
	</div>
<?php endif; ?>

<div id='page' class="hfeed vel header-error">
	<?php
		
		$header_layout = wow_get_config('type_header');	
		$header_style = wow_get_config('header_style');
		
		if(!empty($header_layout))
		{
			$header_layout = str_replace("header-", "", $header_layout);
			get_template_part('headers/header', $header_layout);
		}
		else
		{
			if($header_style == 'default'){
				get_template_part('headers/header', '1');
			}else{
				$header_style = str_replace("header-", "", $header_style);
				get_template_part('headers/header',$header_style);
			}	
		}
	?>
	
	<div id="bin-main" class="bin-main bin-main-content">

	<?php if(isset($wow_settings['breadcrumb']) && $wow_settings['breadcrumb']){
		wow_page_title();
	}else{ ?>
		<div class="container">
			<h1 class="bin-title-default"> <?php if(!is_home()){ the_title(); }else{ echo esc_html_e('Latest Posts', 'wow'); } ?> </h1>
		</div>
	<?php } ?>	