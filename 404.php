<?php
	get_header();
	global $wow_settings;
?>
<div class="container page-404">
	<div class="row">
		<?php if(isset($wow_settings['img-404']) && $wow_settings['img-404']) : ?>
			<div class="col-lg-6 col-md-6 col-xs-12 left">  
				<div class="img-404">
					<img src="<?php echo esc_url($wow_settings['img-404']['url']); ?>">
				</div>
			</div>
		<?php endif; ?>
		<div class="<?php if(isset($wow_settings['img-404']) && $wow_settings['img-404']){ echo "col-lg-6 col-md-6"; } ?>  col-xs-12 right">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
					<header class="page-header">
						<h2 class="page-title"> <?php echo isset($wow_settings['title-error']) ? esc_html($wow_settings['title-error']) : esc_html__('Wow!', 'wow');  ?> </h2>
					</header>
					<div class="page-content">
						<div class="content-404">
							<h3><?php echo isset($wow_settings['not-found-error']) ? esc_html($wow_settings['not-found-error']) : esc_html__('Input a block slug name','wow'); ?></h3>
							<p><?php echo isset($wow_settings['text-error']) ? esc_html($wow_settings['text-error']) : esc_html__('You may have mis-typed the URL. Or the page has been removed, had its name changed, or is temporarily unavailable.', 'wow'); ?></p>
							<div class="btn-404">
								<a href="<?php echo esc_url( home_url('/') ); ?>"><?php echo isset($wow_settings['btn-error']) ? esc_html($wow_settings['btn-error']) : esc_html__('Back to Homepage','wow'); ?></a>
							</div>
						</div>
					</div><!-- .page-content -->

				</div><!-- #content -->
			</div><!-- #primary -->		
		</div>
	</div>
</div>

<?php
get_footer();