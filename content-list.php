<?php 
	global $wow_settings,$instance;
	$format = get_post_format();
?>
<?php if( empty($format) || $format == 'image' ){?>
<div class="list-post">
    <article id="post-<?php esc_attr(the_ID()); ?>" <?php post_class(); ?>>
		<div class="entry-thumb single-thumb">
		<?php if ( get_the_post_thumbnail() ){?>
			<a class="post-thumbnail" href="<?php the_permalink(); ?>" title="<?php esc_attr(the_title_attribute()); ?>">
				<?php the_post_thumbnail( 'large' )?>				
			</a>
			<h3 class="post-date">
               <?php echo ( get_the_title() ) ? '<span class="days">'.date( 'j',strtotime($post->post_date)).'</span>'.date( 'F, Y',strtotime($post->post_date)) : '<a href="'.esc_url(get_the_permalink()).'">'.date( 'F j, Y',strtotime($post->post_date)).'</a>'; ?>
            </h3>		
		<?php }else{ ?>
			<?php if($format){?>
			<a class="post-thumbnail" href="<?php esc_url(the_permalink()); ?>" title="<?php esc_attr(the_title_attribute()); ?>">
				<img src="<?php echo esc_url(get_template_directory_uri().'/assets/img/format/medium-'.$format.'.png'); ?>" title="<?php esc_attr(the_title_attribute()); ?>" alt="<?php esc_attr(the_title_attribute()); ?>" />
			</a>
			<?php } ?>
		<?php } ?>
		</div>
        <div class="post-content">            
        	<h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
        	<?php
        		if (isset($wow_settings['blog-excerpt']) && $wow_settings['blog-excerpt']) {
                    echo wow_get_excerpt( $wow_settings['list-blog-excerpt-length'], 15);
                }
        	?>
			<?php if(isset($wow_settings['archives-readmore']) && $wow_settings['archives-readmore']) : ?>
				<h3 class="post-btn"><a class="post-btn-more" href="<?php esc_url(the_permalink()); ?>"> <?php echo  esc_html__( 'Read more', 'wow' ) ?></a></h3>
			<?php endif; ?>
    	</div>
    </article><!-- #post-## -->
</div>
<?php }else{ ?>
<div class="list-post">
    <article id="post-<?php esc_attr(the_ID()); ?>" <?php post_class(); ?>>
		<div class="entry-thumb single-thumb">	
			<a class="post-thumbnail text-center" href="<?php esc_url(the_permalink()); ?>" title="<?php esc_attr(the_title_attribute()); ?>">
				<img src="<?php echo esc_url(get_template_directory_uri().'/images/placeholder.jpg'); ?>" title="<?php esc_attr(the_title_attribute()); ?>" alt="<?php esc_attr(the_title_attribute()); ?>" />
			</a>
		<?php if( $format == 'gallery' ) { 
			if(preg_match_all('/\[gallery(.*?)?\]/', get_post($instance['post_id'])->post_content, $matches)){
				$attrs = array();
				if (count($matches[1])>0){
					foreach ($matches[1] as $m){
						$attrs[] = shortcode_parse_atts($m);
					}
				}
				if (count($attrs)> 0){
					foreach ($attrs as $attr){
						if (is_array($attr) && array_key_exists('ids', $attr)){
							$ids = $attr['ids'];
							break;
						}
					}
				}
			?>
			<div class="entry-thumb">
				<div id="gallery_slider_<?php echo esc_attr($post->ID); ?>" class="carousel slide gallery-slider" data-interval="0">	
					<div class="carousel-inner">
						<?php
							$ids = '';						
							$ids = explode(',', $ids);						
							foreach ( $ids as $i => $id ){ ?>
								<div class="item<?php echo ( $i== 0 ) ? ' active' : '';  ?>">			
										<?php echo wp_get_attachment_image($id, 'full'); ?>
								</div>
							<?php }	?>
					</div>
					<a href="#gallery_slider_<?php echo esc_attr($post->ID); ?>" class="left carousel-control" data-slide="prev"><?php esc_html_e( 'Prev', 'wow' ) ?></a>
					<a href="#gallery_slider_<?php echo esc_attr($post->ID); ?>" class="right carousel-control" data-slide="next"><?php esc_html_e( 'Next', 'wow' ) ?></a>
				</div>
			</div>
			<?php }	?>							
		<?php } ?>

		<?php if( $format == 'quote' ) { ?>
		<?php } ?>
		</div>
		<h3 class="post-date">
			<?php echo ( get_the_title() ) ? '<span class="days">'.date( 'j',strtotime($post->post_date)).'</span>'.date( 'F, Y',strtotime($post->post_date)) : '<a href="'.esc_url(get_the_permalink()).'">'.date( 'F j, Y',strtotime($post->post_date)).'</a>'; ?>
		</h3>		
        <div class="post-content">
           
        	<h2 class="entry-title"><a href="<?php esc_url(the_permalink()); ?>"><?php the_title() ?></a></h2>
        	<?php
        		if (isset($wow_settings['blog-excerpt']) && $wow_settings['blog-excerpt']) {
                    echo wow_get_excerpt( wow_get_config('list-blog-excerpt-length'), 15);
                }
        	?>
			<?php if(isset($wow_settings['archives-readmore']) && $wow_settings['archives-readmore']) : ?>
				<h3 class="post-btn"><a class="post-btn-more" href="<?php esc_url(the_permalink()); ?>"> <?php echo  esc_html__( 'Read more', 'wow' ) ?></a></h3>
			<?php endif; ?>
    	</div>
    </article><!-- #post-## -->
</div>	
<?php }?>