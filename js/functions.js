/**
 * Theme functions file
 *
 * Contains handlers for navigation, accessibility, header sizing
 * footer widgets and Featured Content slider
 *
 */
( function( $ ) {
	"use strict";
	var body    = $( 'body' ),
		_window = $( window );

	 /* Hover intent
    -------------------------------------------------------------- */
    jQuery('body').on('mouseenter', '#cart',
        function () {
            jQuery(this).find(".cart-popup").stop().slideDown(300);
        }
    );
    jQuery('body').on('mouseleave', '#cart',
        function () {
            jQuery(this).find(".cart-popup").stop().slideUp(300);
        }
    );
    jQuery('body').on('mouseenter', '.menu-box',
        function () {
            jQuery(this).find(".display-dropdown").stop().slideDown(300);
        }
    );
    jQuery('body').on('mouseleave', '.menu-box',
        function () {
            jQuery(this).find(".display-dropdown").stop().slideUp(300);
        }
    );
    jQuery('body').on('mouseenter', '.block-account',
        function () {
            jQuery(this).find(".block-account-content").stop().slideDown(300);
        }
    );
    jQuery('body').on('mouseleave', '.block-account',
        function () {
            jQuery(this).find(".block-account-content").stop().slideUp(300);
        }
    );
	

	// Enable menu toggle for small screens.
	( function() {
		$(document).ready(function() {
			/*
			** Blog Masonry
			*/
			// $('body').find('.blog-content-grid').isotope({ 
			// 	layoutMode : 'masonry'
			// });		
			
			if($(window).width() > 1023){
				if($(".header-content").data("sticky_header"))
					stickymenu();				
			};
		
			var wd_width = $(window).width();
			if(wd_width > 992)
				offtogglemegamenu();
			else
				appendGrower();	
				
			$(window).resize(function() {
				var bin_width = $( window ).width();
				if(bin_width > 992){
					removeGrower();
					offtogglemegamenu();
				}	
				else{
					appendGrower();
				}	
			});

			function _fix_vc_full_width_row(){
				var $elements = jQuery('[data-vc-full-width="true"]');
				jQuery.each($elements, function () {
					var $el = jQuery(this);
					$el.css('right', $el.css('left')).css('left', '');
				});
			}

			// Fixes rows in RTL
			jQuery(document).on('vc-full-width-row', function () {
				if($('.rtl').length){
					_fix_vc_full_width_row();					
				}
			});
			// Run one time because it was not firing in Mac/Firefox and Windows/Edge some times
			if($('.rtl').length){
				_fix_vc_full_width_row();					
			}		
			
			/* Quickview */
			$('.fancybox').fancybox({
				'width'     : 850,
				'height'   : '500',
				'autoSize' : false,
				afterShow: function() {
					$(' .share ').on('click', function() {
						$(' .social-share ').toggle( "slow" );
					});
					$( '.quickview-container .product-images' ).each(function(){
						var $id = this.id;
						var $rtl = $('body').hasClass( 'rtl' );
						var $img_slider = $( '#' + $id + ' .product-responsive');
						var $thumb_slider = $( '#' + $id + ' .product-responsive-thumbnail' )
						$img_slider.slick({
							slidesToShow: 1,
							slidesToScroll: 1,
							fade: true,
							arrows: false,
							rtl: $rtl,
							asNavFor: $thumb_slider
						});
						$thumb_slider.slick({
							slidesToShow: 3,
							slidesToScroll: 1,
							asNavFor: $img_slider,
							arrows: true,
							focusOnSelect: true,
							rtl: $rtl,
							responsive: [				
								{
								  breakpoint: 360,
								  settings: {
									slidesToShow: 2    
								  }
								}
							  ]
						});

						$(".product-images").fadeIn(1000, function() {
							$(this).removeClass("loading");
						});
					});
					_load_owl_carousel();
				}
			});	
			
			_event_view_list();	
		});
		
		function _event_view_list(){
			var this_class	= $("ul.products").data("col");
			var list_class 	= "col-lg-12 col-md-12 col-xs-12";	
			if($('.view-grid').hasClass('active'))
				$("ul.products-list").addClass('grid');
			if($('.view-list').hasClass('active')){
				$("ul.products-list").addClass('list');	
				$("ul.products-list li").removeClass(this_class).addClass(list_class);
			}	
			/* Change List Product */		
			$('.view-grid').on('click',function(){
				$('.view-list').removeClass('active');
				$(this).addClass('active');
				$("ul.products-list li").removeClass(list_class).addClass(this_class);
				$("ul.products-list").removeClass("list").addClass('grid');	
				return false;
			});
			
			$('.view-list').on('click',function(){
				$('.view-grid').removeClass('active');
				$(this).addClass('active');
				$("ul.products-list li").removeClass(this_class).addClass(list_class);
				$("ul.products-list").removeClass("grid").addClass('list');
				return false;
			});			
		}
	
		$('#show-megamenu').click(function() {
			if($('.bin-navigation').hasClass('bin-navigation-active'))
				$('.bin-navigation').removeClass('bin-navigation-active');
			else
				$('.bin-navigation').addClass('bin-navigation-active');
			return false;
		});
		$('#remove-megamenu').click(function() {
			$('.bin-navigation').removeClass('bin-navigation-active');
			return false;
		});
		
		function stickymenu(){
			var CurrentScroll = 0;
			var bin_width = $( window ).width();
			$(window).scroll(function() {
				if(($(".header-content").data("sticky_tablet") == 0) && bin_width < 992)
					return;
				var NextScroll = $(this).scrollTop();
				if ((NextScroll < CurrentScroll) && NextScroll > 200) {
					$('.bin-header').addClass('sticky');
					$('.bingoLogo').hide();
					$('.bingoLogo-sticky').show();
				} else if (NextScroll >= CurrentScroll ||  NextScroll <=200 ) {
					$('.bin-header').removeClass('sticky');
					$('.bingoLogo-sticky').hide();
					$('.bingoLogo').show();
				}

				CurrentScroll = NextScroll;  
			});	
		}
		
		function clickGrower(){
			$(".bin-navigation  li.menu-item-has-children  .grower").click(function(){
				if($(this).hasClass('close'))
					$(this).addClass('open').removeClass('close');
				else
					$(this).addClass('close').removeClass('open');				
				$('.sub-menu',$(this).parent()).first().toggle(300);			
			});			
		}
		
		function appendGrower()
		{
			$(".bin-navigation  li.menu-item-has-children").append('<span class="grower close"> </span>');
			clickGrower();
		}
		function removeGrower()
		{
			$(".grower",".bin-navigation  li.menu-item-has-children").remove();
		}
		function offtogglemegamenu()
		{
			$('#bin-navigation li.menu-item-has-children .sub-menu').css('display','');	
			$('#bin-navigation').removeClass('bin-navigation-active');
			$("#bin-navigation  li.menu-item-has-children  .grower").removeClass('open').addClass('close');	
		}	
	} )();

	( function () {
	 var data = {
	   'action': 'mode_theme_update_mini_cart'
	 };
	 $.post(
	   woocommerce_params.ajax_url, // The AJAX URL
	   data, // Send our PHP function
	   function(response){
	     $('#cart-popup').html(response); // Repopulate the specific element with the new content
	   }
	 );
	// Close anon function.
	} );

	/*
	 * Makes "skip to content" link work correctly in IE9 and Chrome for better
	 * accessibility.
	 *
	 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
	 */
	_window.on( 'hashchange.break', function() {
		var hash = location.hash.substring( 1 ), element;

		if ( ! hash ) {
			return;
		}

		element = document.getElementById( hash );

		if ( element ) {
			if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) {
				element.tabIndex = -1;
			}

			element.focus();

			// Repositions the window on jump-to-anchor to account for header height.
			window.scrollBy( 0, -80 );
		}
	} );

	$( function() {
		// Search toggle.
		$( '.search-toggle' ).on( 'click.break', function( event ) {
			var wrapper = $( '.search-overlay' );
				wrapper.toggleClass( 'hidden' );
		} );
		$( '.search-popup-bg' ).on( 'click.break', function( event ) {
			var wrapper = $( '.search-overlay' );
				wrapper.toggleClass( 'hidden' );
		} );


		// Focus styles for menus.
		$( '.primary-navigation, .secondary-navigation' ).find( 'a' ).on( 'focus.break blur.break', function() {
			$( this ).parents().toggleClass( 'focus' );
		} );
	} );

	_window.load( function() {
		// Arrange footer widgets vertically.
		if ( $.isFunction( $.fn.masonry ) ) {
			$( '#footer-sidebar' ).masonry( {
				itemSelector: '.widget',
				columnWidth: function( containerWidth ) {
					return containerWidth / 4;
				},
				gutterWidth: 0,
				isResizable: true,
				isRTL: $( 'body' ).is( '.rtl' )
			} );
		}

		// Initialize Featured Content slider.
		if ( body.is( '.slider' ) ) {
			$( '.featured-content' ).featuredslider( {
				selector: '.featured-content-inner > article',
				controlsContainer: '.featured-content'
			} );
		}
	} );

	//Check to see if the window is top if not then display button
   _window.scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.back-top').addClass('button-show');
        } else {
            $('.back-top').removeClass('button-show');
        }
    });
    //Click event to scroll to top
    $('.back-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $(window).load(function() {
		// Animate loader off screen
		$('body').addClass('loaded');
	});

	/* Add button show / hide for widget_product_categories */
	$( '.widget.woocommerce ul.children' ).each(function(index, el) {
		$( this ).parent().addClass('cat-parent').prepend('<span class="arrow"></span>');
		$( this ).parent().find( '.arrow' ).on( 'click', function( e ) {
			e.preventDefault();
			$( this ).parent().toggleClass('open').find( '> .children' ).stop().slideToggle(400);
		});
	});	

	/* Tooltip Block */
	if($('.products-list .products-thumb a.quickview').length){
		$(".products-list .products-thumb a.quickview").wrap("<div class='bin-quickview-btn'><div></div></div>");
	}
	if($('.BoxInfo .compare.button').length){
		$('.BoxInfo .compare.button').wrap("<div class='bin-compare-btn'></div>");
	}
	if($('.woocommerce-info .showlogin').length){
		$('.woocommerce-info .showlogin').parent().addClass('first');
	}

	jQuery('.product-wapper .product-button a, .product-wapper .products-thumb a.quickview, .yith-wcwl-add-to-wishlist a, .BoxInfo .compare.button ').each(function(){
		wowtooltip(jQuery(this), 'html');
	});	

	/* Tools Tip */
	function wowtooltip(element, content) {				
		if(content=='html'){
			var tipText = element.html();
		} else {
			var tipText = element.attr('title');
		}		
		element.on('mouseover', function(){
			if(jQuery('.wowtooltip').length == 0) {
				element.before('<span class="wowtooltip">'+tipText+'</span>');			
				var tipWidth = jQuery('.wowtooltip').outerWidth();
				var tipPush = -(tipWidth/2 - element.outerWidth()/2);
				jQuery('.wowtooltip').css('margin-left', tipPush);
			}
		});
		element.on('mouseleave', function(){
			jQuery('.wowtooltip').remove();
		});
	}

	/* Show/hide NewsLetter Popup */
	jQuery(window).load(function() {
		wow_ShowNLPopup();
	});

	jQuery('.newsletterpopup .close-popup').click(function(){
		wow_HideNLPopup();
	});

	jQuery('.popupshadow').click(function(){
		wow_HideNLPopup();
	});	
		
	/* Function Show NewsLetter Popup */
	function wow_ShowNLPopup() {
		if($('.newsletterpopup').length){
			var cookieValue = $.cookie("wow_lpopup");
			if(cookieValue == 1) {
				jQuery('.newsletterpopup').hide();
				jQuery('.popupshadow').hide();
			}else{
				jQuery('.newsletterpopup').show();
				jQuery('.popupshadow').show();
			}				
		}
	}


	/* Function Hide NewsLetter Popup when click on button Close */
	function wow_HideNLPopup(){
		jQuery('.newsletterpopup').hide();
		jQuery('.popupshadow').hide();
		$.cookie("wow_lpopup", 1, { expires : 24 * 60 * 60 * 1000 });
	}

	/*Menu Categories*/
	if($('.categories-menu .btn-categories').length){
		$('.categories-menu .btn-categories').click(function(){
			$('.wrapper-categories').toggleClass('bin-active');
		});
		$('.wrapper-categories .remove-megamenu').click(function(){
			$('.wrapper-categories').removeClass('bin-active');
		});
	}
	/*Background Dark*/
	if($('.bin-main .background-dark').length){
		$('body').addClass('head-background-dark');
	}else{
		$('body').removeClass('head-background-dark');
	}
	
    $(window).load(function() {
		// Animate loader off screen
		$('body').addClass('loaded');
		_load_owl_carousel();
		//owl-carousel		
	});	
	
	function _load_owl_carousel(){
		$( ".owl-carousel.bin-carousel" ).each(function() {
				$(this).owlCarousel({
				nav: ($(this).data("nav")) ? true : false,
				navContainerClass: 'owl-buttons',
				navText: [ '<i class="fa fa-angle-double-left"></i>','<i class="fa fa-angle-double-right"></i>' ],
				navClass: [ 'owl-prev carousel-control left', 'owl-next carousel-control right' ],
				dots: false,
				loop: ($(this).data("loop")) ? true : false,		    
				// center: true,
				// items:2,
				rtl: $("body").hasClass("rtl") ? true : false ,
				margin: ($(this).data("margin")) ? $(this).data("margin") : 0,
				responsiveClass:true,
				responsive:{
					0:{
						items:$(this).data("columns4")
					},
					480:{
						items:$(this).data("columns3")
					},
					768:{
						items:$(this).data("columns2")
					},
					1024:{
						items:$(this).data("columns1")
					},					
					1200:{
						items:$(this).data("columns")
					}
				},
			});
		});			
		var isMobile = false;
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) 
			isMobile = true;
			
		if(isMobile){
			$( ".products-list, .post-list" ).each(function() {
				$(this).removeClass("owl-carousel");
				var class_item = "col-lg-"+(12/$(this).data("columns"))+ " col-md-"+(12/$(this).data("columns1"))+" col-sm-"+(12/$(this).data("columns2"))+" col-xs-"+(12/$(this).data("columns4"));
				$('article',$(this)).addClass(class_item);
			});	
		}else{
			$( ".owl-carousel" ).each(function() {
					$(this).owlCarousel({
					nav: false,
					navContainerClass: 'owl-buttons',
					navText: [ '<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>' ],
					navClass: [ 'owl-prev carousel-control left', 'owl-next carousel-control right' ],
					dots:false,
					loop:false,
					rtl: $("body").hasClass("rtl") ? true : false ,
					margin: ($(this).data("margin")) ? $(this).data("margin") : 0,
					responsiveClass:true,
					responsive:{
						0:{
							items:$(this).data("columns4")
						},
						480:{
							items:$(this).data("columns3")
						},
						768:{
							items:$(this).data("columns2")
						},
						1024:{
							items:$(this).data("columns1")
						},					
						1200:{
							items:$(this).data("columns")
						}
					},
				});
			});				
		}			
	}
	/*Onload Input*/
	$('body').on('focus','.form-row input',function(){
		$(this).parents('.form-row').addClass('filled').removeClass('no-text');
	});
	$('body').on('blur','.form-row input',function(){
		if($(this).val().length > 0) $(this).parents('.form-row').addClass('has-text').removeClass('no-text');
		else $(this).parents('.form-row').removeClass('has-text').addClass('no-text');
		$(this).parents('.form-row').removeClass('filled');
	});
	
	//on load
	function checkValueOnLoad() {
		$('.form-row input').each(function(){
			if($(this).val().length > 0) $(this).parents('.form-row').addClass('has-text').removeClass('no-text');
		});
	}
	checkValueOnLoad();

	function wow_close_tab(){
		if($('.close_tab').length){
			$('#show-megamenu').click(function(){
				$('.close_tab').toggleClass('on');
			});
			$('#remove-megamenu').click(function(){
				$('.close_tab').removeClass('on');
			});
			$('.close_tab').click(function(){
				$('.bingo-menu-wrapper .bin-navigation').removeClass('bin-navigation-active')
				$('.close_tab').removeClass('on');
			});
		}
		if($('.close-tab-2').length){
			$('.btn-categories').click(function(){
				$('.close-tab-2').toggleClass('on');
			});
			$('.remove-megamenu--2').click(function(){
				$('.close-tab-2').removeClass('on');
			});
			$('.close-tab-2').click(function(){
				$('.categories-menu .wrapper-categories').removeClass('bin-active');
				$('.close-tab-2').removeClass('on');
			});
		}	
	}
	wow_close_tab();

	// function wow_btn_view_all(){
	// 	if(_window.width() > 1023){
	// 		if($('.wrap-title').length){
	// 			var height_view_all = $('.p-wrap--first:nth-child(1) .item-product').height();
	// 			$('.wrap-title').css('height', (height_view_all - 15));
	// 		}
	// 		if($('.p-wrap--last').length){
	// 			var height_p_wrap_first 	= $('.p-wrap--first').height();
	// 			var height_p_wrap_thumb 	= $('.p-wrap--last .products-thumb').height();
	// 			var height_p_wrap_content 	= $('.p-wrap--last .products-content').height();
	// 			var height_all  			= height_p_wrap_first - height_p_wrap_thumb - height_p_wrap_content - 30;
	// 			$('.p-wrap--last .product-wapper .products-content').css('padding-top', height_all);	
	// 		}
	// 	}
	// }
	// wow_btn_view_all();

} )( jQuery );