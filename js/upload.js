(function($) {
	"use strict";
		jQuery(document).ready(function($) {
			$(document).on("click", ".upload_image_button", function() {
				var inputText = $(this).data("image_id");
				window.send_to_editor = function(html) {
					var imgurl = $('img',html).attr('src');
					$('#' + inputText).val(imgurl);
					$('.' + inputText).attr('src' , imgurl);
					$('.' + inputText).show();
					tb_remove();
				};
				tb_show('', 'media-upload.php?type=image&TB_iframe=true');
				return false;
			});
			
			$(document).on("click", ".remove_image_button", function() {
				var inputText = $(this).data("image_id");
				$('#' + inputText).val("");
				$('.' + inputText).hide();
				$('.' + inputText).attr('src',"");
				return false;
			});
			
		});		
})(jQuery);


