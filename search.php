<?php
get_header();

$sidebar_blog = wow_get_config('sidebar_blog');
$blog_layout = wow_get_config('blog_layout');

if( $sidebar_blog == 'full' ) {
	$sidebar_blog_class = '';
} else {
	$sidebar_blog_class = $sidebar_blog == 'left' ? ' pull-right' : '';
}

?>
<div class="container bin-main-content"  id="container">
	<div class="row">
		<div class="<?php echo esc_attr(wow_get_class()->class_blog_content  . $sidebar_blog_class); ?>">
			<div id="primary" class="content-area">
				
				<div id="content" class="site-content<?php echo isset($site_content_class) ? esc_attr($site_content_class) : ""; ?>" role="main">
					<?php if ( have_posts() ) : ?>								
					<div class="row">

						<?php
							// Start the Loop.
							while ( have_posts() ) : the_post();

							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							 
							//get_template_part( 'content', get_post_format() );
							get_template_part( 'content', $blog_layout);

							endwhile;

						?>
					</div>

						<?php

							// Previous/next page navigation.
							wow_paging_nav();

						else :
							// If no content, include the "No posts found" template.
							get_template_part( 'content', 'none' );

						endif;
					?>
		
				</div><!-- #content --> 
			</div><!-- #primary -->
		</div><!-- #primary -->
		<?php if( $sidebar_blog == 'right' || $sidebar_blog == 'left' ) : ?>
			<div class="<?php echo esc_attr(wow_get_class()->class_sidebar_right); ?>">
				<?php dynamic_sidebar('sidebar-blog');?>
			</div>
		<?php endif; ?>				
	</div>
</div>
<?php
get_footer();
