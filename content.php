<?php global $wow_settings; ?>
<article id="post-<?php esc_attr(the_ID()); ?>" <?php post_class(); ?>>
	<?php if ( get_the_post_thumbnail() ){ ?>
		<div class="entry-thumb single-thumb">
			<?php wow_post_thumbnail(); ?>
		</div>
	<?php }; ?>	
	
	<div class="top-single-post">	
		<?php if (isset($wow_settings['post-author']) && $wow_settings['post-author']) {?>
			<span class="entry-meta-link"><?php esc_html_e( "Post by ", 'wow' )?><?php the_author_posts_link(); ?></span>
		<?php }?>				
		<span class="post-date">
			<?php echo ( get_the_title() ) ? date( 'F j, Y',strtotime($post->post_date)) : '<a href="'.esc_url(get_the_permalink()).'">'.date( 'F j, Y',strtotime($post->post_date)).'</a>'; ?>
		</span>
	</div>
	<?php if ( is_search() ) : ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="post-content">
		
		<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && wow_categorized_blog() ) : ?>
		<div class="entry-meta hidden">
			<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'wow' ) ); ?></span>
		</div>
		<?php
			endif;

		?>
		<div class="post-excerpt">
			<?php
				the_content( sprintf(
					__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'wow' ),
					the_title( '<span class="screen-reader-text">', '</span>', false )
				) );

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'wow' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			?>
		</div>
		
		<div class="clearfix"></div>
		<!-- Tag -->
		<?php if(get_the_tag_list()) { ?>
		<div class="entry-tag single-tag">
			<?php echo get_the_tag_list('<span class="custom-font title-tag">Tags: </span>',' , ','');  ?>
		</div>
		<?php } ?>
		
		<!-- Social -->
		<?php wow_add_social(); ?>
		
		<?php 
            $comment_count = $post->comment_count;
            if($comment_count > 1) {
        ?>
            <span class="entry-comment">
                <?php echo $post->comment_count .'<span>'. esc_html__(' Comments', 'wow').'</span>'; ?>
            </span>
        <?php } ?>
		
		<div class="entry-header hidden">
			<div class="entry-meta">
				<?php
					if ( 'post' == get_post_type() )
						wow_posted_on();

					if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
				?>
				<span class="comments-link"><?php comments_popup_link( esc_html__( 'Leave a comment', 'wow' ), esc_html__( '1 Comment', 'wow' ), esc_html__( '% Comments', 'wow' ) ); ?></span>
				<?php
					endif;

					edit_post_link( esc_html__( 'Edit', 'wow' ), '<span class="edit-link">', '</span>' );
				?>
			</div>
		</div>
	</div><!-- .entry-content -->
	<?php endif; ?>
</article><!-- #post-## -->
