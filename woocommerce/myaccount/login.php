<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
	<form action="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>" method="post" id="loginform-top" name="loginform-top">

		<?php do_action( 'woocommerce_login_form_start' ); ?>

		<p class="login-username">
			<label for="user_login"><?php esc_html_e( 'Username or email address', 'wow' ); ?> <span class="required">*</span></label>
			<input type="text" class="input input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
		</p>
		<p class="login-password">
			<label for="user_pass"><?php esc_html_e( 'Password', 'wow' ); ?> <span class="required">*</span></label>
			<input class="input input-text" type="password" name="password" id="password" />
		</p>

		<?php do_action( 'woocommerce_login_form' ); ?>
		<div class="links-more lost_password">
			<p><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'wow' ); ?></a></p>
			<p><a href="<?php echo esc_url(get_permalink( get_option('woocommerce_myaccount_page_id') )); ?>" title="<?php esc_attr__('login or register', 'wow'); ?>"><?php esc_html_e(' login or register ', 'wow'); ?></a></p>
		</div>
		<p class="login-submit">
			<?php wp_nonce_field( 'woocommerce-login' ); ?>
			<input type="submit" class="button" name="login" value="<?php esc_attr_e( 'Login', 'wow' ); ?>" />
		</p>
		

		<?php do_action( 'woocommerce_login_form_end' ); ?>

	</form>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
