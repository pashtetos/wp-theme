<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
	get_header(); ?>
	<?php	
		do_action( 'woocommerce_before_main_content' );
		$sidebar_product = wow_get_config('sidebar_product');
	?>
	<div class="container bin-main-content">
		<div class="row">
			<?php if($sidebar_product == 'left'):?>			
				<div class="<?php echo esc_attr(wow_get_class()->class_sidebar_left); ?>">
					<?php get_sidebar( 'product' );?>	
				</div>				
			<?php endif; ?>
			<div class="<?php echo esc_attr(wow_get_class()->class_product_content); ?>" >
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

				<?php endif; ?>

				<?php do_action( 'woocommerce_archive_description' ); ?>

				<?php if ( have_posts() ) : ?>
					<div class="content_sortPagiBar top clearfix">
						<?php
							/**
							 * woocommerce_before_shop_loop hook
							 *
							 * @hooked woocommerce_result_count - 20
							 * @hooked woocommerce_catalog_ordering - 30
							 */
							// remove_action('woocommerce_before_shop_loop','woocommerce_result_count', 20);
							add_action('woocommerce_before_shop_loop','wow_display_view', 10);
							add_action('woocommerce_before_shop_loop','woocommerce_result_count', 20);
							do_action( 'woocommerce_before_shop_loop' );
						?>
					</div>

					<?php woocommerce_product_loop_start(); ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php wc_get_template_part( 'content', 'product' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>
					<div class="content_sortPagiBar bottom clearfix">
						<?php
							/**
							 * woocommerce_after_shop_loop hook
							 *
							 * @hooked woocommerce_pagination - 10
							 */
							do_action( 'woocommerce_after_shop_loop' );
						?>
					</div>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php wc_get_template( 'loop/no-products-found.php' ); ?>

				<?php endif; ?>

			</div>
			<?php if($sidebar_product == 'right'):?>
				<div class="<?php echo esc_attr(wow_get_class()->class_sidebar_right); ?>">
					<?php get_sidebar( 'product' );?>	
				</div>	
			<?php endif; ?>
		</div>
	</div>
	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
<?php get_footer( 'shop' ); ?>