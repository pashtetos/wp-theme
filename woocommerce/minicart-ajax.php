<?php 
if ( !class_exists('Woocommerce') ) { 
	return false;
}
global $woocommerce; ?>
<div id="cart" class="dropdown mini-cart top-cart">
	<a class="dropdown-toggle cart-icon" data-toggle="dropdown" data-hover="dropdown" data-delay="0" href="<?php echo esc_url(home_url('/cart')); ?>" title="<?php esc_attr__('View your shopping cart', 'wow'); ?>">
		<span class="icon-shop"><i class="fa fa-shopping-bag"></i></span>
		<?php echo sprintf(_n(' <span class="mini-cart-items"> %d</span> ', ' <span class="mini-cart-items"> %d</span> ', $woocommerce->cart->cart_contents_count, 'wow'), $woocommerce->cart->cart_contents_count);?>
    </a>
	<div class="cart-popup">
		<?php woocommerce_mini_cart(); ?>
	</div>
</div>