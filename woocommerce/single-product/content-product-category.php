<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;
$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'blog-thumbnails' );

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = '';
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes = 'last';

$columns = 12/$woocommerce_loop['columns']
?>

<div <?php post_class('product-container '.$classes.' col-lg-'.$columns.' col-md-'.$columns.' col-sm-'.$columns.' col-xs-12' ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<div class='product-image-wrapper'>
		<div class='product-content-image'>
			<?php 
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 */
			woocommerce_show_product_loop_sale_flash();
			?>
			<a title="<?php esc_attr(the_title()); ?>" href="<?php echo (get_option( 'woocommerce_enable_lightbox' )=='yes' && is_product()) ? esc_url($image_attributes[0]) : esc_url(the_permalink()); ?>" 
			   class="product-image <?php echo (get_option( 'woocommerce_enable_lightbox' )=='yes' &&  is_product())?'zoom':'' ;?>">
			<?php
				/**
				 * woocommerce_before_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);
                do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			</a>
		</div>
		<div class='product-button'>
			<?php do_action( 'woocommerce_single_product_summary' ); ?>
		</div>

	</div>
	
	<div class='product-details'>
		<h4 class='name'><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h4>
		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
	</div>
	

</div>