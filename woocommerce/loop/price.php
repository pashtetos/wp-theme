<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;
?>

<?php if ( $price_html = $product->get_price_html() ) {
	$wpoEngine_price = preg_split("/<ins>/", $price_html);
?>
<?php if(count($wpoEngine_price) > 1) { ?>
	<div class="price old-price">
		<?php if(isset($wpoEngine_price[1])) echo ('<ins>' . $wpoEngine_price[1]); ?>
		<?php if(isset($wpoEngine_price[0])) echo ($wpoEngine_price[0]); ?>
		
	</div>
	<?php }else{ ?>
	<div class="price"><?php echo $price_html; ?></div>
	<?php } ?>

<?php }else{ 
	echo '<div class="price empty"><span class="amount"></span></div>';
}?>