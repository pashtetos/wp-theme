<?php
/**
 * Displayed when no products are found matching the current query
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/no-products-found.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="row">
	<div class="col-xs-12 col-sm-3"></div>
	<div class="col-xs-12 col-sm-6">
		<h1 class="title-notfound"><?php echo esc_html__('Nothing Found','wow') ?></h1>
		<div class="alert alert-warning alert-dismissible" role="alert">
			<p><?php esc_html_e('No products were found matching your selection..', 'wow'); ?></p>
		</div>
		<div class="bin-search-form">		
			<?php if ( class_exists( 'WooCommerce' ) ) {?>
				<?php get_template_part( 'search-form' ); ?>
			<?php }else{ ?>
				<?php get_search_form(); ?>
			<?php } ?>	
		</div>
	</div>
	<div class="col-xs-12 col-sm-3"></div>
</div>
