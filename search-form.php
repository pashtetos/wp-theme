<form role="search" method="get" id="searchform" class="search-from" action="<?php echo esc_attr(home_url( '/' )); ?>" >
	<div class="container">
		<div class="form-content">
			<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="s" placeholder="<?php echo esc_attr__( 'Enter Your Search', 'wow' ); ?>" />
			<button id="searchsubmit" class="btn" type="submit">
				<i class="fa fa-search"></i>
				<span><?php echo esc_html__( 'Search', 'wow' ); ?></span>
			</button>
		</div>
	</div>
	<input type="hidden" name="post_type" value="product" />
</form>