<?php 
	get_header();
	global $wow_settings;
	$sidebar_blog = wow_get_config('sidebar_blog', 'left');
	$blog_layout = wow_get_config('blog_layout','grid');
	$class_content_blog = ($blog_layout == 'grid') ? 'blog-content-grid' : 'blog-content-list';
?>
<?php if ((!isset($wow_settings) && !$wow_settings)) { ?>
	<div class="container">
		<h1 class="bin-title-default"> <?php if(!is_home()){ the_title(); }else{ echo esc_html_e('Latest Posts', 'wow'); } ?> </h1>
	</div>
<?php } ?>
<div class="container bin-main-content">
	<div class="row">
			
			<div class="cate-post-content <?php if(is_active_sidebar('sidebar-blog')){ echo esc_attr(wow_get_class()->class_blog_content); }else{ echo "col-xs-12"; } ?> <?php if(is_active_sidebar('sidebar-blog') && $sidebar_blog == 'left'){ echo "pull-right"; } ?> ">
				<section id="primary" class="content-area">
					<div id="content" class="site-content <?php echo esc_attr($class_content_blog);?>" role="main">

						<?php if ( have_posts() ) : ?>

						<?php
								// Start the Loop.
								while ( have_posts() ) : the_post();

								/*
								 * Include the post format-specific template for the content. If you want to
								 * use this in a child theme, then include a file called called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								get_template_part( 'content', $blog_layout);

								endwhile;
								
								// Previous/next page navigation.							
								wow_paging_nav();

							else :
								// If no content, include the "No posts found" template.
								get_template_part( 'content', 'none' );

							endif;
						?>
					</div><!-- #content -->
				</section><!-- #primary -->
	
            </div>

			<?php if(is_active_sidebar('sidebar-blog') && $sidebar_blog == 'left'):?>			
				<div class="<?php echo esc_attr(wow_get_class()->class_sidebar_left); ?> pull-left">
					<?php dynamic_sidebar('sidebar-blog');?>	
				</div>				
			<?php endif; ?>			
			
            <?php if(is_active_sidebar('sidebar-blog') && $sidebar_blog == 'right'):?>			
				<div class="<?php echo esc_attr(wow_get_class()->class_sidebar_right); ?>">
					<?php dynamic_sidebar('sidebar-blog');?>	
				</div>				
			<?php endif; ?>
            
    </div>
</div>

<?php
get_footer();
