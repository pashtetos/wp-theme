<?php 
	global $wow_settings; 
	$repeat = isset($wow_settings['background_repeat']) && $wow_settings['background_repeat'] ? $wow_settings['background_repeat'] : "no-repeat";
	$layout = isset($wow_settings['layout']) && $wow_settings['layout'] ? $wow_settings['layout'] : "";
	$bg_image_box =	isset($wow_settings['background_box_img']) ? $wow_settings['background_box_img'] : "";
	$background_color = isset($wow_settings['background_color']) ? $wow_settings['background_color'] : "";
	
	function wow_hex2rgb($hex) {
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3) {
		  $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		  $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		  $b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
		  $r = hexdec(substr($hex,0,2));
		  $g = hexdec(substr($hex,2,2));
		  $b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		return implode(",", $rgb);
	}
?>

<?php if($background_color) : ?>
	body{
		background-color: <?php echo $background_color; ?> ;
	}
<?php endif; ?>

<?php if( (isset($bg_image_box['url']) && $bg_image_box['url']) && ($layout == 'boxed' )) {?>
	body.box-layout{
	<?php if( $layout == 'boxed' && $bg_image_box['url']) : ?>
		background-image: url("<?php echo esc_html( $bg_image_box['url'] ) ?>");
		background-position: top center; 
		background-attachment: fixed;
		background-size: cover;
		background-repeat: <?php echo esc_html( $repeat )?>;	
	<?php endif; ?>
	}
<?php }else{ ?>
	body.box-layout.page-id-28{
		background: #f3f3f3;			
	}
<?php } ?>

<?php 
	$background_img =  isset($wow_settings['background_img']) ? $wow_settings['background_img'] : "";
	if( ( isset($background_img['url']) && $background_img['url']) && ($layout != 'boxed' ) ) { ?>
	body{
		background-image: url("<?php echo esc_html( $background_img['url'] ); ?>");
		background-repeat: <?php echo esc_html( $repeat )?>;
	}
	
<?php }else{ ?>
	body.page-id-28{
		background: #f3f3f3;			
	}
<?php } ?>

<?php 		
	$config_fonts = wow_config_font();
	foreach($config_fonts as $key=>$font){
		if($key != 'family_font_custom' && $key != 'class_font_custom'){
			$key = str_replace("family_font_","",$key);
			$key = str_replace("-font","",$key);
			$family_font_body = (isset($font['font-family']) && $font['font-family']) ? $font['font-family'] : '';
			if($family_font_body) : ?>
				<?php echo esc_html($key); ?>
				{
					font-family: <?php echo esc_html($family_font_body) ?> ; 
					font-size:<?php echo esc_html($font['font-size']) ?>; 
					font-weight:<?php echo esc_html($font['font-weight']) ?>; 
				}	
			<?php elseif($key == 'family_font_custom') : 
				$family_font_custom = (isset($config_fonts['family_font_custom']['font-family']) && $config_fonts['family_font_custom']['font-family']) ? ($config_fonts['family_font_custom']['font-family']) : '';
				?>
				<?php if($family_font_custom): ?>
					<?php echo html_entity_decode($config_fonts['class_font_custom']); ?>
					{
						font-family:	<?php echo esc_html($family_font_custom) ?> ; 
						font-size:	<?php echo esc_html($config_fonts['family_font_custom']['font-size']) ?>; 
						font-weight:<?php echo esc_html($config_fonts['family_font_custom']['font-weight']) ?>;	
					}		
			<?php endif; ?>
		<?php endif; ?>
	<?php } ?>
<?php } ?>
<?php $main_theme_color = (isset($wow_settings['main_theme_color']) && $wow_settings['main_theme_color'] ) ? $wow_settings['main_theme_color'] : "#f9b61e"; ?>
<?php if ($main_theme_color) : ?>
<?php	$maincolor_rgb = wow_hex2rgb ($main_theme_color); ?>
.page-id-28 #cart .mini-cart-items {
    background: <?php echo esc_html($main_theme_color) ?>;
}
#cart .cart-popup .buttons .button.checkout {
    background: <?php echo esc_html($main_theme_color) ?>;
}
.onsale{
	background: <?php echo esc_html($main_theme_color) ?>;	
}

#bin-topbar .login-top button[type="button"].bin-btn{
	background: <?php echo esc_html($main_theme_color) ?>;	
}
#bin-topbar .sign-up-top a{
	border: 1px solid <?php echo esc_html($main_theme_color) ?>;
	color: <?php echo esc_html($main_theme_color) ?>;
}
#bin-topbar .sign-up-top a:hover{
	background: <?php echo esc_html($main_theme_color) ?>;
}
a:hover,
.products-list.grid .product-wapper .products-content .yith-wcwl-wishlistaddedbrowse > a::before,
.products-list.grid .product-wapper .products-content .yith-wcwl-wishlistexistsbrowse > a::before,
.wrapper-benner.default .content-benner .btn-banner,
.bin-woo-slider .owl-buttons .carousel-control:hover > i,
.left-content-category .count-comment span.number,
.star-rating span:before,
.products-list.grid .product-wapper .products-content .price > span,
.products-list.grid .product-wapper .products-content h3:hover a,
.left-content-category .count-product span.number,
.left-content-category .count-category span.number,
#cart .cart-popup .mini_cart_item .remove:hover,
#cart .icon-shop:hover,
.wrapper-benner.layout1 .content-benner .title-banner h2,
.wrapper-benner.layout1 .content-benner .number-banner,
.wrapper-benner.layout1 .content-benner .btn-banner,
.products-list.grid .product-wapper .products-content .yith-wcwl-add-to-wishlist .add_to_wishlist:hover::before,
.products-list.grid .product-wapper .products-content .ajax_add_to_cart.added::before,
.products-list.grid .product-wapper .products-content .ajax_add_to_cart:hover::before,
.products-list.grid .product-wapper .products-content .product_type_grouped.added::before,
.products-list.grid .product-wapper .products-content .product_type_grouped:hover::before,
.products-list.grid .product-wapper .products-content .add_to_cart_button.added::before,
.products-list.grid .product-wapper .products-content .add_to_cart_button:hover::before,
.products-list.grid .product-wapper .products-content .price > ins,
.products-list.grid .product-wapper:hover .products-content .product_type_grouped::before,
.products-list.grid .product-wapper .products-thumb .quickview:hover::before,
.bin-header .header-content .binAccount .menu li a:hover,
.bin-navigation ul > li.level-0 .sub-menu li.level-1 .sub-menu a:hover,
.bin-navigation ul > li.level-0.mega-menu .sub-menu li.level-1 > a:hover,
.bin-navigation ul > li.level-0 .sub-menu li.level-1 a:hover,
.home .bin-header .header-content .search-box:hover,
.home .bin-header .bin-navigation ul > li.level-0 > a:hover,
#bin-topbar .list-socials-link li a:hover,
#bin-footer.footer-1 .section-footer1-2 .social-network li a:hover,
#bin-footer.footer-1 .section-footer1-1 .menu li a:hover,
#bin-footer.footer-1 .section-footer1-2 .bin-wraper-copyright .icon-cpy:hover span i,
#bin-footer.footer-1 .section-footer1-2 .bin-wraper-copyright .icon-cpy .info-box .bottom-info p,
#bin-footer.footer-1 .section-footer1-2 .bin-wraper-copyright .icon-cpy .info-box .info h3,
.bin-navigation ul > li.level-0.current_page_item > a,
.bin-header .header-content .search-box:hover,
.bin-breadcrumb a:hover,
.bin-header .bin-navigation ul > li.level-0 > a:hover,
.heading-box h2 span,
.bin-ourteam .ourteam-info .ourteam-customer-name:hover,
.bin-ourteam .socials ul li a:hover,
#bin-footer.footer-2 .email-box .hot-line span,
#bin-footer.footer-2 .email-box h3,
#bin-footer.footer-2 .address-box h3,
#bin-footer.footer-2 .widget .menu li:hover a,
.carousel-control:hover i,
.bin-recent-post.layout-2 .post-content .entry-title a:hover,
.bin-recent-post.layout-2 .post-content time span,
.wrapper-benner.layout3 .content-benner .title-banner h2,
.products-list.grid .product-wapper .products-content .yith-wcwl-wishlistaddedbrowse > a::before,
.products-list.grid .product-wapper .products-content .yith-wcwl-wishlistexistsbrowse > a::before,
#bin-footer.footer-1 .section-footer1-2 .bin-wraper-copyright .text-cpy a:hover,
.bin-woo-countdown-slider.bin-woo-countdown-default .item-countdown span.text,
.woo-no-slider .item-product-cat .item-title a:hover,
.slider-home-2 .tparrows:hover::before,
.bin-header.header-2 .categories-menu .menu > li.level-0 > a:hover,
.bin-header.header-2 .email-box span span,
.bin-header.header-2 .top-header .binAccount .menu li a:hover,
.bin-header.header-2 .search-box button[type="submit"]:hover,
.content_sortPagiBar .display li a.active,
.content_sortPagiBar .display li a:hover,
.bin-sidebar .widget.widget_layered_nav ul li a:hover,
.bin-sidebar .widget.widget_layered_nav .product-categories li a:hover,
.bin-sidebar .widget.widget_archive ul li a:hover,
.bin-sidebar .widget.widget_archive .product-categories li a:hover,
.bin-sidebar .widget.widget_categories ul li a:hover,
.bin-sidebar .widget.widget_categories .product-categories li a:hover,
.bin-sidebar .widget.widget_product_categories ul li a:hover,
.bin-sidebar .widget.widget_product_categories .product-categories li a:hover,
.bin-sidebar .widget.widget_price_filter ul li a:hover,
.bin-sidebar .widget.widget_price_filter .product-categories li a:hover,
.bin-sidebar .widget.bin_brand ul li a:hover,
.bin-sidebar .widget.bin_brand .product-categories li a:hover,
.bin-header.header-2 .bin-navigation-active .remove-megamenu::before,
.widget_product_categories ul li.cat-parent .arrow:hover::before,
.widget_product_categories ol li.cat-parent .arrow:hover::before,
.bin_best_seller .content-best-seller .item-content h4 a:hover,
.bin_best_seller .content-best-seller .item-content span,
.bin_best_seller .content-best-seller .item-content > p > ins span,
.bin_best_seller .content-best-seller .item-content span span,
.products-list.list .product-wapper .products-thumb .quickview::before,
.products-list.list .product-wapper .products-content .price > span,
.products-list.list .product-wapper .products-content .price > ins,
.products-list.list .product-wapper .products-content .product_type_external,
.products-list.list .product-wapper .products-content .product_type_grouped,
.products-list.list .product-wapper .products-content .add_to_cart_button,
.products-list.list .product-wapper .products-content .yith-wcwl-add-to-wishlist .add_to_wishlist::before,
.bin-woo-countdown-slider.bin-woo-countdown-default .item-countdown .product-countdown .countdown-content .countdown-section::before,
#bin-main .widget.widget_search .form-content #searchsubmit:hover,
.shop_table .product-subtotal span,
.cate-post-content .grid-post .post-date .days,
.cate-post-content .grid-post .post-content .entry-title a:hover,
.bin-recent-post.layout-1 .wrapper-recent-post .post-content .entry-title a:hover,
.bin-recent-post.layout-1 .wrapper-recent-post .post-content .entry-date time span,
.navigation.paging-navigation .page-numbers:hover,
.head-background-dark .bin-breadcrumb, .head-background-dark .bin-breadcrumb a,
.head-background-dark #bin-main .page-title h1,
.woocommerce-tabs .star-rating span:before,
.bin-policies li,
.quickview-container .tagged_as a:hover,
.quickview-container .posted_in a:hover,
.quickview-container .sku_wrapper a:hover,
.quickview-container .cart .button,
.quickview-container .yith-wcwl-add-to-wishlist .add_to_wishlist::before,
.products-list.list .product-wapper .products-content .compare::before,
#bin-topbar .login-top .modal-dialog .bin-modal-body p a:hover,
#bin-topbar .login-top .modal-dialog .bin-modal .bin-modal-head .close,
.post-single .top-single-post .entry-meta-link a:hover,
.products-list.list .product-wapper .products-content h3 a:hover,
.quickview-container .price ins span,
.quickview-container .price,
.widget_product_categories ul li a:hover,
.widget_product_categories ol li a:hover,
.single-product .BoxInfo .product_meta .sku_wrapper a:hover,
.single-product .BoxInfo .product_meta .posted_in a:hover,
.single-product .BoxInfo .product_meta .tagged_as a:hover,
.single-product .BoxInfo .price,
.woocommerce-checkout .order-total .woocommerce-Price-amount,
.bin-portfolio .portfolio-tab ul li.selected,
.woocommerce-account .woocommerce-MyAccount-content a,
.bin-portfolio .portfolio-tab ul li:hover,
.bin-recent-post.layout-3 article .post-content .entry-title a:hover,
.single-product .BoxInfo .social-icon .social-share a:hover::before,
.bin-portfolio .portfolio-container .portfolio-item-inner:hover .pitem-text h3 a:hover,
.single-product .entry-summary .price ins span,
#bin-footer.footer-3 .section-footer3-2 .menu li a:hover,
.entry-title:hover a,
#bin-footer.footer-3 .bingo-newsletter-3 .content-newsletter input[type="submit"]:hover,
.products-list.grid .product-wapper .products-content .price,
#bin-topbar .topbar-menu .widget-title:hover{
	color: <?php echo esc_html($main_theme_color) ?>;
}

.bin-breadcrumb,
.bin-breadcrumb a,
.archive.woocommerce.head-background-dark .bin-breadcrumb,
.archive.woocommerce.head-background-dark .bin-breadcrumb a,
.archive.woocommerce.head-background-dark #bin-main .page-title h1,
.products-list.grid .product-wapper .products-content .button:hover::before,
.woocommerce-cart .cart_totals h2,
.woocommerce-cart .woocommerce .product-subtotal span,
.woocommerce-cart .woocommerce .product-price span,
.woocommerce-cart .woocommerce > form .shop_table .cart_item .product-remove a.remove,
.shop_table .product-subtotal span,
.shop_table .product-price span,
.woocommerce-wishlist .woocommerce table.wishlist_table tbody tr td a.remove.remove_from_wishlist,
#bin-main .page-title h1,
.woocommerce-wishlist .woocommerce table.wishlist_table tbody tr td.product-name a:hover,
.woocommerce-checkout form.woocommerce-checkout .border-box table.shop_table > tbody > tr > td .product-quantity,
#order_review .order-total span,
.woocommerce-checkout form.woocommerce-checkout .border-box #order_review_heading,
.checkout.woocommerce-checkout label .required,
.woocommerce-checkout .woocommerce .woocommerce-info > a,
.cate-post-content .list-post .post-content .entry-title a:hover,
.cate-post-content .list-post .post-date .days,
.woocommerce .woocommerce-MyAccount-content > p:first-child a:hover,
.woocommerce .woocommerce-MyAccount-content p a:hover,
.woocommerce .woocommerce-MyAccount-navigation ul li.is-active a,
.woocommerce .woocommerce-MyAccount-navigation ul li a:hover,
.quickview-container .slider_img_productd .carousel-control:hover,
.related .product-button .button:before,
.products-list.grid .product-wapper .products-content .price span,
.quickview-container .cart .button:hover,
#bin-topbar .sign-out-top a,
.bin-header.header-4 #cart .icon-shop:hover,
.bin-header.header-4 .binAccount:hover,
.bin-header .header-content .binAccount:hover > .bin-icon i,
.bin-header.header-4 .bingo-menu-wrapper .bin-navigation ul > li.level-0.current-menu-parent .current-menu-item > a,
.bin-header.header-4 .bingo-menu-wrapper .bin-navigation ul > li.level-0.current-menu-parent > a,
.bin-header.header-3 .bingo-menu-wrapper .bin-navigation ul > li.level-0.current-menu-parent .current-menu-item > a,
.bin-header.header-3 .bingo-menu-wrapper .bin-navigation ul > li.level-0.current-menu-parent > a,
.bin-woo-container-slider.bin-woo-container-slider--2 .wrap-bg:hover .item-title a,
.bin-woo-tab-cat.bin-woo-tab-cat--1 .top-tab-slider .nav-tabs li.active a,
.bin-woo-tab-cat.bin-woo-tab-cat--1 .top-tab-slider .nav-tabs li:hover a,
.bin-policy.bin-policy--2 .bin-policy-link:hover,
#bin-footer.footer-4 .section-footer4-2 .bin-social-network ul li a:hover,
#bin-footer.footer-4 .menu li a:hover,
#bin-footer.footer-2 .social-network li a:hover{
	color: <?php echo esc_html($main_theme_color) ?>;	
}

.quickview-container .social-icon .social-share a:hover,
.post-single .social-icon .social-share a:hover,
#respond .comment-form .form-submit input,
.wowtooltip,
.back-top:hover,
.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .yith-wcwl-add-to-wishlist .add_to_wishlist:hover::before,
.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .add_to_cart_button:hover,
#bin-main .widget.widget_search .form-content::before,
.bingo-newsletter-2 .content-newsletter input[type="submit"],
#cart .mini-cart-items,
.contact-1:hover figure,
#bin_slider_price,
.woocommerce form.login .form-row::before,
.woocommerce form.register .form-row::before,
.quickview-container .yith-wcwl-wishlistexistsbrowse > a::before,
.quickview-container .yith-wcwl-wishlistaddedbrowse > a::before,
.single-product .BoxInfo .single_add_to_cart_button,
.widget.widget_categories ul li > a:hover::before,
.tagcloud a:after,
.products-list.list .product-wapper .products-content .product_type_variable,
.shop_table .quantity .plus:hover,
.shop_table .quantity .minus:hover,
.quickview-container .yith-wcwl-add-to-wishlist .add_to_wishlist:hover::before,
#bin-topbar .login-top .modal-dialog .bin-modal-body input[type="submit"],
.cart-collaterals .wc-proceed-to-checkout a,
.bin-header.header-2 .top-header,
.products-list.list .product-wapper .products-content .product_type_external.added,
.products-list.list .product-wapper .products-content .product_type_external:hover,
.products-list.list .product-wapper .products-content .product_type_grouped.added,
.products-list.list .product-wapper .products-content .product_type_grouped:hover,
.products-list.list .product-wapper .products-content .add_to_cart_button.added,
.products-list.list .product-wapper .products-content .add_to_cart_button:hover,
.products-list.list .product-wapper .products-content .yith-wcwl-wishlistexistsbrowse > a::before,
.products-list.list .product-wapper .products-content .yith-wcwl-wishlistaddedbrowse > a::before,
.products-list.list .product-wapper .products-content .yith-wcwl-add-to-wishlist .add_to_wishlist:hover::before,
.bin-header.header-2 .content-head .bingo-menu-wrapper .navbar-toggle:hover .icon-bar,
.bin-woo-countdown-slider.bin-woo-countdown-default .item-countdown .product-countdown .countdown-content > span .countdown-amount::before,
.bin-woo-countdown-slider.bin-woo-countdown-default .item-countdown .product-countdown .countdown-content > span,
.bin-woo-slider.slider-layout-2 .product .product-wapper .products-content .add-links-wrap .product-button .add_to_cart_button:hover,
.bin-woo-slider.slider-layout-2 .product .product-wapper .products-content .add-links-wrap .product-button .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a::before,
.bin-woo-slider.slider-layout-2 .product .product-wapper .products-content .add-links-wrap .product-button .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a::before,
.bin-recent-post.layout-2 .post-content .btn-read-more a:hover,
.left-content-category .title-block h2::before,
.wrapper-form input[type="submit"]:hover,
.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a::before,
.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a::before,
.products-list.list .product-wapper .products-thumb .quickview:hover::before,
.bingo-newsletter .content-newsletter input[type="submit"],
#bin-footer.footer-1 .section-footer1-1 .bin-btn-1 a,
.wrapper-benner.layout1 .content-benner .btn-banner:hover,
.wrapper-benner.default .content-benner .btn-banner:hover,
.vc_tta-container h2::before, .title-block h2::before,
.post-single .social-share ul li a:hover,
.woocommerce-page .return-to-shop a,
.products-list.list .product-wapper .products-content .compare.added::before,
.products-list.list .product-wapper .products-content .compare:hover::before,
.post-single .comment-respond .form-submit input:hover,
.woocommerce .woocommerce-MyAccount-content .address a:hover,
.woocommerce .woocommerce-MyAccount-content .edit-account .form-row::before,
.single-product .woocommerce-tabs .form-submit .submit,
.woocommerce .woocommerce-MyAccount-content input[name="save_account_details"],
.woocommerce-account #customer_login input[type="submit"],
.woocommerce-checkout .place-order input[type="submit"],
.shop_table .coupon input[type="submit"]:hover,
.bin-header.header-3,
.bin-woo-tab-cat .nav-tabs li > a:after,
.woocommerce-wishlist .woocommerce table.wishlist_table tbody .product-add-to-cart a,
.woocommerce-checkout .woocommerce .login .form-row input[type="submit"],
.woocommerce-checkout .checkout_coupon .form-row input[type="submit"],
.bin-navigation ul > li.level-0 .sub-menu li.level-1 .sub-menu a:hover:before,
.bingo-newsletter-2 .content-newsletter input[type="submit"]:hover,
.quickview-container .slider_img_productd .carousel-indicators li.active,
.bin-navigation ul > li.level-0.mega-menu .sub-menu li.level-1 > a:before{
	background-color: <?php echo esc_html($main_theme_color) ?>;
}

.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .ajax_add_to_cart,
.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .product_type_grouped,
.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .add_to_cart_button,
.woocommerce .woocommerce-MyAccount-content form .form-row::before,
.woocommerce .woocommerce-MyAccount-content input[name="save_address"],
.woocommerce form.register .form-row.form-row-wide::before,
.woocommerce form.login .form-row.form-row-wide::before,
.cate-post-content .list-post .post-content .post-btn .post-btn-more:hover,
.vgwc-featured,
#bin-footer.footer-4 .bingo-newsletter-4 .content-newsletter input[type="submit"],
.cate-post-content .grid-post .post-content .read-more:hover,
.single-product .BoxInfo .cart.variations_form > table tr td.value .reset_variations{
	background-color: <?php echo esc_html($main_theme_color) ?>;
}
.bin-woo-slider.slider-layout-2 .item-product .product-wapper .products-content .add-links-wrap .product-button .add_to_cart_button{
	background: #252525;
}
#cart .cart-popup,
.bin-policies li,
.tagcloud a:hover,
.section5-home-2 div.wpcf7-mail-sent-ok,
.section5-home-2 div.wpcf7-validation-errors,
.woocommerce .woocommerce-MyAccount-navigation,
.shop_table .coupon input[type="submit"],
.woocommerce-cart .woocommerce > form .shop_table .cart_item .product-thumbnail a img,
.woocommerce-cart .woocommerce .cart_totals,
.woocommerce-cart .cart_totals .shop_table tr,
.bin-breadcrumb .delimiter,
.shop_table input[name="update_cart"],
.woocommerce-checkout .woocommerce .login,
.woocommerce-checkout .checkout_coupon,
.woocommerce-checkout form.woocommerce-checkout .border-box table.shop_table > thead > tr > th,
.woocommerce-checkout form.woocommerce-checkout .border-box,
.checkout.woocommerce-checkout input.input-text:focus,
.quickview-container .slider_img_productd .carousel-indicators li,
.contents-detail .images .image-additional .owl-stage > div .img-thumbnail.active,
.contents-detail .images .image-additional .owl-stage > div .img-thumbnail:hover,
#bin-topbar .login-top .modal-dialog .bin-modal-body p input[type="password"]:focus,
#bin-topbar .login-top .modal-dialog .bin-modal-body p input[type="text"]:focus,
.head-background-dark .bin-breadcrumb .delimiter,
.bin-header.header-2 .top-header .binAccount .menu,
.bingo-newsletter-2 .content-newsletter input[type="email"],
#cart .cart-popup .mini_cart_item:hover,
.woocommerce-checkout .woocommerce .woocommerce-info,
.wrapper-benner.layout1 .content-benner .btn-banner,
#yith-wcwl-popup-message,
.title-block-2 h2::before,
.bin-woo-container-slider.bin-woo-container-slider--2 .wrap-bg:hover,
.bin-policy.bin-policy--2 .bin-policy-link:hover,
.bin-woo-slider.slider-layout--4 .view-all:hover,
#bin-footer.footer-3 .bin-social-network ul li a:hover,
.archive.woocommerce.head-background-dark .bin-breadcrumb .delimiter,
.woocommerce-wishlist .woocommerce table.wishlist_table tr td.product-thumbnail img:hover,
.bin-header .header-content .binAccount .menu{
	border-color: <?php echo esc_html($main_theme_color) ?>
}

.bin-recent-post.layout-default .post:hover .entry-title,
.bin-woo-categories-container .content-category .item-product-cat .wrapper .item-image a::before,
.bin-woo-categories-container .content-category .item-product-cat .wrapper:hover .item-image a::before,
.bin-woo-categories-container .content-category .item-product-cat .wrapper .item-title{
	background-color: rgba(<?php echo esc_html($maincolor_rgb); ?>, 0.7);
}
.bin-instagram.layout-1 .instagram::before,
.bin-woo-container-slider.bin-woo-container-slider--1 a::before,
.bin-recent-post.layout-default .post-content time{
	background-color: rgba(<?php echo esc_html($maincolor_rgb); ?>, 0.75);
}
.search-overlay{
	background-color: rgba(<?php echo esc_html($maincolor_rgb); ?>, 0.95);
}

#bin_slider_price	.ui-slider-handle{
	color: <?php echo esc_html($main_theme_color) ?> !important;
	border-color: <?php echo esc_html($main_theme_color) ?> !important;
}
#bin_slider_price,
#button-price-slider{
	background-color: <?php echo esc_html($main_theme_color) ?> !important;		
}
#bin_form_filter_product .bin-filter li span:hover, #bin_form_filter_product .bin-filter span.active{
	border-color: <?php echo esc_html($main_theme_color) ?> !important;	
}
.woocommerce.widget_product_categories ul.product-categories li a:hover:before{
	background-color: <?php echo esc_html($main_theme_color) ?> !important;	
}
.cate-post-content .grid-post .post-content .read-more,
.cate-post-content .list-post .post-content .post-btn .post-btn-more,
.single-product .woocommerce-tabs ul.tabs li.active a,
.bin-header.header-2 .bin-navigation ul > li.level-0:hover > a,
.bin-recent-post.layout-2 .post-content .btn-read-more a{
	border-color: <?php echo esc_html($main_theme_color) ?>;
	color: <?php echo esc_html($main_theme_color) ?>;
}
.cate-post-content .grid-post .post-content .read-more:hover{
	color: #fff;
}
.woocommerce .woocommerce-Message .woocommerce-Button:hover,
.single-product .BoxInfo .quantity.buttons_added .plus:hover,
.single-product .BoxInfo .quantity.buttons_added .minus:hover,
.single-product .BoxInfo .yith-wcwl-add-button:hover a,
.post-single .nav-links a:hover,
.single-product .BoxInfo .yith-wcwl-wishlistaddedbrowse a,
.single-product .BoxInfo .yith-wcwl-wishlistexistsbrowse a,
.single-product .BoxInfo .compare:hover,
.bin-ourteam .carousel-control:hover,
.bin-ourteam .carousel-control:focus{
	background-color: <?php echo esc_html($main_theme_color) ?> !important;
	border-color: <?php echo esc_html($main_theme_color) ?>;	
}
.wowtooltip:after{
	border-top-color: <?php echo esc_html($main_theme_color) ?>;
}
.bin-ourteam .carousel-control:hover,
.bin-ourteam .carousel-control:focus,
.left-content-category > a:hover{
	background-color: <?php echo esc_html($main_theme_color) ?>;
	border-color: <?php echo esc_html($main_theme_color) ?>;	
}
.woocommerce-wishlist .woocommerce table.wishlist_table tbody tr td a.remove.remove_from_wishlist,
.navigation.paging-navigation .current,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li a:focus i,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li a:hover i,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li span:focus i,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li span:hover i,
.content_sortPagiBar .woocommerce-pagination .current,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li a:focus,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li a:hover,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li span:focus,
.content_sortPagiBar .woocommerce-pagination ul.page-numbers li span:hover,
.text-2{
	color: <?php echo esc_html($main_theme_color) ?> !important;
}
.btn-slider1:hover{
	background-color: <?php echo esc_html($main_theme_color) ?> !important;
	border-color: <?php echo esc_html($main_theme_color) ?> !important;
}

<?php endif; ?>