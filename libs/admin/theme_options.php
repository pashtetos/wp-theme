<?php

/**
 * Wow Settings Options
 */

if (!class_exists('Redux_Framework_wow_settings')) {

    class Redux_Framework_wow_settings {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();
            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        function compiler_action($options, $css, $changed_values) {

        }

        function dynamic_section($sections) {

            return $sections;
        }

        function change_arguments($args) {

            return $args;
        }

        function change_defaults($defaults) {

            return $defaults;
        }

        function remove_demo() {

        }

        public function setSections() {

            $page_layouts = wow_options_layouts();
            $sidebars = wow_options_sidebars();
            $body_wrapper = wow_options_body_wrapper();
            $wow_header_type = wow_options_header_types();
			$wow_banners_effect = wow_options_banners_effect();
			
            // General Settings
            $this->sections[] = array(
                'icon' => 'el-icon-dashboard',
                'icon_class' => 'icon',
                'title' => esc_html__('General', 'wow'),
                'fields' => array(	
					array(
					  'id' => 'main_theme_color',
					  'type' => 'color',
					  'title' => esc_html__('Main Theme Color', 'wow'),
					  'subtitle' => esc_html__('Select a main color for your site.', 'wow'),
					  'default' => '#f9b61e',
					  'transparent' => false,
					),					
                    array(
                        'id'=>'layout',
                        'type' => 'image_select',
                        'title' => esc_html__('Body Wrapper', 'wow'),
                        'options' => $body_wrapper,
                        'default' => 'full'
                    ),
                    array(
                        'id'=>'show-loading-overlay',
                        'type' => 'switch',
                        'title' => esc_html__('Loading Overlay', 'wow'),
                        'default' => false,
                        'on' => esc_html__('Show', 'wow'),
                        'off' => esc_html__('Hide', 'wow'),
                    ),
                    array(
                        'id'=>'show-newletter',
                        'type' => 'switch',
                        'title' => esc_html__('Show Newletter Form', 'wow'),
                        'default' => false,
                        'on' => esc_html__('Show', 'wow'),
                        'off' => esc_html__('Hide', 'wow'),
                    ),
					array(
						'id' => 'background_newletter_img',
						'type' => 'media',
						'title' => esc_html__('Background Newletter Image', 'wow'),
						'url'=> true,
                        'readonly' => false,
						'sub_desc' => '',
	                    'default' => array(
                            'url' => get_template_directory_uri() . '/images/newslettermodal.png'
                        )
					),					
					array(
							'id' => 'back_active',
							'type' => 'switch',
							'title' => esc_html__('Back to top', 'wow'),
							'sub_desc' => '',
							'desc' => '',
							'default' => '1'// 1 = on | 0 = off
							),							
					array(
							'id' => 'direction',
							'type' => 'select',
							'title' => esc_html__('Direction', 'wow'),
							'options' => array( 'ltr' => 'Left to Right', 'rtl' => 'Right to Left' ),
							'default' => 'ltr'
						),
					array(
                        'id'=>'banners_effect',
                        'type' => 'image_select',
                        'full_width' => true,
                        'title' => esc_html__('Banner Effect', 'wow'),
                        'options' => $wow_banners_effect,
                        'default' => 'banners-effect-1'
                    ),	
					array(
							'id' => 'sidebar_left_expand',
							'type' => 'select',
							'title' => esc_html__('Left Sidebar Expand', 'wow'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12', 
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'default' => '3',
							'sub_desc' => esc_html__( 'Select width of left sidebar.', 'wow' ),
						),
					
					array(
							'id' => 'sidebar_right_expand',
							'type' => 'select',
							'title' => esc_html__('Right Sidebar Expand', 'wow'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'default' => '3',
							'sub_desc' => esc_html__( 'Select width of right sidebar medium desktop.', 'wow' ),
					),
					array(
						'id' => 'background_box_img',
						'type' => 'media',
						'title' => esc_html__('Background Box Image', 'wow'),
						'sub_desc' => '',
						'default' => ''
					),							
					array(
						'id' => 'background_img',
						'type' => 'media',
						'title' => esc_html__('Background Image', 'wow'),
						'sub_desc' => '',
						'default' => ''
					),
					array(
						'id' => 'background_repeat',
						'type' => 'switch',
						'title' => esc_html__('Background Repeat', 'wow'),
						'sub_desc' => '',
						'desc' => '',
						'default' => '1'// 1 = on | 0 = off
					),
					array(
						'id' => 'responsive_support',
						'type' => 'switch',
						'title' => esc_html__('Responsive Support', 'wow'),
						'sub_desc' => esc_html__( 'Support reponsive layout, if you do not want to use this function, please uncheck.', 'wow' ),
						'desc' => '',
						'default' => '1'// 1 = on | 0 = off
					),							
                )
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                //'subsection' => true,
                'title' => esc_html__('Logo, Icons', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'sitelogo',
                        'type' => 'media',
                        'compiler'  => 'true',
                        'mode'      => false,
                        'title' => esc_html__('Logo', 'wow'),
						'desc'      => esc_html__('Upload Logo image default here.', 'wow'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/logo/logo.png'
                        )
                    ),

                    array(
                        'id'=>'sitelogo_white',
                        'type' => 'media',
                        'compiler'  => 'true',
                        'mode'      => false,
                        'title' => esc_html__('White Logo', 'wow'),
						'desc'      => esc_html__('Upload White Logo image default here.', 'wow'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/logo/logo-white.png'
                        )
                    ),
 
                    array(
                        'id'=>'sitelogo_dark',
                        'type' => 'media',
                        'compiler'  => 'true',
                        'mode'      => false,
                        'title' => esc_html__('Dark Logo', 'wow'),
						'desc'      => esc_html__('Upload White Logo image default here.', 'wow'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/logo/logo_dark.png'
                        )
                    ), 
 
                    array(
                        'id'=>'sticky-logo',
                        'type' => 'media',
                        'compiler'  => 'true',
                        'mode'      => false,
                        'title' => esc_html__('Logo in Sticky Menu', 'wow'),
                        'desc' => esc_html__('if header type is like 1, 4, 13, 14', 'wow'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/logo/logo-white.png'
                        )						
                    ),
                    array(
                        'id'=>'logo-width-sticky',
                        'type' => 'text',
                        'title' => esc_html__('Logo Max Width in Sticky Header', 'wow'),
                        'default' => '80'
                    ),
                    array(
                        'id'=>'favicon',
                        'type' => 'media',
                        'compiler'  => 'true',
                        'mode'      => false,
                        'title' => esc_html__('Favicon', 'wow'),
                        'desc'      => esc_html__('Upload favicon image default here.', 'wow'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/logo/favicon.ico'
                        )
                    ),
                )
            );

            //Skin
            $this->sections[] = array(
                'icon' => 'el-icon-broom',
                'icon_class' => 'icon',
                'title' => esc_html__('Skin', 'wow'),
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Typography', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'select-google-charset',
                        'type' => 'switch',
                        'title' => esc_html__('Select Google Font Character Sets', 'wow'),
                        'default' => false,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'google-charsets',
                        'type' => 'button_set',
                        'title' => esc_html__('Google Font Character Sets', 'wow'),
                        'multi' => true,
                        'required' => array('select-google-charset','equals',true),
                        'options'=> array(
                            'cyrillic' => 'Cyrrilic',
                            'cyrillic-ext' => 'Cyrrilic Extended',
                            'greek' => 'Greek',
                            'greek-ext' => 'Greek Extended',
                            'khmer' => 'Khmer',
                            'latin' => 'Latin',
                            'latin-ext' => 'Latin Extneded',
                            'vietnamese' => 'Vietnamese'
                        ),
                        'default' => array('latin','greek-ext','cyrillic','latin-ext','greek','cyrillic-ext','vietnamese','khmer')
                    ),
                    array(
                        'id'=>'family_font_body',
                        'type' => 'typography',
                        'title' => esc_html__('Body Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#777777",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Source Sans Pro',
                            'font-size'=>'14px',
                            'line-height' => '22px'
                        ),
                    ),
                    array(
                        'id'=>'body-mobile-font',
                        'type' => 'typography',
                        'title' => esc_html__('Body Mobile Font', 'wow'),
                        'google' => false,
                        'subsets' => false,
                        'font-family' => false,
                        'font-weight' => false,
                        'text-align' => false,
                        'color' => false,
                        'font-style' => false,
                        'desc' => esc_html__('Will be change on mobile device(max width < 480).', 'wow'),
                        'default'=> array(
                            'font-size'=>'13px',
                            'line-height' => '20px'
                        ),
                    ),
                    array(
                        'id'=>'h1-font',
                        'type' => 'typography',
                        'title' => esc_html__('H1 Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Montserrat',
                            'font-size'=>'36px',
                            'line-height' => '44px'
                        ),
                    ),
                    array(
                        'id'=>'h2-font',
                        'type' => 'typography',
                        'title' => esc_html__('H2 Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'300',
                            'font-family'=>'Montserrat',
                            'font-size'=>'30px',
                            'line-height' => '40px'
                        ),
                    ),
                    array(
                        'id'=>'h3-font',
                        'type' => 'typography',
                        'title' => esc_html__('H3 Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Montserrat',
                            'font-size'=>'25px',
                            'line-height' => '32px'
                        ),
                    ),
                    array(
                        'id'=>'h4-font',
                        'type' => 'typography',
                        'title' => esc_html__('H4 Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Montserrat',
                            'font-size'=>'20px',
                            'line-height' => '27px'
                        ),
                    ),
                    array(
                        'id'=>'h5-font',
                        'type' => 'typography',
                        'title' => esc_html__('H5 Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'600',
                            'font-family'=>'Montserrat',
                            'font-size'=>'14px',
                            'line-height' => '18px'
                        ),
                    ),
                    array(
                        'id'=>'h6-font',
                        'type' => 'typography',
                        'title' => esc_html__('H6 Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Montserrat',
                            'font-size'=>'14px',
                            'line-height' => '18px'
                        ),
                    ),
					array(
                        'id'=>'family_font_custom',
                        'type' => 'typography',
                        'title' => esc_html__('Custom Font', 'wow'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'color' => false,
                        'default'=> array(
                            'color'=>"#777777",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Montserrat',
                            'font-size'=>'14px',
                            'line-height' => '22px'
                        ),
                    ),
					array(
							'id' => 'class_font_custom',
							'type' => 'text',
							'title' => esc_html__('Custom Class', 'wow'),
							'sub_desc' => esc_html__( 'Example : .product_title .', 'wow' ), 
							'default' => '.product_title'
					)					
                )
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Custom CSS', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'css-code',
                        'type' => 'ace_editor',
                        'title' => esc_html__('CSS Code', 'wow'),
                        'subtitle' => esc_html__('Paste your CSS code here.', 'wow'),
                        'mode' => 'css',
                        'theme' => 'monokai',
                        'default' => ""
                    ),
                )
            );

            // Header Settings
            $this->sections[] = array(
                'icon' => 'el-icon-website',
                'icon_class' => 'icon',
                'title' => esc_html__('Header', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'show-header-top',
                        'type' => 'switch',
                        'title' => esc_html__('Show Header Top', 'wow'),
                        'default' => false,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'welcome-msg',
                        'type' => 'textarea',
                        'title' => esc_html__('Welcome Message', 'wow'),
                        'default' => ""
                    )
                )
            );			
			
            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Header Type', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'header_style',
                        'type' => 'image_select',
                        'full_width' => true,
                        'title' => esc_html__('Header Type', 'wow'),
                        'options' => $wow_header_type,
                        'default' => '1'
                    ),
                )
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Mini Cart', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'show-minicart',
                        'type' => 'switch',
                        'title' => esc_html__('Show Mini Cart', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    )
                )
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Search Form', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'show-searchform',
                        'type' => 'switch',
                        'title' => esc_html__('Show Search Form', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'search-cats',
                        'type' => 'switch',
                        'title' => esc_html__('Show Categories', 'wow'),
                        'required' => array('search-type','equals',array('post', 'product')),
                        'default' => false,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                )
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Sticky Header', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'enable-sticky-header',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Sticky Header', 'wow'),
                        'default' => false,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    )
                )
            );

            // Footer Settings
            $this->sections[] = array(
                'icon' => 'el-icon-website',
                'icon_class' => 'icon',
                'title' => esc_html__('Footer', 'wow'),
                'fields' => array(
                )
            );
	
			$footers = wow_get_footers();
            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Footer Style', 'wow'),
                'fields' => array(
					array(
						'id' => 'footer_style',
						'type' => 'image_select',
						'title' => esc_html__('Footer Style', 'wow'),
						'sub_desc' => esc_html__( 'Select Footer Style', 'wow' ),
						'desc' => '',
						'options' => $footers,
						'default' => '32'
					),
                )
            );

            // Blog
            $this->sections[] = array(
                'icon' => 'el-icon-file',
                'icon_class' => 'icon',
                'title' => esc_html__('Post', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'post-format',
                        'type' => 'switch',
                        'title' => esc_html__('Show Post Format', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'hot-label',
                        'type' => 'text',
                        'title' => esc_html__('"HOT" Text', 'wow'),
                        'desc' => esc_html__('sticky post label', 'wow'),
                        'default' => ''
                    )
                )
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Blog & Post Archives', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'sidebar_blog',
                        'type' => 'image_select',
                        'title' => esc_html__('Page Layout', 'wow'),
                        'options' => $page_layouts,
                        'default' => 'left'
                    ),
					array(
						'id' => 'blog_layout',
						'type' => 'button_set',
						'title' => esc_html__('Layout blog', 'wow'),
						'options' => array(
								'list'	=>  esc_html__( 'List', 'wow' ),
								'grid' =>  esc_html__( 'Grid', 'wow' )								
						),
						'default' => 'list',
						'sub_desc' => esc_html__( 'Select style layout blog', 'wow' ),
					),
					array(
						'id' => 'blog_col_large',
						'type' => 'button_set',
						'title' => esc_html__('Product Listing column Desktop', 'wow'),
						'required' => array('blog_layout','equals','grid'),
						'options' => array(
								'2' => '2',
								'3' => '3',
								'4' => '4',							
								'6' => '6'							
							),
						'default' => '4',
						'sub_desc' => esc_html__( 'Select number of column on Desktop Screen', 'wow' ),
					),
					array(
						'id' => 'blog_col_medium',
						'type' => 'button_set',
						'title' => esc_html__('Product Listing column Medium Desktop', 'wow'),
						'required' => array('blog_layout','equals','grid'),
						'options' => array(
								'2' => '2',
								'3' => '3',
								'4' => '4',							
								'6' => '6'							
							),
						'default' => '3',
						'sub_desc' => esc_html__( 'Select number of column on Medium Desktop Screen', 'wow' ),
					),
                    array(
                        'id'=>'archives-readmore',
                        'type' => 'switch',
                        'title' => esc_html__('Show Read More', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),										
                    array(
                        'id'=>'blog-excerpt',
                        'type' => 'switch',
                        'title' => esc_html__('Show Excerpt', 'wow'),
                        'default' => false,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'list-blog-excerpt-length',
                        'type' => 'text',
                        'required' => array('blog-excerpt','equals',true),
                        'title' => esc_html__('List Excerpt Length', 'wow'),
                        'desc' => esc_html__('The number of words', 'wow'),
                        'default' => '20',
                    ),
                    array(
                        'id'=>'grid-blog-excerpt-length',
                        'type' => 'text',
                        'required' => array('blog-excerpt','equals',true),
                        'title' => esc_html__('Grid Excerpt Length', 'wow'),
                        'desc' => esc_html__('The number of words', 'wow'),
                        'default' => '12',
                    ),					
                )
            );

            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Single Post', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'post-single-layout',
                        'type' => 'image_select',
                        'title' => esc_html__('Page Layout', 'wow'),
                        'options' => $page_layouts,
                        'default' => 'left'
                    ),
                    array(
                        'id'=>'post-title',
                        'type' => 'switch',
                        'title' => esc_html__('Show Title', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'post-author',
                        'type' => 'switch',
                        'title' => esc_html__('Show Author Info', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                )
            );
			
            // Page Title
            $this->sections[] = array(
                'icon' => 'el-icon-file',
                'icon_class' => 'icon',
                'title' => esc_html__('Page Title', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'page_title',
                        'type' => 'switch',
                        'title' => esc_html__('Show Page Title', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'page_title_bg',
                        'type' => 'media',
                        'url'=> true,
                        'readonly' => false,
                        'title' => esc_html__('Background', 'wow'),
                        'required' => array('page_title','equals', true),
	                    'default' => array(
                            'url' => get_template_directory_uri() . '/images/bg-breadcrum-white.png'
                        )						
                    ),
                    array(
                        'id' => 'breadcrumb',
                        'type' => 'switch',
                        'title' => esc_html__('Show Breadcrumb', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                        'required' => array('page_title','equals', true),
                    ),
                )
            );

            // Page 404
            $this->sections[] = array(
                'icon' => 'el-icon-error',
                'icon_class' => 'icon',
                'title' => esc_html__('404 Error', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'title-error',
                        'type' => 'text',
                        'title' => esc_html__('Title 404', 'wow'),
                        'desc' => esc_html__('Title Page 404', 'wow'),
                        'default' => 'Wow!'
                    ),
                    array(
                        'id'=>'not-found-error',
                        'type' => 'text',
                        'title' => esc_html__('Sub title 404', 'wow'),
                        'desc' => esc_html__('Input a block slug name', 'wow'),
                        'default' => 'You have Found the 404 page'
                    ),
                    array(
                        'id'=>'text-error',
                        'type' => 'text',
                        'title' => esc_html__('Content Page 404', 'wow'),
                        'desc' => esc_html__('Input a block slug name', 'wow'),
                        'default' => 'You may have mis-typed the URL. Or the page has been removed, had its name changed, or is temporarily unavailable.'
                    ),					
                    array(
                        'id'=>'btn-error',
                        'type' => 'text',
                        'title' => esc_html__('Button Page 404', 'wow'),
                        'desc' => esc_html__('Input a block slug name', 'wow'),
                        'default' => 'Back to Homepage'
                    ),
                    array(
                        'id'=>'img-404',
                        'type' => 'media',
                        'compiler'  => 'true',
                        'mode'      => false,
                        'title' => esc_html__('Images 404', 'wow'),
                        'desc'      => esc_html__('Upload images 404 default here.', 'wow'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/404.png'
                        )
                    )					
                )
            );
			
            if ( class_exists( 'WooCommerce' ) ) :

                // Woocommerce
                $this->sections[] = array(
                    'icon' => 'el-icon-shopping-cart',
                    'icon_class' => 'icon',
                    'title' => esc_html__('Woocommerce', 'wow'),
                    'fields' => array(
                    )
                );
				
                $this->sections[] = array(
                    'icon_class' => 'icon',
                    'subsection' => true,
                    'title' => esc_html__('Product Archives', 'wow'),
                    'fields' => array(
                        array(
                            'id'=>'sidebar_product',
                            'type' => 'image_select',
                            'title' => esc_html__('Page Layout', 'wow'),
                            'options' => $page_layouts,
                            'default' => 'left'
                        ),
                        array(
                            'id'=>'category-view-mode',
                            'type' => 'button_set',
                            'title' => esc_html__('View Mode', 'wow'),
                            'options' => wow_ct_category_view_mode(),
                            'default' => 'grid',
                        ),
						array(
							'id' => 'product_col_large',
							'type' => 'button_set',
							'title' => esc_html__('Product Listing column Desktop', 'wow'),
							'options' => array(
									'2' => '2',
									'3' => '3',
									'4' => '4',							
									'6' => '6'							
								),
							'default' => '3',
							'sub_desc' => esc_html__( 'Select number of column on Desktop Screen', 'wow' ),
						),
						array(
							'id' => 'product_col_medium',
							'type' => 'button_set',
							'title' => esc_html__('Product Listing column Medium Desktop', 'wow'),
							'options' => array(
									'2' => '2',
									'3' => '3',
									'4' => '4',							
									'6' => '6'							
								),
							'default' => '3',
							'sub_desc' => esc_html__( 'Select number of column on Medium Desktop Screen', 'wow' ),
						),
                        array(
                            'id'=>'category-image-hover',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Image Hover Effect', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'category-hover',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Hover Effect', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'woo-show-rating',
                            'type' => 'switch',
                            'title' => esc_html__('Show Rating in Woocommerce Products Widget', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),						
                        array(
                            'id'=>'product-wishlist',
                            'type' => 'switch',
                            'title' => esc_html__('Show Wishlist', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),						
                        array(
                            'id'=>'product_quickview',
                            'type' => 'switch',
                            'title' => esc_html__('Show Quick View', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow')
                        )
                    )
                );

                $this->sections[] = array(
                    'icon_class' => 'icon',
                    'subsection' => true,
                    'title' => esc_html__('Single Product', 'wow'),
                    'fields' => array(
                        array(
                            'id'=>'sidebar_detail_product',
                            'type' => 'image_select',
                            'title' => esc_html__('Page Layout', 'wow'),
                            'options' => $page_layouts,
                            'default' => 'left'
                        ),
                        array(
                            'id'=>'product-short-desc',
                            'type' => 'switch',
                            'title' => esc_html__('Show Short Description', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),					
                        array(
                            'id'=>'product-related',
                            'type' => 'switch',
                            'title' => esc_html__('Show Related Products', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'product-related-count',
                            'type' => 'text',
                            'required' => array('product-related','equals',true),
                            'title' => esc_html__('Related Products Count', 'wow'),
                            'default' => '10'
                        ),
                        array(
                            'id'=>'product-related-cols',
                            'type' => 'button_set',
                            'required' => array('product-related','equals',true),
                            'title' => esc_html__('Related Product Columns', 'wow'),
                            'options' => wow_ct_related_product_columns(),
                            'default' => '3',
                        ),
                        array(
                            'id'=>'product-upsell',
                            'type' => 'switch',
                            'title' => esc_html__('Show Upsell Products', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),						
                        array(
                            'id'=>'product-upsell-count',
                            'type' => 'text',
                            'required' => array('product-upsell','equals',true),
                            'title' => esc_html__('Upsell Products Count', 'wow'),
                            'default' => '10'
                        ),
                        array(
                            'id'=>'product-upsell-cols',
                            'type' => 'button_set',
                            'required' => array('product-upsell','equals',true),
                            'title' => esc_html__('Upsell Product Columns', 'wow'),
                            'options' => wow_ct_related_product_columns(),
                            'default' => '3',
                        ),
                        array(
                            'id'=>'product-hot',
                            'type' => 'switch',
                            'title' => esc_html__('Show "Hot" Label', 'wow'),
                            'desc' => esc_html__('Will be show in the featured product.', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'product-hot-label',
                            'type' => 'text',
                            'required' => array('product-hot','equals',true),
                            'title' => esc_html__('"Hot" Text', 'wow'),
                            'default' => ''
                        ),
                        array(
                            'id'=>'product-sale',
                            'type' => 'switch',
                            'title' => esc_html__('Show "Sale" Label', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'product-sale-label',
                            'type' => 'text',
                            'required' => array('product-sale','equals',true),
                            'title' => esc_html__('"Sale" Text', 'wow'),
                            'default' => ''
                        ),
                        array(
                            'id'=>'product-sale-percent',
                            'type' => 'switch',
                            'required' => array('product-sale','equals',true),
                            'title' => esc_html__('Show Saved Sale Price Percentage', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'product-share',
                            'type' => 'switch',
                            'title' => esc_html__('Show Social Share Links', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                    )
                );

                $this->sections[] = array(
                    'icon_class' => 'icon',
                    'subsection' => true,
                    'title' => esc_html__('Product Image & Zoom', 'wow'),
                    'fields' => array(
                        array(
                            'id'=>'product-thumbs',
                            'type' => 'switch',
                            'title' => esc_html__('Show Thumbnails', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'product-thumbs-count',
                            'type' => 'text',
                            'required' => array('product-thumbs','equals',true),
                            'title' => esc_html__('Thumbnails Count', 'wow'),
                            'default' => '4'
                        ),
                        array(
                            'id'=>'product-image-popup',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Image Popup', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'zoom-type',
                            'type' => 'button_set',
                            'title' => esc_html__('Zoom Type', 'wow'),
                            'options' => array('basic' => esc_html__('Basic', 'wow'),'inner' => esc_html__('Inner', 'wow'), 'lens' => esc_html__('Lens', 'wow')),
                            'default' => 'basic'
                        ),
                        array(
                            'id'=>'zoom-scroll',
                            'type' => 'switch',
                            'title' => esc_html__('Scroll Zoom', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'zoom-border',
                            'type' => 'text',
                            'title' => esc_html__('Border Size', 'wow'),
                            'default' => '2'
                        ),
                        array(
                            'id'=>'zoom-border-color',
                            'type' => 'color',
                            'title' => esc_html__('Border Color', 'wow'),
                            'default' => '#f9b61e'
                        ),						
                        array(
                            'id'=>'zoom-lens-size',
                            'type' => 'text',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('Lens Size', 'wow'),
                            'default' => '200'
                        ),
                        array(
                            'id'=>'zoom-lens-shape',
                            'type' => 'button_set',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('Lens Shape', 'wow'),
                            'options' => array('round' => esc_html__('Round', 'wow'), 'square' => esc_html__('Square', 'wow')),
                            'default' => 'square'
                        ),
                        array(
                            'id'=>'zoom-contain-lens',
                            'type' => 'switch',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('Contain Lens Zoom', 'wow'),
                            'default' => true,
                            'on' => esc_html__('Yes', 'wow'),
                            'off' => esc_html__('No', 'wow'),
                        ),
                        array(
                            'id'=>'zoom-lens-border',
                            'type' => 'text',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('Lens Border', 'wow'),
                            'default' => true
                        ),
                    )
                );

            endif;

            // Social Share
            $this->sections[] = array(
                'icon' => 'el-icon-share-alt',
                'icon_class' => 'icon',
                'title' => esc_html__('Social Share', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'social-share',
                        'type' => 'switch',
                        'title' => esc_html__('Show Social Links', 'wow'),
                        'desc' => esc_html__('Show social links in post and product, page, portfolio, etc.', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'share-fb',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Facebook Share', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'share-tw',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Twitter Share', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'share-linkedin',
                        'type' => 'switch',
                        'title' => esc_html__('Enable LinkedIn Share', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'share-googleplus',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Google + Share', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'share-pinterest',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Pinterest Share', 'wow'),
                        'default' => false,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                )
            );
			
            $this->sections[] = array(
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('Socials Link', 'wow'),
                'fields' => array(
                    array(
                        'id'=>'socials_link',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Socials link', 'wow'),
                        'default' => true,
                        'on' => esc_html__('Yes', 'wow'),
                        'off' => esc_html__('No', 'wow'),
                    ),
                    array(
                        'id'=>'link-fb',
                        'type' => 'text',
                        'title' => esc_html__('Enter Facebook link', 'wow'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-tw',
                        'type' => 'text',
                        'title' => esc_html__('Enter Twitter link', 'wow'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-linkedin',
                        'type' => 'text',
                        'title' => esc_html__('Enter LinkedIn link', 'wow'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-googleplus',
                        'type' => 'text',
                        'title' => esc_html__('Enter Google + link', 'wow'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-pinterest',
                        'type' => 'text',
                        'title' => esc_html__('Enter Pinterest link', 'wow'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-instagram',
                        'type' => 'text',
                        'title' => esc_html__('Enter Instagram link', 'wow'),
                    ),
                )
            );			
        }

        public function setHelpTabs() {

        }
		
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                'opt_name'          => 'wow_settings',
                'display_name'      => $theme->get('Name') . ' ' . esc_html__('Theme Options', 'wow') . '<a class="wow-theme-link" href="' . admin_url( 'admin.php?page=wow' ) . '">Welcome</a><a class="wow-theme-link" href="' . admin_url( 'admin.php?page=wow-system' ) . '">System Status</a><a class="wow-theme-link" href="' . admin_url( 'admin.php?page=wow-plugins' ) . '">Plugins</a><a class="wow-theme-link wow-theme-link-last" href="' . admin_url( 'admin.php?page=wow-demos' ) . '">Install Demos</a>',
                'display_version'   => esc_html__('Theme Version: ', 'wow') . wow_version,
                'menu_type'         => 'submenu',
                'allow_sub_menu'    => true,
                'menu_title'        => esc_html__('Theme Options', 'wow'),
                'page_title'        => esc_html__('Theme Options', 'wow'),
                'footer_credit'     => esc_html__('Theme Options', 'wow'),

                'google_api_key' => 'AIzaSyAX_2L_UzCDPEnAHTG7zhESRVpMPS4ssII',
                'disable_google_fonts_link' => true,

                'async_typography'  => false,
                'admin_bar'         => false,
                'admin_bar_icon'       => 'dashicons-admin-generic',
                'admin_bar_priority'   => 50,
                'global_variable'   => '',
                'dev_mode'          => false,
                'customizer'        => false,
                'compiler'          => false,

                'page_priority'     => null,
                'page_parent'       => 'themes.php',
                'page_permissions'  => 'manage_options',
                'menu_icon'         => '',
                'last_tab'          => '',
                'page_icon'         => 'icon-themes',
                'page_slug'         => 'wow_settings',
                'save_defaults'     => true,
                'default_show'      => false,
                'default_mark'      => '',
                'show_import_export' => true,
                'show_options_object' => false,

                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => false,
                'output_tag'        => false,

                'database'              => '',
                'system_info'           => false,

                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                ),
                'ajax_save'                 => false,
                'use_cdn'                   => true,
            );


            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
            }
            // $this->args['intro_text'] = sprintf('<p style="color: #0088cc">'.__('Please regenerate again default css files in <strong>Skin > Compile Default CSS</strong> after <strong>update theme</strong>.', 'wow').'</p>', $v);
        }			

    }

    global $reduxWowSettings;
    $reduxWowSettings = new Redux_Framework_wow_settings();
}