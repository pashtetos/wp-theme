jQuery(document).ready(function(){
	"use strict";
	
	if(jQuery('#last_tab').val() == ''){

		jQuery('.bingo-opts-group-tab:first').slideDown('fast');
		jQuery('#bingo-opts-group-menu li:first').addClass('active');
	
	}else{
		
		tabid = jQuery('#last_tab').val();
		jQuery('#'+tabid+'_section_group').slideDown('fast');
		jQuery('#'+tabid+'_section_group_li').addClass('active');
		
	}
	
	
	jQuery('input[name="'+bingo_opts.opt_name+'[defaults]"]').click(function(){
		if(!confirm(bingo_opts.reset_confirm)){
			return false;
		}
	});
	
	jQuery('.bingo-opts-group-tab-link-a').click(function(){
		relid = jQuery(this).attr('data-rel');
		
		jQuery('#last_tab').val(relid);
		
		jQuery('.bingo-opts-group-tab').each(function(){
			if(jQuery(this).attr('id') == relid+'_section_group'){
				jQuery(this).show();
			}else{
				jQuery(this).hide();
			}
			
		});
		
		jQuery('.bingo-opts-group-tab-link-li').each(function(){
				if(jQuery(this).attr('id') != relid+'_section_group_li' && jQuery(this).hasClass('active')){
					jQuery(this).removeClass('active');
				}
				if(jQuery(this).attr('id') == relid+'_section_group_li'){
					jQuery(this).addClass('active');
				}
		});
	});
	
	
	
	
	if(jQuery('#bingo-opts-save').is(':visible')){
		jQuery('#bingo-opts-save').delay(4000).slideUp('slow');
	}
	
	if(jQuery('#bingo-opts-imported').is(':visible')){
		jQuery('#bingo-opts-imported').delay(4000).slideUp('slow');
	}	
	
	jQuery('input, textarea, select').change(function(){
		jQuery('#bingo-opts-save-warn').slideDown('slow');
	});
	
	
	jQuery('#bingo-opts-import-code-button').click(function(){
		if(jQuery('#bingo-opts-import-link-wrapper').is(':visible')){
			jQuery('#bingo-opts-import-link-wrapper').fadeOut('fast');
			jQuery('#import-link-value').val('');
		}
		jQuery('#bingo-opts-import-code-wrapper').fadeIn('slow');
	});
	
	jQuery('#bingo-opts-import-link-button').click(function(){
		if(jQuery('#bingo-opts-import-code-wrapper').is(':visible')){
			jQuery('#bingo-opts-import-code-wrapper').fadeOut('fast');
			jQuery('#import-code-value').val('');
		}
		jQuery('#bingo-opts-import-link-wrapper').fadeIn('slow');
	});
	
	
	
	
	jQuery('#bingo-opts-export-code-copy').click(function(){
		if(jQuery('#bingo-opts-export-link-value').is(':visible')){jQuery('#bingo-opts-export-link-value').fadeOut('slow');}
		jQuery('#bingo-opts-export-code').toggle('fade');
	});
	
	jQuery('#bingo-opts-export-link').click(function(){
		if(jQuery('#bingo-opts-export-code').is(':visible')){jQuery('#bingo-opts-export-code').fadeOut('slow');}
		jQuery('#bingo-opts-export-link-value').toggle('fade');
	});
	
	

	
	
	
});