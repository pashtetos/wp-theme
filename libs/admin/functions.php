<?php

function wow_check_theme_options() {
    // check default options
    global $wow_settings;

    ob_start();
    $options = ob_get_clean();
    $wow_default_settings = json_decode($options, true);

    foreach ($wow_default_settings as $key => $value) {
        if (is_array($value)) {
            foreach ($value as $key1 => $value1) {
                if ($key1 != 'google' && (!isset($wow_settings[$key][$key1]) || !$wow_settings[$key][$key1])) {
                    $wow_settings[$key][$key1] = $wow_default_settings[$key][$key1];
                }
            }
        } else {
            if (!isset($wow_settings[$key])) {
                $wow_settings[$key] = $wow_default_settings[$key];
            }
        }
    }

    return $wow_settings;
}

function wow_options_sidebars() {
    return array(
        'wide-left-sidebar',
        'wide-right-sidebar',
        'left-sidebar',
        'right-sidebar'
    );
}

function wow_options_body_wrapper() {
    return array(
        'full' 		=> array('alt' => 'Full', 'img' => get_template_directory_uri().'/libs/admin/theme_options/layouts/body_full.jpg'),
        'boxed' 	=> array('alt' => 'Boxed', 'img' => get_template_directory_uri().'/libs/admin/theme_options/layouts/body_boxed.jpg'),
    );
}

function wow_options_layouts() {
    return array(
        "full" => array('alt' => 'Without Sidebar', 'img' => get_template_directory_uri().'/libs/admin/theme_options/layouts/page_full.jpg'),
        "left" => array('alt' => "Left Sidebar", 'img' => get_template_directory_uri().'/libs/admin/theme_options/layouts/page_full_left.jpg'),
        "right" => array('alt' => "Right Sidebar", 'img' => get_template_directory_uri().'/libs/admin/theme_options/layouts/page_full_right.jpg')
    );
}

if(!function_exists('wow_options_header_types')) :
	function wow_options_header_types() {
		$path = get_template_directory().'/headers/';
		$files = array_diff(scandir($path), array('..', '.'));
		if(count($files)>0){
			foreach ($files as  $file) {
				$name_file = str_replace( '.php', '', basename($file) );
				$value = str_replace( 'header-', '',$name_file);
				$name =  str_replace( '-', ' ', ucwords($name_file) );
				$header[$value] = array('title' => $name, 'img' => get_template_directory_uri().'/libs/admin/theme_options/headers/'.$name_file.'.jpg');
			}
		}	
		return $header;	
	}
endif;

function wow_options_banners_effect() {
	$banners_effects = array();
	for ($i = 1; $i <= 12; $i++) {
		$banners_effects['banners-effect-'.$i] =  array('alt' => 'Banner Effect', 'img' => get_template_directory_uri().'/libs/admin/theme_options/effects/banner-effect.png');
	}
    return $banners_effects;
}

if(!function_exists('wow_get_footers')) :
	function wow_get_footers() {
		$footer = array();
		$footers = get_posts( array('posts_per_page'=>-1,
							'post_type'=>'bin_footer',
							'orderby'          => 'name',
							'order'            => 'ASC'
					) );
		foreach ($footers as  $key=>$value) {
			$footer[$value->ID] = array('title' => $value->post_title, 'img' => get_template_directory_uri().'/libs/admin/theme_options/footers/'.$value->post_name.'.jpg');
		}
		return $footer;
	}
endif;

function wow_demo_filters() {
    return array(
        '*' => 'Show All',
        'demos' => 'Demos',
        'classic' => 'Classic',
        'corporate' => 'Corporate',
        'shop' => 'Shop',
        'dark' => 'Dark',
        'rtl' => 'RTL',
    );
}

function wow_demo_types() {
    return array(
        'classic-original' => array('alt' => 'Main Demo', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/classic_original.jpg', 'filter' => 'demos'),
        'construction' => array('alt' => 'Construction', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/demo_construction.jpg', 'filter' => 'demos'),
        'restaurant' => array('alt' => 'Restaurant', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/demo_restaurant.jpg', 'filter' => 'demos'),
        'digital-agency' => array('alt' => 'Digital Agency', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/demo_digital_agency.jpg', 'filter' => 'demos'),
        'law-firm' => array('alt' => 'Law Firm', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/demo_law_firm.jpg', 'filter' => 'demos'),
        'landing' => array('alt' => 'Landing', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/landing.jpg', 'filter' => 'classic'),
        'classic-color' => array('alt' => 'Classic Color', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/classic_color.jpg', 'filter' => 'classic'),
        'classic-light' => array('alt' => 'Classic Light', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/classic_light.jpg', 'filter' => 'classic'),
        'classic-video' => array('alt' => 'Classic Video', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/classic_video.jpg', 'filter' => 'classic'),
        'classic-video-light' => array('alt' => 'Classic Video Light', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/classic_video_light.jpg', 'filter' => 'classic'),
        'corporate1' => array('alt' => 'Corporate 1', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_1.jpg', 'filter' => 'corporate'),
        'corporate2' => array('alt' => 'Corporate 2', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_2.jpg', 'filter' => 'corporate'),
        'corporate3' => array('alt' => 'Corporate 3', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_3.jpg', 'filter' => 'corporate'),
        'corporate4' => array('alt' => 'Corporate 4', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_4.jpg', 'filter' => 'corporate'),
        'corporate5' => array('alt' => 'Corporate 5', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_5.jpg', 'filter' => 'corporate'),
        'corporate6' => array('alt' => 'Corporate 6', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_6.jpg', 'filter' => 'corporate'),
        'corporate7' => array('alt' => 'Corporate 7', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_7.jpg', 'filter' => 'corporate'),
        'corporate8' => array('alt' => 'Corporate 8', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_8.jpg', 'filter' => 'corporate'),
        'corporate-hosting' => array('alt' => 'Corporate Hosting', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/corporate_hosting.jpg', 'filter' => 'corporate'),
        'shop1' => array('alt' => 'Shop 1', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_1.jpg', 'filter' => 'shop'),
        'shop2' => array('alt' => 'Shop 2', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_2.jpg', 'filter' => 'shop'),
        'shop3' => array('alt' => 'Shop 3', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_3.jpg', 'filter' => 'shop'),
        'shop4' => array('alt' => 'Shop 4', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_4.jpg', 'filter' => 'shop'),
        'shop5' => array('alt' => 'Shop 5', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_5.jpg', 'filter' => 'shop'),
        'shop6' => array('alt' => 'Shop 6', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_6.jpg', 'filter' => 'shop'),
        'shop7' => array('alt' => 'Shop 7', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_7.jpg', 'filter' => 'shop'),
        'shop8' => array('alt' => 'Shop 8', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_8.jpg', 'filter' => 'shop'),
        'shop9' => array('alt' => 'Shop 9', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_9.jpg', 'filter' => 'shop'),
        'shop10' => array('alt' => 'Shop 10', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/shop_10.jpg', 'filter' => 'shop'),
        'dark' => array('alt' => 'Dark Original', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/dark_original.jpg', 'filter' => 'dark'),
        'rtl' => array('alt' => 'RTL Original', 'img' => get_template_directory_uri().'/libs/admin/theme_options/demos/rtl_original.jpg', 'filter' => 'rtl'),
    );
}
// Function for Content Type, ReducxFramework
function wow_ct_layouts() {
    return array(
        "widewidth" => esc_html__("Wide Width", 'wow'),
        "wide-left-sidebar" => esc_html__("Wide Left Sidebar", 'wow'),
        "wide-right-sidebar" => esc_html__("Wide Right Sidebar", 'wow'),
        "fullwidth" => esc_html__("Without Sidebar", 'wow'),
        "left-sidebar" => esc_html__("Left Sidebar", 'wow'),
        "right-sidebar" => esc_html__("Right Sidebar", 'wow')
    );
}

function wow_ct_post_archive_layouts() {
    return array(
        'full' => esc_html__('Full', 'wow'),
        'large' => esc_html__('Large', 'wow'),
        'large-alt' => esc_html__('Large Alt', 'wow'),
        'medium' => esc_html__('Medium', 'wow'),
        'grid' => esc_html__('Grid', 'wow'),
        'timeline' => esc_html__('Timeline', 'wow')
    );
}

function wow_ct_post_single_layouts() {
    return array(
        'full' => esc_html__('Full', 'wow'),
        'large' => esc_html__('Large', 'wow'),
        'large-alt' => esc_html__('Large Alt', 'wow'),
        'medium' => esc_html__('Medium', 'wow')
    );
}

function wow_ct_portfolio_archive_layouts() {
    return array(
        'grid' => esc_html__('Grid', 'wow'),
        'masonry' => esc_html__('Masonry', 'wow'),
        'timeline' => esc_html__('Timeline', 'wow'),
        'full' => esc_html__('Full', 'wow'),
        'large' => esc_html__('Large', 'wow'),
        'medium' => esc_html__('Medium', 'wow')
    );
}

function wow_ct_portfolio_single_layouts() {
    return array(
        'medium' => esc_html__('Medium Slider', 'wow'),
        'large' => esc_html__('Large Slider', 'wow'),
        'full' => esc_html__('Full Slider', 'wow'),
        'gallery' => esc_html__('Gallery', 'wow'),
        'gallery' => esc_html__('Gallery', 'wow'),
        'carousel' => esc_html__('Carousel', 'wow'),
        'medias' => esc_html__('Medias', 'wow'),
        'full-video' => esc_html__('Full Width Video', 'wow'),
        'masonry' => esc_html__('Masonry Images', 'wow'),
        'full-images' => esc_html__('Full Images', 'wow'),
        'extended' => esc_html__('Extended', 'wow')
    );
}

function wow_ct_sidebars() {
    global $wp_registered_sidebars;

    $sidebar_options = array();
    if (!empty($wp_registered_sidebars)) {
        foreach ($wp_registered_sidebars as $sidebar) {
            if (!in_array($sidebar['id'], array('content-bottom-1', 'content-bottom-2', 'content-bottom-3', 'content-bottom-4', 'footer-top', 'footer-column-1', 'footer-column-2', 'footer-column-3', 'footer-column-4', 'footer-bottom')))
            $sidebar_options[$sidebar['id']] = $sidebar['name'];
        }
    };

    return $sidebar_options;
}


function wow_ct_header_view() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "fixed" => esc_html__("Fixed", 'wow')
    );
}

function wow_ct_related_product_columns() {
    return array(
        "2" => "2",
        "3" => "3",
        "4" => "4",
        "5" => "5",
        "6" => "6"
    );
}

function wow_ct_product_columns() {
    return array(
        "2" => "2",
        "3" => "3",
        "4" => "4",
        "5" => "5",
        "6" => "6",
        "7" => esc_html__("7 (widthout sidebar)", 'wow'),
        "8" => esc_html__("8 (widthout sidebar)", 'wow')
    );
}

function wow_ct_category_addlinks_pos() {
    return array(
        "outimage" => esc_html__("Out of Image", 'wow'),
        "onimage" => esc_html__("On Image", 'wow'),
        "wq_onimage" => esc_html__("Wishlist, Quick View On Image", 'wow')
    );
}

function wow_ct_bg_repeat() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "no-repeat" => esc_html__("No Repeat", 'wow'),
        "repeat" => esc_html__("Repeat All", 'wow'),
        "repeat-x" => esc_html__("Repeat Horizontally", 'wow'),
        "repeat-y" => esc_html__("Repeat Vertically", 'wow'),
        "inherit" => esc_html__("Inherit", 'wow'),
    );
}

function wow_ct_bg_size() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "inherit" => esc_html__("Inherit", 'wow'),
        "cover" => esc_html__("Cover", 'wow'),
        "contain" => esc_html__("Contain", 'wow'),
    );
}

function wow_ct_bg_attachment() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "fixed" => esc_html__("Fixed", 'wow'),
        "scroll" => esc_html__("Scroll", 'wow'),
        "inherit" => esc_html__("Inherit", 'wow'),
    );
}

function wow_ct_bg_position() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "left top" => esc_html__("Left Top", 'wow'),
        "left center" => esc_html__("Left Center", 'wow'),
        "left bottom" => esc_html__("Left Bottom", 'wow'),
        "center top" => esc_html__("Center Top", 'wow'),
        "center center" => esc_html__("Center Center", 'wow'),
        "center bottom" => esc_html__("Center Bottom", 'wow'),
        "right top" => esc_html__("Right Top", 'wow'),
        "right center" => esc_html__("Right Center", 'wow'),
        "right bottom" => esc_html__("Right Bottom", 'wow'),
    );
}

function wow_ct_category_view_mode() {
    return array(
        "grid" => esc_html__("Grid", 'wow'),
        "list" => esc_html__("List", 'wow')
    );
}

function wow_ct_share_options() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "yes" => esc_html__("Yes", 'wow'),
        "no" => esc_html__("No", 'wow')
    );
}

function wow_ct_show_options() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "yes" => esc_html__("Show", 'wow'),
        "no" => esc_html__("Hide", 'wow')
    );
}

function wow_ct_enable_options() {
    return array(
        "" => esc_html__("Default", 'wow'),
        "yes" => esc_html__("Enable", 'wow'),
        "no" => esc_html__("Disable", 'wow')
    );
}

function wow_ct_slideshow_types() {
    return array(
        'images' => esc_html__('Featured Images', 'wow'),
        'video' => esc_html__('Video & Audio or Content', 'wow'),
        'none' => esc_html__('None', 'wow'),
    );
}


