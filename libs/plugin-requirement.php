<?php
/***** Active Plugin ********/

add_action( 'tgmpa_register', 'wow_register_required_plugins' );
function wow_register_required_plugins() {
    $plugins = array(
		array(
            'name'               => esc_html__('Woocommerce', 'wow'), 
            'slug'               => 'woocommerce', 
            'required'           => false
        ),		
		array(
            'name'               => esc_html__('Visual Composer', 'wow'), 
            'slug'               => 'js_composer', 
            'source'             => esc_url('http://wordpress.bingotheme.com/wow/plugins/js_composer.zip'), 
            'required'           => true, 
        ),
		array(
            'name'               => esc_html__('Revolution Slider', 'wow'), 
            'slug'               => 'revslider', 
            'source'             => esc_url('http://wordpress.bingotheme.com/wow/plugins/revslider.zip'), 
            'required'           => true, 
        ),
		array(
            'name'               => esc_html__('Bingo Content Types', 'wow'), 
            'slug'               => 'bin-content-types', 
            'source'             => esc_url('http://wordpress.bingotheme.com/wow/plugins/bin-content-types.zip'), 
            'required'           => true, 
        ),	
		array(
            'name'               => esc_html__('Bingo Widgets', 'wow'), 
            'slug'               => 'bin-widgets', 
            'source'             => esc_url('http://wordpress.bingotheme.com/wow/plugins/bin-widgets.zip'), 
            'required'           => true, 
        ),	
		array(
            'name'               => esc_html__('Bingo Importer', 'wow'), 
            'slug'               => 'bin-importer', 
            'source'             => esc_url('http://wordpress.bingotheme.com/wow/plugins/bin-importer.zip'), 
            'required'           => true, 
        ),	
		array(
            'name'               => esc_html__('Bingo Woocommerce', 'wow'), 
            'slug'               => 'bin-woocommerce', 
            'source'             => esc_url('http://wordpress.bingotheme.com/wow/plugins/bin-woocommerce.zip'), 
            'required'           => true, 
        ),			
		array(
            'name'               => esc_html__('Redux Framework', 'wow'), 
            'slug'               => 'redux-framework', 
            'required'           => false
        ),			
		array(
            'name'      		 => esc_html__('Contact Form 7', 'wow'),
            'slug'     			 => 'contact-form-7',
            'required' 			 => false
        ), 
		 array(
            'name'     			 => esc_html__('YITH Woocommerce Wishlist', 'wow'),
            'slug'      		 => 'yith-woocommerce-wishlist',
            'required' 			 => false
        ),
		array(
            'name'     			 => esc_html__('WooCommerce Currency Switcher', 'wow'),
            'slug'      		 => 'woocommerce-currency-switcher',
            'required' 			 => false
        ), 		
    );
    $config = array();

    tgmpa( $plugins, $config );

}	