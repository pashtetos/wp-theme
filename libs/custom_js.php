<?php 
	global $wow_settings;
	$zoom_scroll = (isset($wow_settings['zoom-scroll']) && $wow_settings['zoom-scroll']) ? 'true' : 'false';
	$zoom_contain_lens = (isset($wow_settings['zoom-contain-lens']) && $wow_settings['zoom-contain-lens']) ? 'true' : 'false';	
?>

(function ($, window, document) { 
	if (($('body').width()) >= 992){
		$("#image").elevateZoom({
				<?php if( isset($wow_settings['zoom-type']) && $wow_settings['zoom-type'] != 'basic' ) { ?>
					zoomType : "<?php echo isset($wow_settings['zoom-type']) ? esc_js($wow_settings['zoom-type']) : 'zoom-type';?>",
				<?php } ?>
				scrollZoom  : <?php echo esc_js($zoom_scroll); ?>,
				lensSize    : <?php echo isset($wow_settings['zoom-lens-size']) ? esc_js($wow_settings['zoom-lens-size']) : '200';?>,
				lensShape    : "<?php echo isset($wow_settings['zoom-lens-shape']) ? esc_js($wow_settings['zoom-lens-shape']) : 'zoom-lens-shape';?>",
				containLensZoom  : <?php  echo esc_js($zoom_contain_lens); ?>,
				gallery:'image-additional',
				cursor: 'pointer',
				galleryActiveClass: "active",
				lensBorder:  <?php echo isset($wow_settings['zoom-lens-border']) ? esc_js($wow_settings['zoom-lens-border']) : '10';?>,
				borderSize : "<?php echo isset($wow_settings['zoom-border']) ? esc_js($wow_settings['zoom-border']) : '2';?>",				
				borderColour : "<?php echo isset($wow_settings['zoom-border-color']) ? esc_js($wow_settings['zoom-border-color']) : '#252525';?>"
		});
		<?php if(isset($wow_settings['product-image-popup']) && $wow_settings['product-image-popup']) {?>
		$("#image").bind("click", function(e) {  
			var ez =   $('#image').data('elevateZoom');	
				$.fancybox(ez.getGalleryList());
			return false;
		});	
		<?php }else{ ?>	
			$("#image").bind("click", function(e) {  
				return false;
			});				
		<?php } ?>
	}
	else if (($(window).width()) <= 991){
		$("#image").elevateZoom({
				zoomEnabled: false,
				scrollZoom: false,
				gallery:'image-additional',
				cursor: 'pointer',
				galleryActiveClass: "active"
		});
		<?php if(isset($wow_settings['product-image-popup']) && $wow_settings['product-image-popup']) {?>
		$("#image").bind("click", function(e) {  
		  var ez =   $('#image').data('elevateZoom');	
			$.fancybox(ez.getGalleryList());
		  return false;
		});		
		<?php }else{ ?>	
			$("#image").bind("click", function(e) {  
				return false;
			});				
		<?php } ?>
	}
})(jQuery);