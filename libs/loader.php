<?php
if ( ! function_exists( 'wow_setup' ) ) :

		function wow_setup() {

			load_theme_textdomain( 'wow', get_template_directory() . '/languages' );
			// Add RSS feed links to <head> for posts and comments.
			add_theme_support( 'automatic-feed-links' );

			// Enable support for Post Thumbnails, and declare two sizes.
			add_theme_support( 'post-thumbnails' );
			set_post_thumbnail_size( 584, 710, true );
			add_image_size( 'wow-full-width', 1038, 576, true );

			// This theme uses wp_nav_menu() in two locations.
			register_nav_menus( array(
				'primary'   => esc_html__( 'Top primary menu', 'wow' ),
				'secondary' => esc_html__( 'Secondary menu in left sidebar', 'wow' ),
			) );
			
			add_theme_support( "title-tag" );
			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
			) );

			/*
			 * Enable support for Post Formats.
			 * See http://codex.wordpress.org/Post_Formats
			 */
			add_theme_support( 'post-formats', array(
				'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
			) );

			// This theme allows users to set a custom background.
			add_theme_support( 'custom-background', apply_filters( 'wow_custom_background_args', array(
				'default-color' => 'f5f5f5',
			) ) );

			// Custom image header
			$wow_image_headers = array(
				'default-image' => get_template_directory_uri().'/images/logo/logo-default.png',
				'uploads'       => true
			);
			add_theme_support( 'custom-header', $wow_image_headers );

			// Tell the TinyMCE editor to use a custom stylesheet
			add_editor_style( 'css/editor-style.css' );
			
			// This theme uses its own gallery styles.
			add_filter( 'use_default_gallery_style', '__return_false' );
			
			add_theme_support( 'woocommerce' );
		}
		endif; // wow_setup
		add_action( 'after_setup_theme', 'wow_setup' );


		function wow_widgets_init() {
			register_sidebar( array(
				'name'          => esc_html__( 'Sidebar Blog', 'wow' ),
				'id'            => 'sidebar-blog',
				'description'   => esc_html__( 'Additional sidebar that appears on the right.', 'wow' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => esc_html__( 'Header Top Link ', 'wow' ),
				'id'            => 'top-link',
				'description'   => esc_html__( 'Main sidebar that appears on the left.', 'wow' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => esc_html__( 'Sidebar Shop', 'wow' ),
				'id'            => 'sidebar-product',
				'description'   => esc_html__( 'Main sidebar that appears on the left.', 'wow' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => esc_html__( 'Sidebar Left Detail Product', 'wow' ),
				'id'            => 'sidebar-detail-product-left',
				'description'   => esc_html__( 'Main sidebar product that appears on the left.', 'wow' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );			
			register_sidebar( array(
				'name'          => esc_html__( 'Content Header 1', 'wow' ),
				'id'            => 'content-header-1',
				'description'   => esc_html__( 'Appears in the content top section of the site.', 'wow' ),
				'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => esc_html__( 'Content Header 2', 'wow' ),
				'id'            => 'content-header-2',
				'description'   => esc_html__( 'Appears in the content top section of the site.', 'wow' ),
				'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
			register_sidebar( array(
				'name'          => esc_html__( 'Newletter Popup', 'wow' ),
				'id'            => 'newletter-popup-form',
				'description'   => esc_html__( 'Appears in the content top section of the site.', 'wow' ),
				'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );			
		}
		add_action( 'widgets_init', 'wow_widgets_init' );

		function wow_scripts() {
			//Font Default 
			$font_default  = "https://fonts.googleapis.com/css?family=Montserrat:400,600,700";
			$font_default2 = "https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i";
			wp_enqueue_style( "wow-fonts-default",$font_default, array(), null );
			wp_enqueue_style( "wow-fonts-default-2",$font_default2, array(), null );
			// Add font, used in the main stylesheet.
			$config_fonts = wow_config_font();
			foreach($config_fonts as $key => $selector_fonts){
				if(strpos($key,'class_') === false && isset($selector_fonts['font-family']) && $selector_fonts['font-family']){
					$font = str_replace(" ","+",$selector_fonts['font-family']);
					if( !preg_match('/'.esc_attr($font).'/', $font_default,$matches) ){ 
						if($selector_fonts['font-weight'])
							$font = $font.":".$selector_fonts['font-weight'];
						$fonts_url = add_query_arg( array(
							'family' => $font,
						), 'https://fonts.googleapis.com/css' );
						wp_enqueue_style( 'wow-fonts-'.$key,$fonts_url, array(), null );	
					}
					if( !preg_match('/'.esc_attr($font).'/', $font_default2,$matches) ){ 
						if($selector_fonts['font-weight'])
							$font = $font.":".$selector_fonts['font-weight'];
						$fonts_url = add_query_arg( array(
							'family' => $font,
						), 'https://fonts.googleapis.com/css' );
						wp_enqueue_style( 'wow-fonts-'.$key,$fonts_url, array(), null );	
					}					
				}
			}
			
			// Load our main stylesheet.
			wp_enqueue_style( 'wow-style', get_stylesheet_uri() );

			// Load the Internet Explorer specific stylesheet.
			wp_enqueue_style( 'wow-ie', get_template_directory_uri() . '/css/ie.css', array( 'wow-style' ), '20131205' );
			wp_style_add_data( 'wow-ie', 'conditional', 'lt IE 9' );

			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}

			if ( is_singular() && wp_attachment_is_image() ) {
				wp_enqueue_script( 'wow-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
			}

			if ( is_active_sidebar( 'sidebar-3' ) ) {
				wp_enqueue_script( 'wow-jquery-masonry' );
			}

			wp_enqueue_script( 'wow-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery','jquery-cookie'), '20140616', true );
		}
		add_action( 'wp_enqueue_scripts', 'wow_scripts' );

		function wow_add_scripts() {
			wp_enqueue_script('bootstrap-js',get_template_directory_uri().'/js/bootstrap.min.js');
			wp_enqueue_script('owl.carousel',get_template_directory_uri().'/js/owl.carousel.min.js', array('wpb_composer_front_js'));
			wp_enqueue_script('instafeed-js',get_template_directory_uri().'/js/instafeed.min.js');
			wp_enqueue_script('jquery-countdown',get_template_directory_uri().'/js/jquery.countdown.min.js');
			wp_enqueue_script('jquery-fancybox', get_template_directory_uri().'/js/jquery.fancybox.pack.js');
			wp_enqueue_script('wc-quantity-increment-js', get_template_directory_uri().'/js/wc-quantity-increment.min.js');
			wp_register_script('isotope-js', get_template_directory_uri() . '/js/isotope.js' , array(), null, true );
			wp_enqueue_script('isotope-js' );
			wp_register_script( 'wow-portfolio-js', get_template_directory_uri() . '/js/portfolio.js', array(), null, true );
			wp_enqueue_script( 'wow-portfolio-js' );
			wp_register_script( 'elevatezoom-js', get_template_directory_uri() . '/js/elevatezoom-min.js' , array(), null, false );
			wp_enqueue_script( 'elevatezoom-js' );
	
			$direction = wow_get_direction(); 
			if( is_rtl() || $direction == "rtl"){
				wp_enqueue_style( 'bootstrap-rtl-css',get_template_directory_uri().'/css/bootstrap-rtl.css' );
			}else{
				wp_enqueue_style( 'bootstrap-css', get_template_directory_uri().'/css/bootstrap.css' );
			}
			wp_enqueue_style('jquery-fancybox-css', get_template_directory_uri().'/css/jquery.fancybox.css', array(), null);
			wp_enqueue_style( 'wow-style-template', get_template_directory_uri().'/css/template.css');
			wp_enqueue_style( 'fontawesome-css',get_template_directory_uri().'/css/font-awesome.css' );
			wp_enqueue_style( 'icofont-css',get_template_directory_uri().'/css/icofont.css' );
			wp_enqueue_style( 'animate-css',get_template_directory_uri().'/css/animate.css' );
			wp_enqueue_style( 'simple-line-icons-css', get_template_directory_uri().'/css/simple-line-icons.css' );

			wp_register_style( 'wow-woocommerce', get_template_directory_uri() . '/css/woocommerce.css' );
			if ( class_exists( 'Woocommerce' ) ) {
				wp_enqueue_style( 'wow-woocommerce' );
				add_filter( 'woocommerce_enqueue_styles', '__return_false' );
			}		
		}
		add_action( 'wp_enqueue_scripts', 'wow_add_scripts' );
		
		function wow_admin_style() {
			wp_enqueue_style('wow-admin-styles', get_template_directory_uri().'/libs/admin/css/options.css');
		}
		add_action('admin_enqueue_scripts', 'wow_admin_style');	
?>