<?php
	add_action( 'init', 'wow_button_product' );
	function wow_button_product(){
		global $wow_settings;	
		//Button List Product
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
		add_action('woocommerce_after_shop_loop_item', 'wow_create_button_cart', 10 );	
		if(isset($wow_settings['product-wishlist']) && $wow_settings['product-wishlist'] && class_exists( 'YITH_WCWL' ) )
			add_action('woocommerce_after_shop_loop_item', 'wow_add_loop_wishlist_link', 15 );		
		add_action( 'woocommerce_after_shop_loop_item', 'wow_kill_after_shop_loop_item', 0 ); 	
		//Button Detail Product
		// add_action( 'woocommerce_single_product_summary', 'wow_add_loop_wishlist_link',36 );	
	}
	/*add second thumbnail loop product*/
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'wow_woocommerce_template_loop_product_thumbnail', 10 );
	function wow_product_thumbnail( $size = 'full', $placeholder_width = 0, $placeholder_height = 0  ) {
		global $wow_settings;
		$quickview = wow_get_config('product_quickview'); 
		global $product;
		$html = '';
		$id = get_the_ID();
		$gallery = get_post_meta($id, '_product_image_gallery', true);
		$attachment_image = '';
		if(!empty($gallery)) {
			$gallery = explode(',', $gallery);
			$first_image_id = $gallery[0];
			$attachment_image = wp_get_attachment_image($first_image_id , $size, false, array('class' => 'hover-image back'));
		}
		
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), '' );
			if ( has_post_thumbnail() ){
				if( $attachment_image ){
					$html .= '<div class="product-thumb-hover">';
					$html .= '<a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';
					$html .= (get_the_post_thumbnail( $product->get_id(), $size )) ? get_the_post_thumbnail( $product->get_id(), $size ): '<img src="'.get_template_directory_uri().'/assets/img/placeholder/'.esc_url($size).'.png" alt="No thumb">';
					if($wow_settings['category-image-hover']){
						$html .= $attachment_image;
					}
					$html .= '</a>';				
					$html .= '</div>';	
				}else{	
					$html .= '<a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';				
					$html .= (get_the_post_thumbnail( $product->get_id(), $size )) ? get_the_post_thumbnail( $product->get_id(), $size ): '<img src="'.get_template_directory_uri().'/assets/img/placeholder/'.esc_url($size).'.png" alt="No thumb">';
					$html .= '</a>';
				}			
			}else{
				$html .= '<a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';				
				$html .= '<img src="'.get_template_directory_uri().'/assets/img/placeholder/'.esc_url($size).'.png" alt="No thumb">';		
				$html .= '</a>';
			}
	

		/* quickview */
		if( $quickview ) :
			$nonce = wp_create_nonce("wow_quickviewproduct_nonce");
			$link = admin_url('admin-ajax.php?ajax=true&amp;action=wow_quickviewproduct&amp;post_id='.$product->get_id().'&amp;nonce='.$nonce);
			$html .= '<div class="bin-quickview-btn"><div>';
			$html .= '<a href="'. esc_url($link) .'" data-fancybox-type="ajax" class="quickview group fancybox fancybox.ajax" >'.apply_filters( 'out_of_stock_add_to_cart_text', esc_html__( 'Quick View ', 'wow' ) ).'</a>';	
			$html .= '</div></div>';
		endif;
		return $html;
	}
	
	function wow_woocommerce_template_loop_product_thumbnail(){
		echo wow_product_thumbnail();
	}
	
	function wow_kill_after_shop_loop_item() {
		wow_remove_anonymous_object_filter(
			'woocommerce_after_shop_loop_item',
			'YITH_Woocompare_Frontend',
			'add_compare_link'
		);
	}
	
	function wow_remove_anonymous_object_filter($tag, $class, $method) {
		$filters = $GLOBALS['wp_filter'][$tag];

		if (empty($filters)){
			return;
		}

		if(version_compare(get_bloginfo('version'), '4.7', '>=')) {
			$callbacks = $filters->callbacks;
		} else {
			$callbacks = $filters;
		}

		foreach ($callbacks as $priority => $filter) {
			foreach ($filter as $identifier => $function) {
				if (is_array( $function) && is_a( $function['function'][0], $class ) && $method === $function['function'][1]) {
				  remove_filter($tag, array ( $function['function'][0], $method ), $priority);
				}
			}
		}
	}	

	function wow_add_excerpt_in_product_archives() {
		global $post;
		if ( ! $post->post_excerpt ) return;		
		echo '<div class="item-description item-description2">'.wp_trim_words( $post->post_excerpt, 45 ).'</div>';
	}	

	add_action( 'woocommerce_after_shop_loop_item_title', 'wow_add_excerpt_in_product_archives', 40 );
		
	function wow_create_button_cart() {
			global $product;
			$bt_cart = apply_filters( 'woocommerce_loop_add_to_cart_link',
				sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="1" class="button ajax_add_to_cart %s product_type_%s">%s</a>',
					esc_url( $product->add_to_cart_url() ),
					esc_attr( $product->get_id() ),
					esc_attr( $product->get_sku() ),
					$product->is_purchasable() && $product->is_in_stock() ? ' add_to_cart_button' : '',
					// esc_attr( $product->product_type ),
					esc_attr( $product->get_type() ),
					esc_html( $product->add_to_cart_text() )
				),$product );
			echo $bt_cart;
	}	
	/*********QUICK VIEW PRODUCT**********/
	function wow_quickview(){
		global $product;
		$html = '';	
		$quickview = wow_get_config('product_quickview'); 
		/* quickview */
		if( $quickview ) : 
			$nonce = wp_create_nonce("wow_quickviewproduct_nonce");
			$link = admin_url('admin-ajax.php?ajax=true&amp;action=wow_quickviewproduct&amp;post_id='.$product->get_id().'&amp;nonce='.$nonce);
			$html .= '<a href="'. esc_url($link ) .'" data-fancybox-type="ajax" class="quickview group fancybox fancybox.ajax" >'.apply_filters( 'out_of_stock_add_to_cart_text', '<i class="fa fa-search-plus"></i>' ).'</a>';	
		endif;
		echo $html;
	}

	add_action("wp_ajax_wow_quickviewproduct", "wow_quickviewproduct");
	add_action("wp_ajax_nopriv_wow_quickviewproduct", "wow_quickviewproduct");
	
	function wow_quickviewproduct(){
		
		$productid = (isset($_REQUEST["post_id"]) && $_REQUEST["post_id"]>0) ? $_REQUEST["post_id"] : 0;
		
		$query_args = array(
			'post_type'	=> 'product',
			'p'			=> $productid
		);
		$outputraw = $output = '';
		$r = new WP_Query($query_args);
		if($r->have_posts()){ 

			while ($r->have_posts()){ $r->the_post(); setup_postdata($r->post);
				global $product;
				ob_start();
				woocommerce_get_template_part( 'content', 'quickview-product' );
				$outputraw = ob_get_contents();
				ob_end_clean();
			}
		}
		$output = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $outputraw);
		echo $output;exit();
	}
	
	function wow_add_loop_wishlist_link(){	
		if ( class_exists( 'YITH_WCWL' ) ){
			echo do_shortcode( "[yith_wcwl_add_to_wishlist]" );
		}
	}
/**
 * Remove Woocommerce Breadcrumb
 *
 */
remove_action('woocommerce_before_main_content', 'wow_woocommerce_breadcrumb', 20);	

function wow_get_class_item_product(){
	global $wow_settings;
	$product_col_large = 12 /((isset($wow_settings['product_col_large']) && $wow_settings['product_col_large']) ? $wow_settings['product_col_large'] : 4);	
	$product_col_medium = 12/((isset($wow_settings['product_col_medium']) && $wow_settings['product_col_medium']) ? $wow_settings['product_col_medium'] : 3);
	$product_col_sm 	= 12/((isset($wow_settings['product_col_sm']) && $wow_settings['product_col_sm']) ? $wow_settings['product_col_sm'] : 1);
	$class_item_product = 'col-lg-'.$product_col_large.' col-md-'.$product_col_medium.' col-sm-'.$product_col_sm;
	
	return $class_item_product;
}

function wow_login_top(){
	if(class_exists( 'WooCommerce' )) : ?>
		<div class="login-top">
			<!-- Trigger the modal with a button -->
			<button type="button" class="btn bin-btn" data-toggle="modal" data-target="#myModal"><?php echo esc_html__('Login','wow'); ?></button>
			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content bin-modal">
				  <div class="modal-header bin-modal-head">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><?php echo esc_html__('Welcome Back!','wow'); ?></h4>
				  </div>
				  <div class="modal-body bin-modal-body">
					<p><?php echo esc_html__('Login your account','wow'); ?></p>
					<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
						<form action="<?php // echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>" method="post" id="loginform-top" name="loginform-top">
							<?php do_action( 'woocommerce_login_form_start' ); ?>
							<p class="login-username">
								<input type="text" class="input input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" placeholder="Username" />
							</p>
							<p class="login-password">
								<input class="input input-text" type="password" name="password" id="password" placeholder="Password" />
							</p>														
							<?php do_action( 'woocommerce_login_form' ); ?>
							<div class="links-more lost_password">
								<p class="form-row">
									<label for="rememberme" class="inline">
										<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e( 'Remember me', 'wow' ); ?>
									</label>
								</p>																												
								<p><a href="<?php echo esc_url(get_permalink( get_option('woocommerce_myaccount_page_id')) ); ?>" title="<?php esc_attr__('login or register', 'wow'); ?>"><?php esc_html_e(' Login or register ', 'wow'); ?></a></p>
							</div>
							<p class="login-submit">
								<?php wp_nonce_field( 'woocommerce-login' ); ?>
								<input type="submit" class="button" name="login" value="<?php esc_attr_e( 'Login', 'wow' ); ?>" />
							</p>																			
							<p><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'wow' ); ?></a></p>					
							<?php do_action( 'woocommerce_login_form_end' ); ?>
						</form>
					<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
				  </div>
				</div>

			  </div>
			</div>									
		</div>
	<?php endif;
}

/**
 * Remove Woocommerce
 *
 */
add_action( 'woocommerce_single_product_summary', 'wow_add_social', 40 );
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );
add_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 5 );
 
?>