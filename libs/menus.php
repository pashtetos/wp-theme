<?php

    /*
    *
    *	Bingo Framework Menu Functions
    *	------------------------------------------------
    *	Bingo Framework v3.0
    * 	Copyright Bingo Ideas 2016 - http://bingotheme.com/
    *
    *	wow_setup_menus()
    *
    */


    /* CUSTOM MENU SETUP
    ================================================== */
    register_nav_menus( array(
        'main_navigation' => esc_html__( 'Main Menu', 'wow' ),
        'main_navigation2' => esc_html__( 'Menu Categories', 'wow' ),
        'overlay_menu'    => esc_html__( 'Overlay Menu', 'wow' ),
        'mobile_menu'     => esc_html__( 'Mobile Menu', 'wow' ),
        'mobile_menu2'     => esc_html__( 'Mobile Menu Categories', 'wow' ),
        'top_bar_menu'    => esc_html__( 'Top Bar Menu', 'wow' ),
        'footer_menu'     => esc_html__( 'Footer Menu', 'wow' )
    ) );

?>
