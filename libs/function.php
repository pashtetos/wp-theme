<?php	
	function wow_get_config($option,$default='1'){
		global $wow_settings;
		if(isset($_GET[$option]) && $_GET[$option]){
			return $_GET[$option];
		}
		else{
			$value = isset($wow_settings[$option]) ? $wow_settings[$option] : $default;
			return $value;
		}
	}
	
	if ( ! function_exists( 'wow_popup_newsletter' ) ) {
		function wow_popup_newsletter() {
			global $wow_settings; 
			$style = 'style="display:none"';
			if(isset($wow_settings['background_newletter_img']['url']) && !empty($wow_settings['background_newletter_img']['url']))
				$style = 'style="display:none;background-image: url('. esc_url($wow_settings['background_newletter_img']['url']).')"';
			echo '<div class="popupshadow" style="display:none"></div>';
			echo '<div id="newsletterpopup" class="bingo-modal newsletterpopup"';
				if(isset($wow_settings['background_newletter_img']['url']) && !empty($wow_settings['background_newletter_img']['url'])){
					echo $style;
				}
			echo ' >';
				
			echo '<span class="close-popup"></span>
				<div class="wp-newletter">
					<div class="nl-bg">';
						dynamic_sidebar('newletter-popup-form');
					echo '</div>
				</div>';
			echo '</div>';
		}
	}	
		
	
	function wow_config_font(){
		$config_fonts = array();
		$text_fonts = array(
			'family_font_body',
			'family_font_custom',
			'h1-font',
			'h2-font',
			'h3-font',
			'h4-font',
			'h5-font',
			'h6-font',
			'class_font_custom'
		);
		foreach ($text_fonts as $text) {
			if(wow_get_config($text))
				$config_fonts[$text] = wow_get_config($text);
		}
		return $config_fonts;
	}
	
	function wow_get_class(){
		$class = new \stdClass;
		$sidebar_left_expand 		= wow_get_config('sidebar_left_expand',3);
		$sidebar_left_expand_md 	= wow_get_config('sidebar_left_expand_md',3);
		$sidebar_left_expand_sm 	= wow_get_config('sidebar_left_expand_sm',3);
		$class->class_sidebar_left  = 'col-lg-'.$sidebar_left_expand.' col-md-'.$sidebar_left_expand_md.' col-sm-'.$sidebar_left_expand_sm.' col-xs-12';
		
		$sidebar_right_expand 		= wow_get_config('sidebar_right_expand',3);
		$sidebar_right_expand_md 	= wow_get_config('sidebar_right_expand_md',3);
		$sidebar_right_expand_sm 	= wow_get_config('sidebar_right_expand_sm',3);
		$class->class_sidebar_right  = 'col-lg-'.$sidebar_right_expand.' col-md-'.$sidebar_right_expand_md.' col-sm-'.$sidebar_right_expand_sm.' col-xs-12';
		
		$sidebar_blog = wow_get_config('sidebar_blog','left');
		if($sidebar_blog == 'left_right'){
			$blog_content_expand = 12-($sidebar_right_expand + $sidebar_left_expand);
			$blog_content_expand_md = 12-($sidebar_left_expand_md + $sidebar_right_expand_md);
			$blog_content_expand_sm = 12-($sidebar_left_expand_sm + $sidebar_right_expand_sm);		
		}elseif($sidebar_blog == 'left'){
			$blog_content_expand = 12- $sidebar_left_expand;
			$blog_content_expand_md = 12- $sidebar_left_expand_md;
			$blog_content_expand_sm = 12- $sidebar_left_expand_sm;
		}elseif($sidebar_blog == 'right'){
			$blog_content_expand = 12- $sidebar_right_expand;
			$blog_content_expand_md = 12- $sidebar_right_expand_md;
			$blog_content_expand_sm = 12- $sidebar_right_expand_sm;
		}else{
			$blog_content_expand = 12;
			$blog_content_expand_md = 12;
			$blog_content_expand_sm = 12;
		}
		
		$class->class_blog_content  = 'col-lg-'.$blog_content_expand.' col-md-'.$blog_content_expand_md.' col-sm-'.$blog_content_expand_sm.' col-xs-12';		

		
		$post_single_layout = wow_get_config('post-single-layout');
		if($post_single_layout == 'left_right'){
			$blog_single_expand = 12-($sidebar_right_expand + $sidebar_left_expand);
			$blog_single_expand_md = 12-($sidebar_left_expand_md + $sidebar_right_expand_md);
			$blog_single_expand_sm = 12-($sidebar_left_expand_sm + $sidebar_right_expand_sm);		
		}elseif($post_single_layout == 'left'){
			$blog_single_expand = 12- $sidebar_left_expand;
			$blog_single_expand_md = 12- $sidebar_left_expand_md;
			$blog_single_expand_sm = 12- $sidebar_left_expand_sm;
		}elseif($post_single_layout == 'right'){
			$blog_single_expand = 12- $sidebar_right_expand;
			$blog_single_expand_md = 12- $sidebar_right_expand_md;
			$blog_single_expand_sm = 12- $sidebar_right_expand_sm;
		}else{
			$blog_single_expand = 12;
			$blog_single_expand_md = 12;
			$blog_single_expand_sm = 12;
		}
		
		$class->class_single_content  = 'col-lg-'.$blog_single_expand.' col-md-'.$blog_single_expand_md.' col-sm-'.$blog_single_expand_sm.' col-xs-12';		
		
		
		$sidebar_product = wow_get_config('sidebar_product');
		if($sidebar_product == 'left'){
			$product_content_expand = 12- $sidebar_left_expand;
			$product_content_expand_md = 12- $sidebar_left_expand_md;
			$product_content_expand_sm = 12- $sidebar_left_expand_sm;
		}elseif($sidebar_product == 'right'){
			$product_content_expand = 12- $sidebar_right_expand;
			$product_content_expand_md = 12- $sidebar_right_expand_md;
			$product_content_expand_sm = 12- $sidebar_right_expand_sm;
		}else{
			$product_content_expand = 12;
			$product_content_expand_md = 12;
			$product_content_expand_sm = 12;
		}
		$class->class_product_content  = 'col-lg-'.$product_content_expand.' col-md-'.$product_content_expand_md.' col-sm-'.$product_content_expand_sm.' col-xs-12';		

		$sidebar_detail_product = wow_get_config('sidebar_detail_product');
		if($sidebar_detail_product == 'left'){
			$product_content_expand = 12- $sidebar_left_expand;
			$product_content_expand_md = 12- $sidebar_left_expand_md;
			$product_content_expand_sm = 12- $sidebar_left_expand_sm;
		}elseif($sidebar_detail_product == 'right'){
			$product_content_expand = 12- $sidebar_right_expand;
			$product_content_expand_md = 12- $sidebar_right_expand_md;
			$product_content_expand_sm = 12- $sidebar_right_expand_sm;
		}else{
			$product_content_expand = 12;
			$product_content_expand_md = 12;
			$product_content_expand_sm = 12;
		}
		$class->class_detail_product_content  = 'col-lg-'.$product_content_expand.' col-md-'.$product_content_expand_md.' col-sm-'.$product_content_expand_sm.' col-xs-12';	
		
		$blog_col_large 	= 12/(wow_get_config('blog_col_large',3));
		$blog_col_medium = 12/(wow_get_config('blog_col_medium',3));
		$blog_col_sm 	= 12/(wow_get_config('blog_col_sm',3));
		
		$class->class_item_blog = 'col-lg-'.$blog_col_large.' col-md-'.$blog_col_medium.' col-sm-'.$blog_col_sm;
		
		return $class;
	}
	
	function wow_is_customize(){
		return isset($_POST['customized']) && ( isset($_POST['customize_messenger_chanel']) || isset($_POST['wp_customize']) );
	}	
	
	function wow_search_form( $form ) {
		$form = '<form role="search" method="get" id="searchform" class="search-from" action="' . esc_attr(home_url( '/' )) . '" >
					<div class="container">
						<div class="form-content">
							<input type="text" value="' . esc_attr(get_search_query()) . '" name="s" id="s" class="s" placeholder="' . esc_attr__( 'Enter Your Search', 'wow' ) . '" />
							<button id="searchsubmit" class="btn" type="submit">
								<i class="fa fa-search"></i>
								<span>' . esc_html__( 'Search', 'wow' ) . '</span>
							</button>
						</div>
					</div>
				  </form>';
		return $form;
	}
	add_filter( 'get_search_form', 'wow_search_form' );

	function wow_custom_login_form( $links ) {
		$links = '<div class="links-more">';
		if ( get_option( 'users_can_register' ) ) {
			$links .= '<p><a href="' . esc_url( wp_registration_url() ) . '">' . esc_html__( 'New customer?', 'wow' ) . '</a></p>';
		}
		$links .= '<p><a href="'. esc_url( wp_lostpassword_url() ) . '">' . esc_html__( 'Forget password?', 'wow' ) . '</a></p>';
		$links .= '</div>';
		return $links;
	}
	add_filter( 'login_form_middle', 'wow_custom_login_form' );

	add_action( 'login_enqueue_scripts', 'wow_sourcexpress_login_enqueue_scripts', 10 );
	
	function wow_sourcexpress_login_enqueue_scripts() {
		$script = "<script type='text/javascript'>jQuery(document).ready(function(){
			'use strict';
		    jQuery('#user_login').attr('placeholder', '". esc_html__( 'Username', 'wow' )."');
		    jQuery('#user_pass').attr('placeholder', '". esc_html__( 'Password', 'wow' )."');
		});</script>";
		echo $script;
	}

	// Remove each style one by one
	add_filter( 'woocommerce_enqueue_styles', 'wow_jk_dequeue_styles' );
	function wow_jk_dequeue_styles( $enqueue_styles ) {
		unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
		unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
		unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
		return $enqueue_styles;
	}

	// Or just remove them all in one line
	add_filter( 'woocommerce_enqueue_styles', '__return_false' );
					
	function wow_woocommerce_breadcrumb( $args = array() ) {
		$args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
			'delimiter'   => '<span class="delimiter"></span>',
			'wrap_before' => '<div class="bin-breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '><div class="container">',
			'wrap_after'  => '</div></div>',
			'before'      => '',
			'after'       => '',
			'home'        => _x( 'Home', 'breadcrumb', 'wow' )
		) ) );

		$breadcrumbs = new WC_Breadcrumb();

		if ( $args['home'] ) {
			$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
		}

		$args['breadcrumb'] = $breadcrumbs->generate();

		wc_get_template( 'global/breadcrumb.php', $args );
	}
	
	add_filter('woocommerce_add_to_cart_fragments', 'wow_woocommerce_header_add_to_cart_fragment');
	function wow_woocommerce_header_add_to_cart_fragment( $fragments )
	{
	    global $woocommerce;
	    ob_start(); 
	    get_template_part( 'woocommerce/minicart-ajax' );
	    $fragments['.mini-cart'] = ob_get_clean();
	    return $fragments;
	}

	function wow_display_view(){
		global $wow_settings;
		$category_view_mode 		= wow_get_config('category-view-mode','grid');
		$active_list = ($category_view_mode == 'list') ? 'active' : '';
		$active_grid = ($category_view_mode == 'grid') ? 'active' : '';
		$html = '<ul class="display pull-left">
                <li>
                	<a class="view-grid '.$active_grid.'" href="#"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                </li>
                <li>
                	<a class="view-list '.$active_list.'" href="#"><i class="fa fa-align-justify" aria-hidden="true"></i></a>
            	</li>
            </ul>';
		echo $html;	
    }
	
	
	function wow_main_menu($id, $layout = "") {

		global $wow_settings, $post;

		$show_cart = $show_wishlist = false;
		if ( isset($wow_settings['show_cart']) ) {
		$show_cart            = $wow_settings['show_cart'];
		}
		if ( isset($wow_settings['show_wishlist']) ) {
		$show_wishlist            = $wow_settings['show_wishlist'];
		}
		$vertical_header_text = (isset($wow_settings['vertical_header_text']) && $wow_settings['vertical_header_text']) ? $wow_settings['vertical_header_text'] : '';
		$page_menu = $menu_output = $menu_full_output = $menu_with_search_output = $menu_float_output = $menu_vert_output = "";

		$main_menu_args = array(
			'echo'            => false,
			'theme_location' => 'main_navigation',
			'walker' => new wow_mega_menu_walker,
		);
		
		$menu_output .= '<nav id="'.$id.'" class="std-menu clearfix">'. "\n";

		if(function_exists('wp_nav_menu')) {
			if (has_nav_menu('main_navigation')) {
				$menu_output .= wp_nav_menu( $main_menu_args );
			}
			else {
				$menu_output .= '<div class="no-menu">'. esc_html__("Please assign a menu to the Main Menu in Appearance > Menus", 'wow').'</div>';
			}
		}
		$menu_output .= '</nav>'. "\n";
		switch ($layout) {
			case 'full':
					$menu_full_output .= '<div class="container">'. "\n";
					$menu_full_output .= '<div class="row">'. "\n";
					$menu_full_output .= '<div class="menu-left">'. "\n";
					$menu_full_output .= $menu_output . "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_full_output .= '<div class="menu-right">'. "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_output = $menu_full_output;
				break;
			case 'float':
					$menu_float_output .= '<div class="float-menu">'. "\n";
					$menu_float_output .= $menu_output . "\n";
					$menu_float_output .= '</div>'. "\n";
					$menu_output = $menu_float_output;
				break;
			case 'float-2':
					$menu_float_output .= '<div class="float-menu container">'. "\n";
					$menu_float_output .= $menu_output . "\n";
					$menu_float_output .= '</div>'. "\n";
					$menu_output = $menu_float_output;
				break;				
			case 'vertical':
				$menu_vertical_output .= $menu_output . "\n";
				$menu_vertical_output .= '<div class="vertical-menu-bottom">'. "\n";
				if($vertical_header_text)
				$menu_vertical_output .= '<div class="copyright">'.do_shortcode(stripslashes($vertical_header_text)).'</div>'. "\n";
				$menu_vertical_output .= '</div>'. "\n";

				$menu_output = $menu_vertical_output;
				break;
		}	
		return $menu_output;
	}				
	
	function wow_main_menu2($id, $layout = "") {

		global $wow_settings, $post;

		$show_cart = $show_wishlist = false;
		if ( isset($wow_settings['show_cart']) ) {
		$show_cart            = $wow_settings['show_cart'];
		}
		if ( isset($wow_settings['show_wishlist']) ) {
		$show_wishlist            = $wow_settings['show_wishlist'];
		}
		$vertical_header_text = (isset($wow_settings['vertical_header_text']) && $wow_settings['vertical_header_text']) ? $wow_settings['vertical_header_text'] : '';
		$page_menu = $menu_output = $menu_full_output = $menu_with_search_output = $menu_float_output = $menu_vert_output = "";

		$main_menu_args = array(
			'echo'            => false,
			'theme_location' => 'main_navigation2',
			'walker' => new wow_mega_menu_walker,
		);
		
		$menu_output .= '<nav id="'.$id.'" class="std-menu clearfix">'. "\n";

		if(function_exists('wp_nav_menu')) {
			if (has_nav_menu('main_navigation2')) {
				$menu_output .= wp_nav_menu( $main_menu_args );
			}
			else {
				$menu_output .= '<div class="no-menu">'. esc_html__("Please assign a menu to the Categories Menu in Appearance > Menus", 'wow').'</div>';
			}
		}
		$menu_output .= '</nav>'. "\n";
		switch ($layout) {
			case 'full':
					$menu_full_output .= '<div class="container">'. "\n";
					$menu_full_output .= '<div class="row">'. "\n";
					$menu_full_output .= '<div class="menu-left">'. "\n";
					$menu_full_output .= $menu_output . "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_full_output .= '<div class="menu-right">'. "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_full_output .= '</div>'. "\n";
					$menu_output = $menu_full_output;
				break;
			case 'float':
					$menu_float_output .= '<div class="float-menu">'. "\n";
					$menu_float_output .= $menu_output . "\n";
					$menu_float_output .= '</div>'. "\n";
					$menu_output = $menu_float_output;
				break;
			case 'float-2':
					$menu_float_output .= '<div class="float-menu container">'. "\n";
					$menu_float_output .= $menu_output . "\n";
					$menu_float_output .= '</div>'. "\n";
					$menu_output = $menu_float_output;
				break;				
			case 'vertical':
				$menu_vertical_output .= $menu_output . "\n";
				$menu_vertical_output .= '<div class="vertical-menu-bottom">'. "\n";
				if($vertical_header_text)
				$menu_vertical_output .= '<div class="copyright">'.do_shortcode(stripslashes($vertical_header_text)).'</div>'. "\n";
				$menu_vertical_output .= '</div>'. "\n";

				$menu_output = $menu_vertical_output;
				break;
		}	
		return $menu_output;
	}
	
	add_action('admin_enqueue_scripts','wow_upload_scripts');	

	function wow_upload_scripts()
    {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
		wp_enqueue_script('wow-admin-upload', get_template_directory_uri().'/js/upload.js');
    }		
	
	function wow_body_classes( $classes ) {
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}

		if ( get_header_image() ) {
			$classes[] = 'header-image';
		} elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
			$classes[] = 'masthead-fixed';
		}

		if ( is_archive() || is_search() || is_home() ) {
			$classes[] = 'list-view';
		}

		if ( is_singular() && ! is_front_page() ) {
			$classes[] = 'singular';
		}

		if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
			$classes[] = 'slider';
		} elseif ( is_front_page() ) {
			$classes[] = 'grid';
		}
		
		$type_banner = wow_get_config('banners_effect');
		//$type_banner = wow_get_config('type_banner');
		$classes[] = $type_banner;		
		
		$direction = wow_get_direction(); 
		if($direction && $direction == 'rtl'){
			$classes[] = 'rtl';
		}
		
		$box_layout 	= wow_get_config('layout');
		if( $box_layout == 'boxed' ){
			$classes[] = 'box-layout';
		}else{
			$classes[] = 'full-layout';
		}
		
		return $classes;
	}
	add_filter( 'body_class', 'wow_body_classes' );

	function wow_post_classes( $classes ) {
		if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
			$classes[] = 'has-post-thumbnail';
		}

		return $classes;
	}
	add_filter( 'post_class', 'wow_post_classes' );


	function wow_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $title;
		}

		// Add the site name.
		$title .= get_bloginfo( 'name', 'display' );

		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title = "$title $sep $site_description";
		}

		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title = "$title $sep " . sprintf( __( 'Page %s', 'wow' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'wow_wp_title', 10, 2 );
	
	/* Favicon */
	if ( ! function_exists( 'wp_site_icon' ) ) {
	function wow_wp_site_icon_add_favicon(){
		global $wow_settings;
		if ( $wow_settings['favicon'] ){
			echo '<link rel="shortcut icon" href="' . esc_url( $wow_settings['favicon'] ). '" />';
		}
	}
	add_action('wp_head', 'wow_wp_site_icon_add_favicon');
	}
	
	function wow_get_excerpt($limit = 45, $more_link = true, $more_style_block = false) {
		global $wow_settings;

		if (!$limit) {
			$limit = 45;
		}

		if (has_excerpt()) {
			$content = get_the_excerpt();
		} else {
			$content = get_the_content();
		}

		$content = wow_strip_tags( apply_filters( 'the_content', $content ) );
		$content = explode(' ', $content, $limit);

		if (count($content) >= $limit) {
			array_pop($content);
			if ($more_link)
				$content = implode(" ",$content).'... ';
			else
				$content = implode(" ",$content).' [...]';
		} else {
			$content = implode(" ",$content);
		}
		
		$content = '<p class="post-excerpt">'.$content.'</p>';
		
		return $content;
	}	
	
	function wow_strip_tags( $content ) {
		$content = str_replace( ']]>', ']]&gt;', $content );
		$content = preg_replace("/<script.*?\/script>/s", "", $content);
		$content = preg_replace("/<style.*?\/style>/s", "", $content);
		$content = strip_tags( $content );
		return $content;
	}	
	
	function wow_fnc_disable_srcset( $sources ) {
		return false;
	}
	add_filter( 'wp_calculate_image_srcset', 'wow_fnc_disable_srcset' );
	
	if( !function_exists( 'wow_socials_link' ) ) :
	function wow_socials_link() {
		global $wow_settings;
		if(isset($wow_settings['socials_link']) && $wow_settings['socials_link']){
			$html = '<ul class="list-socials-link">';
			
				if( isset($wow_settings['link-fb']) && $wow_settings['link-fb'] ) :
					$html .= '<li class="facebook"><a href="' . esc_url($wow_settings['link-fb']) . '"><i class="fa fa-facebook"></i></a></li>';
				endif;

				if( isset($wow_settings['link-tw']) && $wow_settings['link-tw'] ) :
					$html .= '<li class="twitter"><a href="' . esc_url($wow_settings['link-tw']) . '"><i class="fa fa-twitter"></i></a></li>';
				endif;

				if( isset($wow_settings['link-linkedin']) && $wow_settings['link-linkedin'] ) :
					$html .= '<li class="linkedin"><a href="' . esc_url($wow_settings['link-linkedin']) . '"><i class="fa fa-linkedin"></i></a></li>';
				endif;

				if( isset($wow_settings['link-googleplus']) && $wow_settings['link-googleplus'] ) :
					$html .= '<li class="googleplus"><a href="' . esc_url($wow_settings['link-googleplus']) . '"><i class="fa fa-google-plus"></i></a></li>';
				endif;

				if( isset($wow_settings['link-pinterest']) && $wow_settings['link-pinterest'] ) :
					$html .= '<li class="pinterest"><a href="' . esc_url($wow_settings['link-pinterest']) . '"><i class="fa fa-pinterest"></i></a></li>';
				endif;

				if( isset($wow_settings['link-instagram']) && $wow_settings['link-instagram'] ) :
					$html .= '<li class="instagram"><a href="' . esc_url($wow_settings['link-instagram']) . '"><i class="fa fa-instagram"></i></a></li>';
				endif;
			$html .= '</ul>';
		
		return $html;
		}
	}
	endif;	
	
	if( !function_exists( 'wow_get_direction' ) ) :
	function wow_get_direction(){
		$direction = wow_get_config('direction','ltr');		
		if (isset($_COOKIE['wow_direction_cookie']))
			$direction = $_COOKIE['wow_direction_cookie'];
		if(isset($_GET['direction']) && $_GET['direction'])
			$direction = $_GET['direction'];
		return 	$direction;
	}
	endif;	
	
	function wow_parseShortcodesCustomCss($post_content){
	  $output = '';
	  if(class_exists("Vc_Manager")){
	   $shortcodes_custom_css = visual_composer()->parseShortcodesCustomCss( $post_content );
	   if ( ! empty( $shortcodes_custom_css ) ) {
		$shortcodes_custom_css = strip_tags( $shortcodes_custom_css );
		$output .= '<style type="text/css" data-type="vc_shortcodes-custom-css">';
		$output .= $shortcodes_custom_css;
		$output .= '</style>';
	   }
	  }
	  echo $output;  
	}	

?>