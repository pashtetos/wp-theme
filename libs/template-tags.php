<?php

if ( ! function_exists( 'wow_paging_nav' ) ) :
	function wow_paging_nav() {
		global $wp_query, $wp_rewrite;

		// Don't print empty markup if there's only one page.
		if ( $wp_query->max_num_pages < 2 ) {
			return;
		}

		$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
		$pagenum_link = html_entity_decode( get_pagenum_link() );
		$query_args   = array();
		$url_parts    = explode( '?', $pagenum_link );

		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}

		$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
		$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

		$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

		// Set up paginated links.
		$links = paginate_links( array(
			'base'     => $pagenum_link,
			'format'   => $format,
			'total'    => $wp_query->max_num_pages,
			'current'  => $paged,
			'mid_size' => 1,
			'add_args' => array_map( 'urlencode', $query_args ),
			'prev_text' => esc_html__( 'Previous', 'wow' ),
			'next_text' => esc_html__( 'Next', 'wow' ),
		) );

		if ( $links ) :

		?>
		<nav class="navigation paging-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php esc_html_e( 'Posts navigation', 'wow' ); ?></h1>
			<div class="pagination loop-pagination">
				<?php echo $links; ?>
			</div><!-- .pagination -->
		</nav><!-- .navigation -->
		<?php
		endif;
	}

endif;

if ( ! function_exists( 'wow_post_nav' ) ) :

function wow_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}

	?>
	<nav class="navigation post-navigation" role="navigation">
		<div class="nav-links">
			<?php
			if ( is_attachment() ) :
				previous_post_link( '%link', __( '<span class="meta-nav">Published In</span>', 'wow' ) );
			else :
				previous_post_link( '%link', __( '<span class="meta-nav left"><i class="fa fa-long-arrow-left"></i> Previous Post</span>', 'wow' ) );
				next_post_link( '%link', __( '<span class="meta-nav right">Next Post <i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>', 'wow' ) );
			endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'wow_posted_on' ) ) :

function wow_posted_on() {
	if ( is_sticky() && is_home() && ! is_paged() ) {
		echo '<span class="featured-post">' . esc_html__( 'Sticky', 'wow' ) . '</span>';
	}

	// Set up and print post meta information.
	printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a></span> <span class="byline"><span class="author vcard"><a class="url fn n" href="%4$s" rel="author">%5$s</a></span></span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		get_the_author()
	);
}
endif;

function wow_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'wow_category_count' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'wow_category_count', $all_the_cool_cats );
	}

	if ( 1 !== (int) $all_the_cool_cats ) {
		return true;
	} else {
		return false;
	}
}


function wow_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'wow_category_count' );
}
add_action( 'edit_category', 'wow_category_transient_flusher' );
add_action( 'save_post',     'wow_category_transient_flusher' );

if ( ! function_exists( 'wow_post_thumbnail' ) ) :

function wow_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
	<?php
		if ( ( ! is_active_sidebar( 'sidebar-2' ) || is_page_template( 'page-templates/full-width.php' ) ) ) {
			the_post_thumbnail( 'bingo-full-width' );
		} else {
			the_post_thumbnail();
		}
	?>
	</div>

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
	<?php
		if ( ( ! is_active_sidebar( 'sidebar-2' ) || is_page_template( 'page-templates/full-width.php' ) ) ) {
			the_post_thumbnail( 'bingo-full-width' );
		} else {
			the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
		}
	?>
	</a>

	<?php endif;
}
endif;


if ( ! function_exists( 'wow_posted_on' ) ) :

function wow_posted_on() {
	global $wow_settings;
	if ( is_sticky() && is_home() && ! is_paged() ) {
		echo '<span class="featured-post">' . esc_html__( 'Sticky', 'wow' ) . '</span>';
	}

	// Set up and print post meta information.
	printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);
	
	if(isset($wow_settings['archives-author']) && $wow_settings['archives-author'])
	{
		// Set up and print post meta information.
		printf( '<span class="byline"><span class="author vcard"><a class="url fn n" href="%1$s" rel="author">%2$s</a></span></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			get_the_author()
		);			
	}
}
endif;

if ( ! function_exists( 'wow_posted_on2' ) ) :

function wow_posted_on2() {
	global $wow_settings;
	
	printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);	
	
	if(isset($wow_settings['archives-author']) && $wow_settings['archives-author'])
	{
		// Set up and print post meta information.
		printf( '<span class="byline"><span class="author vcard"><a class="url fn n" href="%1$s" rel="author">%2$s</a></span></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			get_the_author()
		);			
	}
}
endif;

if ( ! function_exists( 'wow_posted_on3' ) ) :
function wow_posted_on3() {
	global $wow_settings;

	printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);
}
endif;

if ( ! function_exists( 'wow_excerpt_more' ) && ! is_admin() ) :

function wow_excerpt_more( $more ) {
	$link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
			sprintf( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'wow' ), '<span class="screen-reader-text">' . get_the_title( get_the_ID() ) . '</span>' )
		);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'wow_excerpt_more' );
endif;

function wow_page_title() {
	global $post, $wow_settings,$wp_query;

	
	$bg = isset($wow_settings['page_title_bg']['url']) ? $wow_settings['page_title_bg']['url'] : "";
	if (function_exists('is_product_category') && is_product_category()) {
		$cat_obj = $wp_query->get_queried_object();
		$thumbnail_bg = absint( get_woocommerce_term_meta( $cat_obj->term_id, 'thumbnail_bg', true ) );
		$background_dark = absint( get_woocommerce_term_meta( $cat_obj->term_id, 'background_dark', true ) );	
		if ( $thumbnail_bg ){
			$image_attributes = wp_get_attachment_url( $thumbnail_bg );
			if ( $image_attributes ){
				$bg = $image_attributes;
			}else{
				$bg = wc_placeholder_img_src();
			}
		}
	}

	?>
	<div class="page-title bin-title <?php if(isset($background_dark) && $background_dark == 1){echo "background-dark";}?>" <?php echo $bg ? ' style="background-image:url(' . esc_url( $bg ) . ');"' : ''; ?>>
		<div class="container">
			<?php if( isset( $wow_settings['page_title']) &&  $wow_settings['page_title'] ) :  ?>
				<h1>
					<?php
						if( is_category() ) :
							single_cat_title();

						elseif ( is_tax() ) :
							single_tag_title();	

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							esc_html_e( 'Galleries', 'wow' );

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							esc_html_e( 'Images', 'wow' );

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							esc_html_e( 'Videos', 'wow' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							esc_html_e( 'Quotes', 'wow' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							esc_html_e( 'Audios', 'wow' );

						elseif ( is_archive() ) :
							
							printf( esc_html__( 'Category Page left sidebar', 'wow' ), get_search_query() );

						elseif ( is_search() ) :
							printf( esc_html__( 'Search for: %s', 'wow' ), get_search_query() );

						elseif ( is_404() ) :
							esc_html_e( '404 Error', 'wow' );

						elseif ( is_singular( 'knowledge' ) ) :
							esc_html_e( 'Knowledge Base', 'wow' );

						elseif ( is_home() ) :
							esc_html_e( 'Blog', 'wow' );

						else :
							printf( esc_html__( '%s', 'wow' ), get_the_title() );
						endif;
					?>
				</h1>
			<?php endif; ?>

			<?php if(isset($wow_settings['breadcrumb']) && $wow_settings['breadcrumb']) : ?>
				<?php
					if(function_exists('is_woocommerce') && is_woocommerce())
						wow_woocommerce_breadcrumb(); 
					else
						get_template_part( 'breadcrumb'); 
				?>						
			<?php endif; ?>
			
		</div><!-- .container -->
	</div><!-- .page-title -->
	<?php
}

	function wow_add_social() {
		echo '<div class="social-icon">
		<div class="social-icon-button"></div>';
		 echo do_action( 'woocommerce_share' );
		 echo wow_get_social();
		echo '</div>';
	}	

	function wow_get_social() {
		global $post, $wow_settings;
	
		if(isset($wow_settings['product-share']) && $wow_settings['product-share']){
			$social['social-share'] = wow_get_config('social-share');
			$social['share-fb'] = wow_get_config('share-fb');
			$social['share-tw'] = wow_get_config('share-tw');
			$social['share-linkedin'] = wow_get_config('share-linkedin');
			$social['share-googleplus'] = wow_get_config('share-googleplus');
			$social['share-pinterest'] = wow_get_config('share-pinterest');
			
			if (!$social['social-share']) return false;
			
			$permalinked = urlencode(get_permalink($post->ID));
			$permalink = get_permalink($post->ID);
			$title = urlencode($post->post_title);
			$stitle = $post->post_title;
			$image = esc_url(wp_get_attachment_url( get_post_thumbnail_id() ));
			
			$data = '<div class="label-share">'. esc_html__('Share: ','wow') .'</div><div class="social-share">';
			
			if ($social['share-fb']) {
				$data .='<a href="http://www.facebook.com/sharer.php?m2w&amp;s=100&amp;p&#091;url&#093;='.esc_url($permalink).'&amp;p&#091;images&#093;&#091;0&#093;='.esc_url($image).'&amp;p&#091;title&#093;='.esc_url($stitle).'"  title="'. esc_attr__('Facebook', 'wow').'" class="share-facebook">'. esc_html__('', 'wow').'<i class="fa fa-facebook"></i></a>';
			}			
			if ($social['share-tw']) {
				$data .='<a href="https://twitter.com/intent/tweet?text='.esc_url($stitle).'&amp;url='.esc_url($permalink).'"  title="'. esc_attr__('Twitter', 'wow').'" class="share-twitter">'. esc_html__('', 'wow').'<i class="fa fa-twitter"></i></a>';
			}
			if ($social['share-linkedin']) {
				$data .='<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url='.esc_url($permalink).'&amp;title='.esc_url($stitle).'"  title="'. esc_attr__('LinkedIn', 'wow').'" class="share-linkedin">'. esc_html__('', 'wow').'<i class="fa fa-linkedin"></i></a>';
			}
			if ($social['share-googleplus']) {
				$data .= '<a href="https://plus.google.com/share?url='.esc_url($permalink).'"  title="'. esc_attr__('Google +', 'wow').'" class="share-googleplus">'. esc_html__('', 'wow').'<i class="fa fa-google-plus"></i></a>';
			}
			if ($social['share-pinterest']) {
				$data .= '<a href="https://pinterest.com/pin/create/button/?url='.esc_url($permalink).'&amp;media='.esc_url($image).'"  title="'. esc_attr__('Pinterest', 'wow').'" class="share-pinterest">'. esc_html__('', 'wow').'<i class="fa fa-pinterest"></i></a>';
			}
			$data .= '</div>';
			echo $data;
		}
	}