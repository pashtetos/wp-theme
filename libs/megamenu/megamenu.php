<?php

    /*
    *
    *	Bingo MEGA MENU FRAMEWORK
    *	------------------------------------------------
    *	Bingo Framework
    * 	Copyright Bingo Ideas 2016 - http://bingotheme.com/
    *
    */

    class wow_mega_menu {

        /*--------------------------------------------*
         * Constructor
         *--------------------------------------------*/

        /**
         * Initializes the plugin by setting localization, filters, and administration functions.
         */
        function __construct() {
			
			// add new fields via hook
			add_action( 'wp_nav_menu_item_custom_fields', array( $this, 'wow_mega_menu_add_custom_fields' ), 10, 5 );
			
            // add custom menu fields to menu
            add_filter( 'wp_setup_nav_menu_item', array( $this, 'wow_mega_menu_add_custom_nav_fields' ) );

            // save menu custom fields
            add_action( 'wp_update_nav_menu_item', array( $this, 'wow_mega_menu_update_custom_nav_fields' ), 10, 3 );

            // edit menu walker
            add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wow_mega_menu_edit_walker' ), 10, 2 );

        } // end constructor
        
        /**
         * Add custom fields to edit menu page
         *
         * @access      public
         * @since       1.0
         * @return      void
         */
        function wow_mega_menu_add_custom_fields( $item_id, $item, $depth, $args ) {
			?>
        	<div class="menu-options">

                <?php if ( $depth == 0 ) { ?>
					
                    <h4><?php esc_html_e( 'Mega Menu Options', 'wow' ); ?></h4>
                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-megamenu-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Enable Mega Menu', 'wow' ); ?>
                            <input type="checkbox" id="edit-menu-megamenu-<?php echo esc_attr($item_id); ?>"
                                   class="edit-menu-item-custom" id="menu-megamenu[<?php echo esc_attr($item_id); ?>]"
                                   name="menu-megamenu[<?php echo esc_attr($item_id); ?>]"
                                   value="1" <?php echo checked( ! empty( $item->megamenu ), 1, false ); ?> />
                        </label>
                    </p>
                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-menucol-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Mega Menu Columns', 'wow' ); ?></label>
                        <select class="fat" id="edit-menu-menucol-<?php echo esc_attr($item_id); ?>"
                                name="menu-menucol[<?php echo esc_attr($item_id); ?>]">
                            <?php for ( $i = 1; $i <= 6; $i ++ ) {
                                if ( $i == $item->menucol ) {
                                    echo '<option selected="selected">' . esc_html($i) . '</option>';
                                } else {
                                    echo '<option>' . esc_html($i) . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </p>
                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-is-naturalwidth-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Natural Width Mega Menu', 'wow' ); ?>
                            <input type="checkbox" id="edit-menu-is-naturalwidth-<?php echo esc_attr($item_id); ?>"
                                   class="edit-menu-item-custom" id="menu-is-naturalwidth[<?php echo esc_attr($item_id); ?>]"
                                   name="menu-is-naturalwidth[<?php echo esc_attr($item_id); ?>]"
                                   value="1" <?php echo checked( ! empty( $item->isnaturalwidth ), 1, false ); ?> />
                        </label>
                    </p>
                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-is-altstyle-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Alternative Style Mega Menu', 'wow' ); ?>
                            <input type="checkbox" id="edit-menu-is-altstyle-<?php echo esc_attr($item_id); ?>"
                                   class="edit-menu-item-custom" id="menu-is-altstyle[<?php echo esc_attr($item_id); ?>]"
                                   name="menu-is-altstyle[<?php echo esc_attr($item_id); ?>]"
                                   value="1" <?php echo checked( ! empty( $item->altstyle ), 1, false ); ?> />
                        </label>
                    </p>
                    
                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-hideheadings-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Hide Mega Menu Headings', 'wow' ); ?>
                            <input type="checkbox" id="edit-menu-hideheadings-<?php echo esc_attr($item_id); ?>"
                                   class="edit-menu-item-custom" id="menu-hideheadings[<?php echo esc_attr($item_id); ?>]"
                                   name="menu-hideheadings[<?php echo esc_attr($item_id); ?>]"
                                   value="1" <?php echo checked( ! empty( $item->hideheadings ), 1, false ); ?> />
                        </label>
                    </p>

                    <p class="field-custom description description-wide" style="height: 10px;"></p>

                <?php } ?>

                <h4><?php esc_html_e( 'Custom Menu Options', 'wow' ); ?></h4>

                <p class="field-custom description description-wide">
                    <label
                        for="edit-menu-loggedin-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Visible only when logged in', 'wow' ); ?>
                        <input type="checkbox" id="edit-menu-loggedin-<?php echo esc_attr($item_id); ?>"
                               class="edit-menu-item-custom" id="menu-loggedin[<?php echo esc_attr($item_id); ?>]"
                               name="menu-loggedin[<?php echo esc_attr($item_id); ?>]"
                               value="1" <?php echo checked( ! empty( $item->loggedin ), 1, false ); ?> />
                    </label>
                </p>

                <p class="field-custom description description-wide">
                    <label
                        for="edit-menu-loggedout-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Visible only when logged out', 'wow' ); ?>
                        <input type="checkbox" id="edit-menu-loggedout-<?php echo esc_attr($item_id); ?>"
                               class="edit-menu-item-custom" id="menu-loggedout[<?php echo esc_attr($item_id); ?>]"
                               name="menu-loggedout[<?php echo esc_attr($item_id); ?>]"
                               value="1" <?php echo checked( ! empty( $item->loggedout ), 1, false ); ?> />
                    </label>
                </p>

                <?php if ( $depth == 0 ) { ?>

                	<p class="field-custom description description-wide">
                	    <label
                	        for="edit-menu-newbadge-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'New Badge', 'wow' ); ?>
                	        <input type="checkbox" id="edit-menu-newbadge-<?php echo esc_attr($item_id); ?>"
                	               class="edit-menu-item-custom" id="menu-newbadge[<?php echo esc_attr($item_id); ?>]"
                	               name="menu-newbadge[<?php echo esc_attr($item_id); ?>]"
                	               value="1" <?php echo checked( ! empty( $item->newbadge ), 1, false ); ?> />
                	    </label>
                	</p>
                	<p class="field-custom description description-wide">
                	    <label
                	        for="edit-menu-salebadge-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Sale Badge', 'wow' ); ?>
                	        <input type="checkbox" id="edit-menu-salebadge-<?php echo esc_attr($item_id); ?>"
                	               class="edit-menu-item-custom" id="menu-salebadge[<?php echo esc_attr($item_id); ?>]"
                	               name="menu-salebadge[<?php echo esc_attr($item_id); ?>]"
                	               value="1" <?php echo checked( ! empty( $item->salebadge ), 1, false ); ?> />
                	    </label>
                	</p>
                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-menubtn-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Button Style Menu Item', 'wow' ); ?>
                            <input type="checkbox" id="edit-menu-menubtn-<?php echo esc_attr($item_id); ?>"
                                   class="edit-menu-item-custom" id="menu-menubtn[<?php echo esc_attr($item_id); ?>]"
                                   name="menu-menubtn[<?php echo esc_attr($item_id); ?>]"
                                   value="1" <?php echo checked( ! empty( $item->menubtn ), 1, false ); ?> />
                        </label>
                    </p>

                <?php } ?>

                <p class="field-custom description description-thin"
                   style="height: auto;overflow: hidden;width: 50%;float: none;">
                    <label
                        for="edit-menu-item-icon-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Menu Icon (Icon Mind / Font Awesome)', 'wow' ); ?>
                        <input type="text" id="edit-menu-item-icon-<?php echo esc_attr($item_id); ?>"
                               class="widefat edit-menu-item-custom" name="menu-item-icon[<?php echo esc_attr($item_id); ?>]"
                               placeholder="fa-star" value="<?php echo esc_attr( $item->menuicon ); ?>"/>
                    </label>
                </p>

                <?php if ( $depth == 1) { ?>

                    <h4><?php esc_html_e( 'Mega Menu Column Options', 'wow' ); ?></h4>

                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-megatitle-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Mega Menu No Link Title', 'wow' ); ?>
                            <input type="checkbox" id="edit-menu-megatitle-<?php echo esc_attr($item_id); ?>"
                                   class="edit-menu-item-custom" id="menu-megatitle[<?php echo esc_attr($item_id); ?>]"
                                   name="menu-megatitle[<?php echo esc_attr($item_id); ?>]"
                                   value="1" <?php echo checked( ! empty( $item->megatitle ), 1, false ); ?> />
                        </label>
                    </p>

                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-nocolspa-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'No Menu Column Spacing (for custom html column)', 'wow' ); ?>
                            <input type="checkbox" id="edit-menu-nocolspa-<?php echo esc_attr($item_id); ?>"
                                   class="edit-menu-item-custom" id="menu-nocolspa[<?php echo esc_attr($item_id); ?>]"
                                   name="menu-nocolspa[<?php echo esc_attr($item_id); ?>]"
                                   value="1" <?php echo checked( ! empty( $item->nocolspa ), 1, false ); ?> />
                        </label>
                    </p>
                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-item-width-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Custom HTML Column Width (optional)', 'wow' ); ?>
                            <input type="text" id="edit-menu-item-width-<?php echo esc_attr($item_id); ?>"
                                   class="widefat edit-menu-item-custom" name="menu-item-width[<?php echo esc_attr($item_id); ?>]"
                                   value="<?php echo esc_attr( $item->menuwidth ); ?>"/>
                        </label>
                    <p><?php esc_html_e( 'Optionally set a width here for the menu column, ideal if you want to make it wider. Numeric value (no px).', 'wow' ); ?></p>
                    </p>

                    <p class="field-custom description description-wide">
                        <label
                            for="edit-menu-item-htmlcontent-<?php echo esc_attr($item_id); ?>"><?php esc_html_e( 'Custom HTML Column (within Mega Menu)', 'wow' ); ?>
                            <textarea id="edit-menu-item-htmlcontent-<?php echo esc_attr($item_id); ?>"
                                      name="menu-item-htmlcontent[<?php echo esc_attr($item_id); ?>]" cols="30"
                                      rows="4"><?php echo esc_textarea( $item->htmlcontent ); ?></textarea>
                        </label>
                    </p>

                <?php } ?>
				<p class="field-imagemenu description description-wide">
					<label for="edit-menu-item-imagemenu-<?php echo esc_attr($item_id); ?>">
					<?php esc_html_e( 'Menu Thumbnail', 'wow' ); ?>
					<input id="edit-menu-item-imagemenu-<?php echo esc_attr($item_id); ?>" class="code edit-menu-item-imagemenu" name="menu-item-imagemenu[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->imagemenu ); ?>" type="hidden">
					<?php if($item->imagemenu){?>
						<img class="edit-menu-item-imagemenu-<?php echo esc_attr($item_id);?>" src="<?php echo esc_url( $item->imagemenu ); ?>" style="display: block; width:400px;height:150px;">
					<?php }else{?>
						<img class="edit-menu-item-imagemenu-<?php echo esc_attr($item_id);?>" src="" style="display: none; width:400px;height:150px;">
					<?php } ?>
					<a class="upload_image_button" href="javascript:void(0);" data-image_id="edit-menu-item-imagemenu-<?php echo esc_attr($item_id);?>"><?php esc_html_e( 'Browse', 'wow' ); ?></a>
					<a class="remove_image_button" href="javascript:void(0);" data-image_id="edit-menu-item-imagemenu-<?php echo esc_attr($item_id);?>"><?php esc_html_e( 'Remove', 'wow' ); ?></a>
					</label>
				</p>
            </div>
        	<?php
        }

        /**
         * Add custom fields to $item nav object
         * in order to be used in custom Walker
         *
         * @access      public
         * @since       1.0
         * @return      void
         */
        function wow_mega_menu_add_custom_nav_fields( $menu_item ) {

            $menu_item->subtitle        = get_post_meta( $menu_item->ID, '_menu_item_subtitle', true );
            $menu_item->htmlcontent     = get_post_meta( $menu_item->ID, '_menu_item_htmlcontent', true );
            $menu_item->nocolspa = get_post_meta( $menu_item->ID, '_menu_nocolspa', true );
            $menu_item->megamenu        = get_post_meta( $menu_item->ID, '_menu_megamenu', true );
            $menu_item->menucol    = get_post_meta( $menu_item->ID, '_menu_menucol', true );
            $menu_item->isnaturalwidth  = get_post_meta( $menu_item->ID, '_menu_is_naturalwidth', true );
            $menu_item->altstyle        = get_post_meta( $menu_item->ID, '_menu_is_altstyle', true );
            $menu_item->hideheadings    = get_post_meta( $menu_item->ID, '_menu_hideheadings', true );
            $menu_item->loggedin     = get_post_meta( $menu_item->ID, '_menu_loggedin', true );
            $menu_item->loggedout    = get_post_meta( $menu_item->ID, '_menu_loggedout', true );
            $menu_item->newbadge   		= get_post_meta( $menu_item->ID, '_menu_newbadge', true );
			$menu_item->salebadge   		= get_post_meta( $menu_item->ID, '_menu_salebadge', true );
            $menu_item->menubtn     = get_post_meta( $menu_item->ID, '_menu_menubtn', true );
            $menu_item->megatitle       = get_post_meta( $menu_item->ID, '_menu_megatitle', true );
            $menu_item->menuicon        = get_post_meta( $menu_item->ID, '_menu_item_icon', true );
            $menu_item->menuwidth       = get_post_meta( $menu_item->ID, '_menu_item_width', true );
			$menu_item->imagemenu       = get_post_meta( $menu_item->ID, '_menu_item_imagemenu', true );

            return $menu_item;

        }

        /**
         * Save menu custom fields
         *
         * @access      public
         * @since       1.0
         * @return      void
         */
        function wow_mega_menu_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {

            // Check if element is properly sent
            if ( isset( $_REQUEST['menu-item-subtitle'] ) ) {
                $subtitle_value = $_REQUEST['menu-item-subtitle'][ $menu_item_db_id ];
                update_post_meta( $menu_item_db_id, '_menu_item_subtitle', $subtitle_value );
            }

            if ( isset( $_REQUEST['menu-item-icon'] ) ) {
                $menu_icon_value = $_REQUEST['menu-item-icon'][ $menu_item_db_id ];
                update_post_meta( $menu_item_db_id, '_menu_item_icon', $menu_icon_value );
            }

            if ( isset( $_REQUEST['menu-item-htmlcontent'][ $menu_item_db_id ] ) ) {
                $menu_htmlcontent_value = $_REQUEST['menu-item-htmlcontent'][ $menu_item_db_id ];
                update_post_meta( $menu_item_db_id, '_menu_item_htmlcontent', $menu_htmlcontent_value );
            }
			
            if ( isset( $_REQUEST['menu-item-imagemenu'] ) ) {
                $menu_imagemenu_value = $_REQUEST['menu-item-imagemenu'][ $menu_item_db_id ];
                update_post_meta( $menu_item_db_id, '_menu_item_imagemenu', $menu_imagemenu_value );
            }			

            if ( isset( $_REQUEST['menu-megamenu'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_megamenu', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_megamenu', 0 );
            }

            if ( isset( $_REQUEST['menu-menucol'][ $menu_item_db_id ] ) ) {
                $menucol_value = $_REQUEST['menu-menucol'][ $menu_item_db_id ];
                update_post_meta( $menu_item_db_id, '_menu_menucol', $menucol_value );
            }

            if ( isset( $_REQUEST['menu-is-naturalwidth'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_is_naturalwidth', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_is_naturalwidth', 0 );
            }

            if ( isset( $_REQUEST['menu-menubtn'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_menubtn', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_menubtn', 0 );
            }

            if ( isset( $_REQUEST['menu-loggedin'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_loggedin', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_loggedin', 0 );
            }

            if ( isset( $_REQUEST['menu-loggedout'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_loggedout', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_loggedout', 0 );
            }

            if ( isset( $_REQUEST['menu-newbadge'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_newbadge', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_newbadge', 0 );
            }
			
            if ( isset( $_REQUEST['menu-salebadge'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_salebadge', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_salebadge', 0 );
            }			

            if ( isset( $_REQUEST['menu-is-altstyle'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_is_altstyle', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_is_altstyle', 0 );
            }

            if ( isset( $_REQUEST['menu-hideheadings'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_hideheadings', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_hideheadings', 0 );
            }

            if ( isset( $_REQUEST['menu-megatitle'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_megatitle', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_megatitle', 0 );
            }

            if ( isset( $_REQUEST['menu-nocolspa'][ $menu_item_db_id ] ) ) {
                update_post_meta( $menu_item_db_id, '_menu_nocolspa', 1 );
            } else {
                update_post_meta( $menu_item_db_id, '_menu_nocolspa', 0 );
            }

            if ( isset( $_REQUEST['menu-item-width'][ $menu_item_db_id ] ) ) {
                $menu_width_value = $_REQUEST['menu-item-width'][ $menu_item_db_id ];
                update_post_meta( $menu_item_db_id, '_menu_item_width', $menu_width_value );
            }

        }

        /**
         * Define new Walker edit
         *
         * @access      public
         * @since       1.0
         * @return      void
         */
        function wow_mega_menu_edit_walker( $walker, $menu_id ) {

            return 'Walker_Nav_Menu_Edit_Custom';

        }

    }

    $GLOBALS['wow_mega_menu'] = new wow_mega_menu();

?>