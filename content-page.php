<article id="post-<?php esc_attr(the_ID()); ?>" <?php post_class(); ?>>
	<?php
		// Page thumbnail and title.
		wow_post_thumbnail();
		
	?>

	<div class="entry-content">
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'wow' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );

		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
