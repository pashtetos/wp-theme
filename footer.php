	</div><!-- #main -->
		<?php 
			global $wow_settings,$page_id;
			$footer_style = wow_get_config('footer_style','');
			$footer_style = (get_post_meta( $page_id,'page_footer_style', true )) ? get_post_meta( $page_id, 'page_footer_style', true ) : $footer_style ;
			$header_style = wow_get_config('header_style', ''); 
			$header_style  = (get_post_meta( $page_id, 'page_header_style', true )) ? get_post_meta($page_id, 'page_header_style', true ) : $header_style ;
		?>	
		<?php if($footer_style && (get_post($footer_style))){ ?>
			<footer id="bin-footer" class="bin-footer <?php echo esc_attr( get_post($footer_style)->post_name ); ?>">
				<div class="container">
					<?php
						$post_content = get_post( $footer_style )->post_content;
						echo do_shortcode( $post_content ); 
						wow_parseShortcodesCustomCss($post_content);
					?>
				</div>
			</footer>
		<?php }else{ ?>
			<div class="bin-copyright">
				<div class="container">					
					<div class="site-info text-center">
						<?php echo esc_html__( 'БМФ ©  ','wow'); ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html__('Wow', 'wow'); ?></a>
						<?php echo esc_html__( 'Всі права захищені.','wow'); ?>
					</div><!-- .site-info -->
				</div>
			</div>
		<?php } ?>
	</div><!-- #page -->
	
	
	<?php if(isset($header_style) && ($header_style == 1 || $header_style == 4)) { ?>
		<div class="search-overlay hidden">
			<div class="search-popup-bg"></div>
			<div class="container">
				<?php get_template_part( 'search-form' ); ?>			
			</div>
		</div>
	<?php } ?>
	
	<?php 
		$back_active = wow_get_config('back_active');
		if($back_active && $back_active == 1):
	?>
	<div class="back-top">
		<span class="fa fa-angle-up">&nbsp;</span>
		<span><?php echo esc_html__('TOP', 'wow') ?></span>
	</div>
	<?php endif;?>
	
	<?php if((isset($wow_settings['show-newletter']) && $wow_settings['show-newletter']) && is_active_sidebar('newletter-popup-form')) : ?>		
		<?php wow_popup_newsletter(); ?>
	<?php endif;  ?>
	
	</div>
	<?php wp_footer(); ?>
</body>
</html>