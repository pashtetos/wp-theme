<?php
/**
 * Template Name: Home Page
 *
 * @package Bingotheme
 * @subpackage Wow
 * @since Bingo Wow 1.0
 */
get_header(); ?>

<div id="main-content" class="main-content main-content-page">
	<div id="primary" class="content-area container">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

				endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_footer();
