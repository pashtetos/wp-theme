<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<?php if ( is_active_sidebar( 'sidebar-product' ) ) : ?>
<div id="primary-sidebar-product" class="bin-sidebar sidebar-product widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-product' ); ?>
</div><!-- #primary-sidebar -->
<?php endif; ?>
