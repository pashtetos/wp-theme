<?php

define('wow_version',               '1.0'); 
if (!isset($content_width)) { $content_width = 940; }

require_once( get_template_directory().'/libs/class-tgm-plugin-activation.php' );
require_once (get_template_directory().'/libs/plugin-requirement.php');
require_once(get_template_directory().'/libs/function.php' );
require_once( get_template_directory().'/libs/loader.php' );
require_once(get_template_directory().'/libs/megamenu/megamenu.php' );
include_once(get_template_directory().'/libs/megamenu/mega_menu_custom_walker.php' );
include_once( get_template_directory().'/libs/menus.php' );
include_once(get_template_directory().'/libs/template-tags.php' );
require_once( get_template_directory().'/libs/woocommerce.php' );
require_once(get_template_directory().'/libs/admin/functions.php' );
require_once( get_template_directory().'/libs/admin/theme_options.php' );
require_once( get_template_directory().'/libs/aq-resizer.php' );

function wow_custom_css() {
	global $wow_settings;
	if (!is_admin()) {
		wp_enqueue_style(
			'wow_custom_css',
			get_template_directory_uri() . '/css/custom.css'
		);		
		ob_start(); 
		include( get_template_directory().'/libs/custom_css.php' ); 
		$custom_css = ob_get_clean();
		$custom_css = str_replace(array("\r\n", "\r"), "\n", $custom_css);
		$lines = explode("\n", $custom_css);
		$new_lines = array();
		foreach ($lines as $i => $line) { if(!empty($line)) $new_lines[] = trim($line); }
		wp_add_inline_style( 'wow_custom_css', implode($new_lines) );
		
		if ( (isset($wow_settings['css-code'])) && (trim($wow_settings['css-code']) != "" ) ) {
			wp_add_inline_style( 'wow_custom_css', html_entity_decode( $wow_settings['css-code'] ) );
		}		
	}
}

add_action('wp_enqueue_scripts', 'wow_custom_css' );

function wow_custom_js() {
	if (!is_admin()) {
		wp_enqueue_script('wow_custom_js',get_template_directory_uri() . '/js/custom.js', 
		array('jquery'), NULL, true);
		
		ob_start(); 
		include( get_template_directory().'/libs/custom_js.php' ); 
		$custom_js = ob_get_clean();
		$custom_js = str_replace(array("\r\n", "\r"), "\n", $custom_js);
		$lines = explode("\n", $custom_js);
		$new_lines = array();
		foreach ($lines as $i => $line) { if(!empty($line)) $new_lines[] = trim($line); }
			wp_add_inline_script( 'wow_custom_js', implode($new_lines) );
	}
}

add_action('wp_enqueue_scripts', 'wow_custom_js' );











