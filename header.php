<?php
/**
 * Version:            1.0.0
 * Theme Name:         Wow
 * Theme URI:          http://bingotheme.com/themes/wow/
 * Author:             Bingotheme
 * Author URI:         http://bingotheme.com/
 * License:            GNU General Public License v2 or later
 */
?>
<!DOCTYPE html>
<html lang="en-US" >
<?php 
	global $wow_settings, $page_id,$post, $wp_query;
	$direction = wow_get_direction(); 
	$class_direction = '';
	if($direction && $direction == 'rtl'){
		$class_direction .= 'direction="rtl"';
	} 
	$page_id = get_the_ID();	
	$header_style = wow_get_config('header_style', ''); 
	$header_style  = (get_post_meta( $page_id, 'page_header_style', true )) ? get_post_meta($page_id, 'page_header_style', true ) : $header_style ;	
	$sitelogo = wow_get_config('sitelogo');
	$welcome_msg = $wow_settings['welcome-msg'] ? $wow_settings['welcome-msg'] : esc_html__( "Welcome to wow!", 'wow');
	$class_title = (isset($wow_settings['page_title']) && $wow_settings['page_title']) || (isset($wow_settings['breadcrumb']) && $wow_settings['breadcrumb']) || (isset($wow_settings['page_title_bg']) && $wow_settings['page_title_bg']) ? "" : "no-title";
?>
<!--<![endif]-->
<head <?php echo esc_attr($class_direction); ?>>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php esc_url(bloginfo( 'pingback_url' )); ?>">
	<?php if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) : ?>
		<?php if (isset($wow_settings['favicon']) && $wow_settings['favicon']['url']){ ?>
			<link rel="shortcut icon" href="<?php echo esc_url( $wow_settings['favicon']['url'] ); ?>" />
		<?php } ?>
	<?php endif;  ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="wrapper-body <?php echo esc_attr($class_title); ?>">
<?php if(isset($wow_settings['show-loading-overlay']) && $wow_settings['show-loading-overlay'] ){ ?>
<div class="loader-content">
	<div id="loader">
		<div class="dot"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div>
	</div>
</div>
 <?php } ?>
 
<div id='page' class="hfeed page-wrapper">
	<?php if(isset($header_style) && $header_style) { ?>
		<?php get_template_part('headers/header',$header_style); ?>
	<?php }else{ ?>

		<div class="header-wrapper bin-wrapper bin-head-default">
			<div class="container">
				<div class="header-content" data-sticky_header="<?php echo isset($wow_settings['enable-sticky-header']) ? $wow_settings['enable-sticky-header'] : ""; ?>">
					<div class="row">
						<!-- Main Logo -->
						<div class="col-md-2 col-sm-3 col-xs-3 bingoHeaderLeft">
							<div class="bingoLogo">
								<?php if(isset($background_dark) && $background_dark == 1){ ?>									
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<?php if(isset($wow_settings['sitelogo']) && $wow_settings['sitelogo']){ ?>
											<img src="<?php echo esc_url($wow_settings['sitelogo']['url'] ); ?>" alt="<?php bloginfo('name'); ?>"/>
										<?php }else{
											$logo = get_template_directory_uri().'/images/logo/logo.png';
										?>
											<img src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr( bloginfo('name')); ?>"/>
										<?php } ?>
									</a>								
								<?php }else{ ?>
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<?php if(isset($wow_settings['sitelogo']) && $wow_settings['sitelogo']){ ?>
											<img src="<?php echo esc_url($wow_settings['sitelogo']['url'] ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/>
										<?php }else{
											 $logo = get_template_directory_uri().'/images/logo/logo.png';
										?>
											<img src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr( bloginfo('name')); ?>"/>
										<?php } ?>
									</a>								
								<?php } ?>
							</div>
						</div>
						
						<!-- Main Menu -->
						<div class="col-xs-9 col-md-10 col-sm-9 bingoHeaderMiddle">						
							 <!-- Begin menu -->
							<div class="bingo-menu-wrapper">
							  <div class="megamenu">
							   <nav class="navbar-default">
								<div class="navbar-header">
								 <button type="button" id="show-megamenu"  class="navbar-toggle">
								  <span class="icon-bar"></span>
								  <span class="icon-bar"></span>
								  <span class="icon-bar"></span>
								 </button>
								</div>
								<div  class="bin-navigation primary-navigation navbar-mega">
								 <span id="remove-megamenu" class="remove-megamenu icon-remove"></span>
								 <?php echo wow_main_menu( 'main-navigation', 'float' ); ?>
								</div>
							   </nav> 
							  </div>       
							</div><!-- End menu -->						
						</div>						
					</div>					
				</div>
			</div>

		</div><!-- End header-wrapper -->	
	
	<?php } ?>
<div id="bin-main" class="bin-main">
	<?php if ( (isset($wow_settings['page_title']) && $wow_settings['page_title']) || (isset($wow_settings['breadcrumb']) && $wow_settings['breadcrumb']) || (isset($wow_settings['page_title_bg']) && $wow_settings['page_title_bg']) ) { ?>
		<?php wow_page_title(); ?>
	<?php }else{ ?>
		<div class="container">
			<h1 class="bin-title-default"> <?php if(!is_home()){ the_title(); }else{ echo esc_html_e('Latest Posts', 'wow'); } ?> </h1>
		</div>		
	<?php } ?>