<?php 
	get_header();
	$post_single_layout = wow_get_config('post-single-layout');
?>
<div class="container bin-main-content">
	<div class="row">
			<?php if($post_single_layout == 'left' ||  $post_single_layout == 'left_right'):?>			
			<div class="<?php echo esc_attr(wow_get_class()->class_sidebar_left); ?>">
				<?php dynamic_sidebar('sidebar-blog');?>	
			</div>				
			<?php endif; ?>
			<div class="<?php echo esc_attr(wow_get_class()->class_single_content); ?>">
				<div class="post-single">
					<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();

							get_template_part( 'content');

							// Previous/next post navigation.
							wow_post_nav();

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
							}
						endwhile;
					?>
				</div>
			</div>
			<?php if($post_single_layout == 'right' || $post_single_layout == 'left_right'):?>			
				<div class="<?php echo esc_attr(wow_get_class()->class_sidebar_right); ?>">
					<?php dynamic_sidebar('sidebar-blog');?>	
				</div>				
			<?php endif; ?>
            
    </div>
</div>
<?php
get_footer();