	<?php 
		global $wow_settings;
		$welcome_msg = $wow_settings['welcome-msg'] ? $wow_settings['welcome-msg'] : esc_html__( "Need help? Chat with us!", 'wow');
		$skin = wow_get_config('skin','default');
	?>
	<header id='bin-header' class="bin-header header-2 <?php echo isset($wow_settings['enable-sticky-header']) && $wow_settings['enable-sticky-header'] ? esc_attr('sticky-header') : ""; ?>">
		<div class='header-wrapper bin-wrapper'>
			<?php if(isset($wow_settings['show-header-top']) && $wow_settings['show-header-top']) : ?>
				<div class="top-header">
					<div class='container'>
						<div class="row">
							<?php if(is_active_sidebar('top-link')) : ?>
								<div class="col-xs-5 col-sm-6 left">
									<div class="topbar-menu">
										<?php dynamic_sidebar('top-link'); ?>
									</div>
								</div>
							<?php endif; ?>									
							<div class="col-xs-7 col-sm-6 right">
								<div class="wrapper-box">
									<div class="welcome-msg">
										<a href="<?php echo esc_url( home_url("/contact") ); ?>"><?php echo esc_html($welcome_msg); ?></a>
									</div>
									<?php if(is_active_sidebar('content-header-1')) : ?>
										<div class="binAccount">
											<span class="bin-icon"><i class="fa fa-user"></i></span>
											<?php dynamic_sidebar('content-header-1'); ?>
										</div>
									<?php endif; ?>	
									<?php if(isset($wow_settings['show-minicart']) && $wow_settings['show-minicart']){ ?>
										<div class="bingoCartTop">
											<?php get_template_part( 'woocommerce/minicart-ajax' ); ?>
										</div>
									<?php } ?>									
								</div>
							</div>
						</div>
					</div>						
				</div>
			<?php endif ?>
			<div class="content-head header-content" data-sticky_header="<?php echo isset($wow_settings['enable-sticky-header']) ? esc_attr($wow_settings['enable-sticky-header']) : ""; ?>" >
				<div class='container'>
					<div class="row">
						<div class="col-xs-5 col-sm-2 left">
							<div class="bingoLogo">
								<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
									<?php if(isset($wow_settings['sitelogo']) && $wow_settings['sitelogo']){ ?>
										<img src="<?php echo esc_url($wow_settings['sitelogo']['url'] ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/>
									<?php }else{
										if ($skin){$logo = get_template_directory_uri().'/images/logo/logo-'.$skin.'.png';}
										else $logo = get_template_directory_uri().'/images/logo/logo.png';
									?>
										<img src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/>
									<?php } ?>
								</a>
							</div>
							<?php if(isset($wow_settings['sticky-logo']['url']) && $wow_settings['sticky-logo']['url']){ ?>
								<div class="bingoLogo-sticky">
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<img src="<?php echo esc_url($wow_settings['sticky-logo']['url'] ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>" width="<?php echo esc_attr(isset($wow_settings['logo-width-sticky']) && $wow_settings['logo-width-sticky'] ? $wow_settings['logo-width-sticky'] : "80"); ?>"/>
									</a>
								</div>
							<?php }else{ ?>
								<div class="bingoLogo-sticky">
										<?php $logo = get_template_directory_uri().'/images/logo/logo.png'; ?>
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<img src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>" width="<?php echo esc_attr(isset($wow_settings['logo-width-sticky']) && $wow_settings['logo-width-sticky'] ? $wow_settings['logo-width-sticky'] : "80"); ?>" />
									</a>
								</div>							
							<?php } ?>							
						</div>
						<div class="col-xs-2 col-sm-2 middle">

							<!-- Categories Menu -->
							<div class="primary-menu bin-primary-menu">
								<div class="bingo-menu-wrapper">
								  <div class="megamenu">
								   <nav class="navbar-default">
									<div class="navbar-header">
									 <button type="button" id="show-megamenu"  class="navbar-toggle">
									  <span class="icon-bar"></span>
									  <span class="icon-bar"></span>
									  <span class="icon-bar"></span>
									 </button>
									</div>
									<div class="close_tab"></div>
									<div  class="bin-navigation primary-navigation navbar-mega">
										<span id="remove-megamenu" class="remove-megamenu icon-remove"></span>
										<?php echo wow_main_menu( 'main_navigation', 'float' ); ?>
									</div>
								   </nav> 
								  </div>       
								</div><!-- End menu -->					
							</div>
							
						</div>
						<?php if(is_active_sidebar('content-header-2')) : ?>
							<div class="col-xs-5 col-sm-4 middle">
								<?php dynamic_sidebar('content-header-2'); ?>
							</div>
						<?php endif; ?>
						<?php if(isset($wow_settings['show-searchform']) && $wow_settings['show-searchform']){ ?>
							<div class="col-xs-12 col-sm-4 right">
								<div class="search-box bin-search">
									<?php get_template_part( 'search-form' ); ?>	
								</div>
							</div>
						<?php } ?>										
					</div>
				</div>
			</div>
			<div class="categories-menu">
				<div class='container'>
					<div class="btn-categories visible-xs">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</div>
					<div class="close-tab-2"></div>
					<div class="wrapper-categories">
						<span class="remove-megamenu remove-megamenu--2 visible-xs fa fa-close"></span>
						<?php echo wow_main_menu2( 'main_navigation2', 'float' ); ?>
					</div>
				</div>
			</div>						
		</div><!-- End header-wrapper -->		
	</header><!-- End #bin-header -->