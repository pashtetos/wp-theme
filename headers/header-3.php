	<?php 
		global $wow_settings, $post, $wp_query;
		if (function_exists('is_product_category') && is_product_category()) {
			$cat_obj = $wp_query->get_queried_object();
			$background_dark = absint( get_woocommerce_term_meta( $cat_obj->term_id, 'background_dark', true ) );
		}
		$welcome_msg = (isset($wow_settings['welcome-msg']) && $wow_settings['welcome-msg']) ? $wow_settings['welcome-msg'] : esc_html__( "Welcome to wow!", 'wow');	
	?>
	<header id='bin-header' class="bin-header header-3 <?php echo isset($wow_settings['enable-sticky-header']) && $wow_settings['enable-sticky-header'] ? esc_attr('sticky-header') : ""; ?>">
		<?php if(isset($wow_settings['show-header-top']) && $wow_settings['show-header-top']){ ?>
			<div id="bin-topbar">
				<div class="container">
					<div class="row">
						<div class=" col-md-5 col-sm-3 col-xs-4">
							<div class="topbar-message pull-left">
								<span><?php echo esc_html($welcome_msg); ?></span>
							</div>						
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4 text-center">
							<?php echo wow_socials_link(); ?>
						</div>
						<div class="col-md-5 col-sm-6 col-xs-4 ">
							<div class="wp-top">
								<?php if(is_active_sidebar('top-link')) : ?>
									<div class="topbar-menu">
										<?php dynamic_sidebar('top-link'); ?>
									</div>
								<?php endif; ?>
								<?php if(is_user_logged_in()) { ?>
									<div class="sign-out-top"><?php echo esc_html__('Bingotheme! ','wow'); ?> <a href="<?php echo wp_logout_url(); ?>"><?php echo esc_html__('Signout','wow'); ?></a></div>
								<?php }else{ ?>
									<?php echo wow_login_top(); ?>
									<div class="sign-up-top">
										<a href="<?php echo esc_url(home_url("/my-account")); ?>"><?php echo esc_html__('Sign up','wow'); ?></a>								
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
		<?php } ?>
		
		<div class="header-wrapper bin-wrapper">
			<div class="container">
				<div class="header-content" data-sticky_header="<?php echo isset($wow_settings['enable-sticky-header']) ? $wow_settings['enable-sticky-header'] : ""; ?>">
					<div class="row">
						<!-- Search Form -->
						<?php if(isset($wow_settings['show-searchform']) && $wow_settings['show-searchform']){ ?>
							<div class="col-md-4 col-sm-4 col-xs-12 header-content__left">
								<div class="search-box bin-search">
									<?php get_template_part( 'search-form' ); ?>	
								</div>
							</div>
						<?php } ?>	
						<!-- The end Search Form -->
						<!-- Main Logo -->
						<div class="col-md-4 col-sm-4 col-xs-12 header-content__middle">
							<div class="bingoLogo">
								<?php if(isset($background_dark) && $background_dark == 1){ ?>									
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<?php if(isset($wow_settings['sitelogo']) && $wow_settings['sitelogo']){ ?>
											<img src="<?php echo esc_url($wow_settings['sitelogo']['url'] ); ?>" alt="<?php bloginfo('name'); ?>"/>
										<?php }else{
											$logo = get_template_directory_uri().'/images/logo/logo.png';
										?>
											<img src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr( bloginfo('name')); ?>"/>
										<?php } ?>
									</a>								
								<?php }else{ ?>
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<?php if(isset($wow_settings['sitelogo_dark']) && $wow_settings['sitelogo_dark']){ ?>
											<img src="<?php echo esc_url($wow_settings['sitelogo_dark']['url'] ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/>
										<?php }else{
											 $logo = get_template_directory_uri().'/images/logo/logo_dark.png';
										?>
											<img src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr( bloginfo('name')); ?>"/>
										<?php } ?>
									</a>								
								<?php } ?>
							</div>
							<?php if(isset($wow_settings['sticky-logo']['url']) && $wow_settings['sticky-logo']['url']){ ?>
								<div class="bingoLogo-sticky hide">
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<img src="<?php echo esc_url($wow_settings['sticky-logo']['url'] ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>" width="<?php echo esc_attr(isset($wow_settings['logo-width-sticky']) && $wow_settings['logo-width-sticky'] ? $wow_settings['logo-width-sticky'] : "80"); ?>"/>
									</a>
								</div>
							<?php }else{?>
								<div class="bingoLogo-sticky hide">
										<?php $logo = get_template_directory_uri().'/images/logo/logo_dark.png'; ?>
									<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<img src="<?php echo esc_url( $logo ); ?>" alt="<?php esc_attr(bloginfo('name')); ?>" width="<?php echo esc_attr(isset($wow_settings['logo-width-sticky']) && $wow_settings['logo-width-sticky'] ? $wow_settings['logo-width-sticky'] : "80"); ?>" />
									</a>
								</div>							
							<?php } ?>
						</div>
											
						<!-- Search - Cart -->
						<div class="col-md-4 col-sm-4 col-xs-12 header-content__right">
							<!-- End Search -->
							<?php if(is_active_sidebar('content-header-1')) : ?>
								<div class="binAccount">
									<span class="bin-icon"><i class="icofont icofont-business-man"></i></span>
									<?php dynamic_sidebar('content-header-1'); ?>
								</div>
							<?php endif; ?>
							<?php if(isset($wow_settings['show-minicart']) && $wow_settings['show-minicart']){ ?>
								<div class="bingoCartTop">
									<?php get_template_part( 'woocommerce/minicart-ajax' ); ?>
								</div>
							<?php } ?>
						</div>						
					</div>	
					
					<!-- Begin menu -->
					<div class="bingo-menu-wrapper text-center">
					  <div class="megamenu">
					   <nav class="navbar-default">
						<div class="navbar-header">
						 <button type="button" id="show-megamenu"  class="navbar-toggle">
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						 </button>
						</div>
						<div class="close_tab"></div>
						<div  class="bin-navigation primary-navigation navbar-mega">
						 <span id="remove-megamenu" class="remove-megamenu icon-remove"></span>
						 <?php echo wow_main_menu( 'main-navigation', 'float' ); ?>
						</div>
					   </nav> 
					  </div>       
					</div><!-- End menu -->											
				</div>
			</div>

		</div><!-- End header-wrapper -->

		
	</header><!-- End #bin-header -->