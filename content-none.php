<div class="row">
	<div class="col-xs-12 col-sm-2"></div>
	<div class="col-xs-12 col-sm-8">
		<h1 class="title-notfound"><?php echo esc_html__('Nothing Found','wow'); ?></h1>
		<div class="alert alert-warning alert-dismissible" role="alert">
			<p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'wow'); ?></p>
		</div>
		<div class="bin-search-form">		
			<?php if ( class_exists( 'WooCommerce' ) ) {?>
				<?php get_template_part( 'search-form' ); ?>
			<?php }else{ ?>
				<?php get_search_form(); ?>		
			<?php } ?>	
		</div>
	</div>
	<div class="col-xs-12 col-sm-2"></div>
</div>