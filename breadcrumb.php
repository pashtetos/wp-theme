<?php
global $post,$wp_query;
if(in_array("search-no-results",get_body_class())){ ?>
   <div class="breadcrumb bin-breadcrumb" class="col-sm-12">
   <a href="<?php esc_url( home_url( '/' )); ?>"><?php echo esc_html__('Home', 'wow'); ?></a>
   <span class="delimiter"></span>
   <span class="current"><?php echo esc_html__('Search results for: ', 'wow') . get_search_query(); ?></span> </div>
<?php
    }else{
    	$delimiter = '<span class="delimiter"></span>';
        $home = esc_html__('Home', 'wow');
        $before = '<span class="current">';
        $after = '</span> ';
        echo '<div id="bin-breadcrumb" class="bin-breadcrumb">';
        $homeLink = home_url();
        echo '<div class="bin-breadcrumb-inner">';
        echo '<a href="' . esc_url( $homeLink ). '">' . esc_html($home) . '</a> ' . $delimiter . ' ';
        if ( is_category() ) {
	        $cat_obj = $wp_query->get_queried_object();
	        $thisCat = $cat_obj->term_id;
	        $thisCat = get_category($thisCat);
	        $parentCat = get_category($thisCat->parent);
	        if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
	        echo $before . '' . single_cat_title('', false) . '' . $after;
        } elseif ( is_day() ) {
	        echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
	        echo '<a href="' . esc_url(get_month_link(get_the_time('Y'),get_the_time('m'))) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
	        echo $before . esc_html__('Archive by date','wow') .'"' . get_the_time('d') .'"' . $after;
        } elseif ( is_month() ) {
	        echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
	        echo $before . esc_html__('Archive by month','wow') .'"' . get_the_time('F') .'"' . $after;
        } elseif ( is_year() ) {
        	echo $before . esc_html__('Archive by year', 'wow') .'"' . get_the_time('Y') . '"' . $after;
        } elseif ( is_single() && !is_attachment() ) {
	        if ( get_post_type() != 'post' ) {
		        $post_type = get_post_type_object(get_post_type());
		        $slug = $post_type->rewrite;
		        echo '<a href="' . esc_url($homeLink) . '/' . esc_attr($slug['slug']) . '/">' . $post_type->labels->singular_name . '</a>' . $delimiter . ' ';
		        echo $before . get_the_title() . $after;

		        echo $before . '<span class="breadcrumb-title">' . get_the_title(). '</span>' . $after;
	        } else {
		        $cat = get_the_category(); $cat = $cat[0];
		        echo ' ' . get_category_parents($cat, TRUE, ' ' . $delimiter . ' ') . ' ';
		        echo $before . '' . get_the_title() . '' . $after;
	        }
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
	        $post_type = get_post_type_object(get_post_type());
	        echo $before . $post_type->labels->singular_name . $after;
        } elseif ( is_attachment() ) {
	        $parent_id  = $post->post_parent;
	        $breadcrumbs = array();
	        while ($parent_id) {
		        $page = get_page($parent_id);
		        $breadcrumbs[] = '<a href="' . esc_url(get_permalink($page->ID)) . '">' . get_the_title($page->ID) . '</a>';
		        $parent_id    = $page->post_parent;
	        }
	        $breadcrumbs = array_reverse($breadcrumbs);
	        foreach ($breadcrumbs as $crumb) echo ' ' . $crumb . ' ' . $delimiter . ' ';
	        echo $before . '' . get_the_title() . '' . $after;
        } elseif ( is_page() && !$post->post_parent ) {
        	echo $before . '' . get_the_title() . '' . $after;
        } elseif ( is_page() && $post->post_parent ) {
	        $parent_id  = $post->post_parent;
	        $breadcrumbs = array();
	        while ($parent_id) {
		        $page = get_page($parent_id);
		        $breadcrumbs[] = '<a href="' . esc_url(get_permalink($page->ID)) . '">' . get_the_title($page->ID) . '</a>';
		        $parent_id    = $page->post_parent;
	        }
	        $breadcrumbs = array_reverse($breadcrumbs);
	        foreach ($breadcrumbs as $crumb) echo ' ' . $crumb . ' ' . $delimiter . ' ';
        	echo $before . '' . get_the_title() . '"' . $after;
        } elseif ( is_search()) {
            echo $before . esc_html__('Search results for ','wow') .'"' . get_search_query() . '"' . $after;

        } elseif ( is_tag() ) {
        	echo $before . esc_html__('Archive by tag ','wow') .'"' . single_tag_title('', false) . '"' . $after;
        } elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
				echo $before . esc_html__(' Articles posted by ','wow') .'"' . $userdata->display_name . '"' . $after;
        } elseif ( is_404() ) {
        	echo $before . esc_html__('You got it ','wow') .'"' . esc_html__(' Error 404 not Found ','wow') . '"&nbsp;' . $after;
        }else{
			 echo $before . esc_html__('Blog','wow') . $after;
		}			
        if ( get_query_var('paged') ) {
	        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' ';
	        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo '';
        }
        echo '</div></div>';
    }
?>